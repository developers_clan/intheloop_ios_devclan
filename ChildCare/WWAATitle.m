//
//  WWAATitle.m
//  ChildCare
//
//  Created by Devclan on 19/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "WWAATitle.h"
#import "AppManager.h"

@implementation WWAATitle

- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        _markup=[AppManager getStringForKey:@"#markup" fromDic:dic];
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _markup=@"TITLE N/A";
    }
    return self;
}

@end
