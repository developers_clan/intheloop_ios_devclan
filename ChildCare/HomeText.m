//
//  HomeText.m
//  ChildCare
//
//  Created by developer clan on 11/04/2016.
//  Copyright © 2016 Devclan. All rights reserved.
//

#import "HomeText.h"

@implementation HomeText

-(instancetype)init{
    self = [super init];
    if (self) {
        _homeText=@"";
    }
    return self;
}

-(instancetype)initWithDictionary:(NSDictionary *)dic{
    
    self=[self init];
    if (self) {
        
        _homeText = [AppManager getStringForKey:@"home" fromDic:dic];
        
    }
    return self;
}

@end
