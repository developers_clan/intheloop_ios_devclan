//
//  TeamOffice.h
//  ChildCare
//
//  Created by Devclan on 20/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamOffice : NSObject

@property (nonatomic,strong)NSString* office;
- (instancetype)initWithDictionary:(NSDictionary*)dic;

@end
