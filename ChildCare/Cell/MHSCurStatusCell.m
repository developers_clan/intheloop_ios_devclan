//
//  MHSCurStatusCellTableViewCell.m
//  ChildCare
//
//  Created by Devclan on 30/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "MHSCurStatusCell.h"

@implementation MHSCurStatusCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(IBAction)changeStatus:(id)sender{
    [_delegate cell:self changeStatusClicked:_indexPath];
}
-(CGFloat)cellHeight{
    CGFloat height=0;
    return height;
}
@end
