//
//  ExpandedProgramCell.h
//  ChildCare
//
//  Created by Devclan on 31/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "ProgramCell.h"
#import "MSHCheckBoxCell.h"
@interface ExpandedProgramCell : ProgramCell<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableVu;
@property (strong,nonatomic)NSMutableArray* checkBoxes;
@property (weak,nonatomic)id<StatusUpdateCallBackDelegate>delegate;
-(CGFloat)cellHeight;
-(void)reloadData;
@end
