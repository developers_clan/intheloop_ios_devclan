//
//  ContactCell.h
//  ChildCare
//
//  Created by Devclan on 26/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *number;

@property (weak, nonatomic) IBOutlet UIView *containerVu;
@property (weak, nonatomic) IBOutlet UIView *phoneBg;

@end
