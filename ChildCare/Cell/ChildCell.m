//
//  ChildCell.m
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "ChildCell.h"
#import "AppManager.h"

@implementation ChildCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _container.layer.borderWidth=1;
//    _container.layer.borderColor=[[AppManager sharedManager] placeholderColor].CGColor;
    // Configure the view for the selected state
}
-(IBAction)deleteClicked:(id)sender{
    [_delegate cell:self deleteClicked:_indexPath];
}
-(IBAction)editClicked:(id)sender{
    [_delegate cell:self editClicked:_indexPath];
}
@end
