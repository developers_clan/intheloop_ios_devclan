//
//  ChildCell.h
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChildCell;
@protocol ChildCellInteractionCallBackDelegate <NSObject>

@required
-(void)cell:(ChildCell*)cell editClicked:(NSIndexPath*)indexPath;
-(void)cell:(ChildCell*)cell deleteClicked:(NSIndexPath*)indexPath;
@end

@interface ChildCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *edit;
@property (weak, nonatomic) IBOutlet UIButton *del;
@property (weak, nonatomic) IBOutlet UIView *container;

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak,nonatomic) id<ChildCellInteractionCallBackDelegate>delegate;
@property (strong,nonatomic) NSIndexPath* indexPath;
@end
