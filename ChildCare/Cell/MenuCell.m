//
//  MenuCell.m
//  ChildCare
//
//  Created by Devclan on 23/12/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
