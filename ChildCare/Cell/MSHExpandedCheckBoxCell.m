//
//  MSHExpandedCheckBoxCell.m
//  ChildCare
//
//  Created by Devclan on 31/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "MSHExpandedCheckBoxCell.h"
#import "AppManager.h"
#import "Constants.h"

@implementation MSHExpandedCheckBoxCell{
        PopupViewController* popVC;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    popVC=[storyboard instantiateViewControllerWithIdentifier:@"PopupViewController"];
    popVC.delegate=self;
//    self.dropDownMenu = [[IGLDropDownMenu alloc] init];
//    self.dropDownMenu.delegate=self;
//    self.dropDownMenu.menuText = @"Set status";
//    self.dropDownMenu.paddingLeft = 15;
//    self.dropDownMenu.layer.shadowColor=[UIColor clearColor].CGColor;
//    
//    self.dropDownMenu.type = IGLDropDownMenuTypeNormal;
//    self.dropDownMenu.menuIconImage = [UIImage imageNamed:@"drop_down"];
//    [self setItems:@[@"Waiting",@"Accessing",@"Completed"] ToMenu:self.dropDownMenu];
//    [self.dropBoxVu.superview addSubview:self.dropDownMenu];
//    
//    
//    [self.dropDownMenu setFrame:_dropBoxVu.frame];
//    
//    [_dropDownMenu reloadView];
//    if (_dropdownIndex>-1) {
//        [_dropDownMenu selectItemAtIndex:_dropdownIndex];
//    }
}

/*
#pragma mark - IGLDropdown
- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu selectedItemAtIndex:(NSInteger)index
{
    if (dropDownMenu==self.dropDownMenu) {
        _dropdownIndex=(int)index;
    }
}
-(void)setItems:(NSArray*)items ToMenu:(IGLDropDownMenu*)menu{
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < items.count; i++) {
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        [item setText:items[i]];
        [dropdownItems addObject:item];
    }
    menu.dropDownItems=dropdownItems;
    [menu reloadView];
}

*/


- (IBAction)setStatusAction:(id)sender {
    [popVC.view setFrame:CGRectMake(_dropBoxVu.frame.size.width, _dropBoxVu.frame.origin.y, 5, 5)];//notice this is OFF screen!
    [UIView animateWithDuration:ANIMATION_DURATION_SHORT animations:^{
        [popVC.view setFrame:CGRectMake(_dropBoxVu.frame.origin.x, _dropBoxVu.frame.origin.y, _dropBoxVu.frame.size.width, _dropBoxVu.frame.size.height)];
        [_dropBoxVu.superview addSubview:popVC.view];
    } completion:^(BOOL finished) {
        
    }];
    
//    [self addGestureRecognizer:tapGes];
    [_dropBoxVu.superview addSubview:popVC.view];
}
-(void)waitingButtonTapped{
    [self dismissPop];
    [_setStatusBtn setTitleColor:[[AppManager sharedManager] greeColor] forState:UIControlStateNormal];
    [_setStatusBtn setTitle:@"Waiting" forState:UIControlStateNormal];
    [self.delegate updatedWithProgramIndex:self.programIndexPath.row statusIndex:self.indexPath.row completionStatusIndex:0];
}

-(void)completedButtonTapped{
    [self dismissPop];
    [_setStatusBtn setTitleColor:[[AppManager sharedManager] appThemeColor] forState:UIControlStateNormal];
    [_setStatusBtn setTitle:@"Completed" forState:UIControlStateNormal];
    [self.delegate updatedWithProgramIndex:self.programIndexPath.row statusIndex:self.indexPath.row completionStatusIndex:2];
}
-(void)accessingButtonTapped{
    [self dismissPop];
    [_setStatusBtn setTitleColor:[[AppManager sharedManager] yellowColor] forState:UIControlStateNormal];
    [_setStatusBtn setTitle:@"Accessing" forState:UIControlStateNormal];
        [self.delegate updatedWithProgramIndex:self.programIndexPath.row statusIndex:self.indexPath.row completionStatusIndex:1];
}
-(void)dismissPop{
    [UIView animateWithDuration:ANIMATION_DURATION_SHORT animations:^{
        [popVC.view setFrame:CGRectMake(_dropBoxVu.frame.size.width, _dropBoxVu.frame.origin.y, _dropBoxVu.frame.size.width, _dropBoxVu.frame.size.height)];
        //    [self removeGestureRecognizer:tapGes];
        
    } completion:^(BOOL finished) {
        [popVC.view removeFromSuperview];
    }];
}
@end



