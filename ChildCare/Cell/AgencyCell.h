//
//  AgencyCell.h
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgencyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIView *strip;
@property (weak, nonatomic) IBOutlet UIView *contVu;
@property (weak, nonatomic) IBOutlet UIImageView *animImg;

@end
