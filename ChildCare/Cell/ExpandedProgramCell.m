//
//  ExpandedProgramCell.m
//  ChildCare
//
//  Created by Devclan on 31/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "ExpandedProgramCell.h"
#import "MSHExpandedCheckBoxCell.h"
#import "WWAAStatus.h"
#import "AppManager.h"
@implementation ExpandedProgramCell


-(void)reloadData{
    [_tableVu reloadData];
}
#pragma mark -tableviewdelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.path.stageInd=[NSNumber numberWithInt:-1];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.row==[self.path.statusInd integerValue]) {
        self.path.statusInd=[NSNumber numberWithInt:-1];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }else{
        long previous=[self.path.statusInd integerValue];
        self.path.statusInd=[NSNumber numberWithInteger:indexPath.row];
        if (previous>-1) {
            [tableView reloadRowsAtIndexPaths:@[indexPath,[NSIndexPath indexPathForRow:previous inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }else{
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
    }
    [_delegate tableHeightChangedTo:self.tableVu.contentSize.height indexPath:self.indexPath];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    MSHCheckBoxCell* cel = (MSHCheckBoxCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    WWAAStatus *status = [_checkBoxes objectAtIndex:indexPath.row];
    CGRect titleRect = [AppManager frameForTextInConstraint:CGSizeMake(self.frame.size.width-55, 3000) string:status.markup fontName:@"Lato-Bold" fontSize:18];
    
    
    if ([self.path.statusInd integerValue]==indexPath.row) {
        return 24+33+titleRect.size.height;
    }
    return 13+titleRect.size.height;
}
-(CGFloat)cellHeight{
    return 68+_tableVu.contentSize.height;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell=nil;
    if ([self.path.statusInd integerValue]==indexPath.row) {
        MSHExpandedCheckBoxCell* expCell = [tableView dequeueReusableCellWithIdentifier:@"MSHExpandedCheckBoxCell"];
        [AppManager layoutCellIfNeeded:expCell tableView:tableView];
        cell=expCell;
        expCell.delegate=_delegate;
        expCell.indexPath=indexPath;
        expCell.programIndexPath=self.indexPath;
        WWAAStatus *status = [_checkBoxes objectAtIndex:indexPath.row];
        expCell.title.text=status.markup;
        switch ([self.path.stageInd intValue]) {
            case 0:
                [expCell.setStatusBtn setTitleColor:[[AppManager sharedManager] greeColor] forState:UIControlStateNormal];
                [expCell.setStatusBtn setTitle:@"Waiting" forState:UIControlStateNormal];
                break;
            case 1:
                [expCell.setStatusBtn setTitleColor:[[AppManager sharedManager] yellowColor] forState:UIControlStateNormal];
                [expCell.setStatusBtn setTitle:@"Accessing" forState:UIControlStateNormal];
                break;
            case 2:
                [expCell.setStatusBtn setTitleColor:[[AppManager sharedManager] appThemeColor] forState:UIControlStateNormal];
                [expCell.setStatusBtn setTitle:@"Completed" forState:UIControlStateNormal];
                break;
            default:
                [expCell.setStatusBtn setTitleColor:[[AppManager sharedManager] placeholderColor] forState:UIControlStateNormal];
                [expCell.setStatusBtn setTitle:@"Please Set Status" forState:UIControlStateNormal];
                break;
        }
    }else{
        MSHCheckBoxCell* c=[tableView dequeueReusableCellWithIdentifier:@"MSHCheckBoxCell"];
        [AppManager layoutCellIfNeeded:cell tableView:tableView];
        WWAAStatus *status = [_checkBoxes objectAtIndex:indexPath.row];
        c.title.text=status.markup;
        cell=c;
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _checkBoxes.count;
}
@end
