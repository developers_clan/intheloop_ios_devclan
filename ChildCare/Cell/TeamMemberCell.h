//
//  TeamMemberCell.h
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Team.h"
#import "AppManager.h"
#import "TeamAgencyLogoCell.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TeamOffice.h"


@class TeamMemberCell;
@protocol CellInteractionCallBackDelegate <NSObject>

@required
-(void)cell:(TeamMemberCell*)cell addRemoveButtonPressedWithIndexPath:(NSIndexPath*)indexPath;
-(void)cell:(TeamMemberCell*)cell phonePressedWithIndexPath:(NSIndexPath*)indexPath;
-(void)cell:(TeamMemberCell*)cell emailPressedWithIndexPath:(NSIndexPath*)indexPath;
-(void)cell:(TeamMemberCell*)cell toggleExpansionButtonPressedWithIndexPath:(NSIndexPath*)indexPath;
-(void)cell:(TeamMemberCell*)cell editContactPressedWithIndexPath:(NSIndexPath*)indexPath;

@optional
-(void)cell:(TeamMemberCell*)cell imgUpload:(NSIndexPath*)indexPath;

@end


@interface TeamMemberCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *agencyColVuTop;
@property (strong,nonatomic)    UILongPressGestureRecognizer* lngPress;
@property (weak, nonatomic) IBOutlet UILabel *agencyLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *agenciesCollVuWdt;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locateWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locateLeading;
-(void)reloadData;
@property (weak, nonatomic) IBOutlet UIView *contVu;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *discipline;
@property (strong, nonatomic) Team* teamAPI;
@property (weak, nonatomic) IBOutlet UIButton *toggleExpansionBtn;
@property (weak, nonatomic) IBOutlet UIButton *addRemoveBtn;
@property (weak,nonatomic) id<CellInteractionCallBackDelegate>delegate;
@property (strong,nonatomic) NSIndexPath* indexPath;
- (IBAction)toggleExpansionAction:(id)sender;
- (IBAction)addRemoveAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *emailBtn;
@property (weak, nonatomic) IBOutlet UIButton *thrdBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *agenciesCollVuHghtCost;

@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UICollectionView *agenciesCollectionVu;
- (IBAction)callAction:(id)sender;
- (IBAction)emailAction:(id)sender;
- (IBAction)locateAction:(id)sender;
@end
