//
//  ExpandedMemberCell.h
//  ChildCare
//
//  Created by Devclan on 19/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamMemberCell.h"
@interface ExpandedMemberCell : TeamMemberCell<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *officesTable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *officeHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailsHeightConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phonesTableVuHeight;
-(CGFloat)cellHeight;
@property (weak, nonatomic) IBOutlet UITableView *phonesTable;

@property (weak, nonatomic) IBOutlet UIImageView *officesIcon;
-(void)reloadData;
@property (weak, nonatomic) IBOutlet UIImageView *phonesIcon;
@property (weak, nonatomic) IBOutlet UIImageView *emailsIcon;
@property (weak, nonatomic) IBOutlet UITableView *emailsTable;
@property (weak, nonatomic) IBOutlet UIView *telUnderline;
@property (weak, nonatomic) IBOutlet UIView *emailUnderline;

@end
