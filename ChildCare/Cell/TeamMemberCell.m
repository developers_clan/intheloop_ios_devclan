//
//  TeamMemberCell.m
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "TeamMemberCell.h"
#import <Google/Analytics.h>

@implementation TeamMemberCell

- (void)awakeFromNib {
    // Initialization code
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    _img.userInteractionEnabled = YES;
    _img.layer.cornerRadius = _img.frame.size.height/2.0;
    _img.clipsToBounds=YES;
    [_img addGestureRecognizer:singleTap];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    _lngPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(editContact:)];
    _lngPress.minimumPressDuration=1.0;
    
//    [self customizeButton:_callBtn];
//    [self customizeButton:_emailBtn];
//    [self customizeButton:_locateBtn];

    
//    UITapGestureRecognizer *phoneTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(phoneTapping:)];
//    [phoneTap setNumberOfTapsRequired:1];
//    [_contact addGestureRecognizer:phoneTap];
    
//    UITapGestureRecognizer *emailTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emailTapping:)];
//    [emailTap setNumberOfTapsRequired:1];
//    [_email addGestureRecognizer:emailTap];
}
-(void)editContact:(UILongPressGestureRecognizer *)gestureRecognizer{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan){
        [self.delegate cell:self editContactPressedWithIndexPath:_indexPath];
    }
    
}
-(void)reloadData{
    [_agenciesCollectionVu reloadData];
}
-(void)customizeButton:(UIView*)vu{
    vu.layer.cornerRadius=_callBtn.frame.size.width/2.0;
    vu.layer.borderColor=[[AppManager sharedManager]appThemeColor].CGColor;
    vu.layer.borderWidth=2;
    vu.clipsToBounds=YES;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected==YES) {
        _contVu.backgroundColor=[UIColor lightGrayColor];
        if (_addRemoveBtn.tag==0) {
            [_addRemoveBtn setImage:[UIImage imageNamed:@"my_team_icon_plus_green"] forState:UIControlStateNormal];
        }else{
            [_addRemoveBtn setImage:[UIImage imageNamed:@"my_team_icon_minus_selected"] forState:UIControlStateNormal];
        }
    }else{
        _contVu.backgroundColor=[UIColor colorWithWhite:246/255.0 alpha:1];
        if (_addRemoveBtn.tag==0) {
            [_addRemoveBtn setImage:[UIImage imageNamed:@"my_team_icon_plus"] forState:UIControlStateNormal];
        }else{//added
            [_addRemoveBtn setImage:[UIImage imageNamed:@"my_team_icon_minus"] forState:UIControlStateNormal];
            
        }
    }
    
    // Configure the view for the selected state
}
#pragma mark - CollectionViewDelegates
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    TeamAgencyLogoCell* cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"TeamAgencyLogoCell" forIndexPath:indexPath];
    AgencyImage* ta = [self.teamAPI.agencyThumbnails objectAtIndex:indexPath.row];
    //    Agency* agency = [[AppManager sharedManager] agencyForId:ta.agency.nId];
    //    if (agency!=nil) {
    //        cell.img.backgroundColor=agency.agencyColor;
    //    }
    //    if (agency!=nil) {
    //        [cell.img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",API_BASE_ADDRESS,API_FILES,ta.fileName]]
    //                        placeholderImage:[UIImage new]];
    
    [cell.img sd_setImageWithURL:[NSURL URLWithString:ta.uri]
     
                placeholderImage:[UIImage new]];
    //        cell.img.layer.borderColor=agency.agencyColor.CGColor;
    //    }
    return cell;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.teamAPI.agencyThumbnails.count;
}

#pragma mark - actions
- (IBAction)toggleExpansionAction:(id)sender {
    [_delegate cell:self toggleExpansionButtonPressedWithIndexPath:_indexPath];
}

- (IBAction)addRemoveAction:(id)sender {
    [_delegate cell:self addRemoveButtonPressedWithIndexPath:_indexPath];
}

-(void)singleTapping:(UIGestureRecognizer *)recognizer
{
    [_delegate cell:self imgUpload:_indexPath];
}

-(void)phoneTapping:(UIGestureRecognizer *)recognizer
{
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                        action:kActTeamContactPhonePressed
                                                                                         label:nil
                                                                                         value:nil] build]];
    [_delegate cell:self phonePressedWithIndexPath:_indexPath];
}

-(void)emailTapping:(UIGestureRecognizer *)recognizer
{
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                        action:kActTeamContactEmailPressed
                                                                                         label:nil
                                                                                         value:nil] build]];
    [_delegate cell:self emailPressedWithIndexPath:_indexPath];
}

- (IBAction)callAction:(id)sender {
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                        action:kActTeamContactPhonePressed
                                                                                         label:nil
                                                                                         value:nil] build]];
    if (_teamAPI.phones.firstObject) {
        TeamPhone* t = _teamAPI.phones.firstObject;
        NSString* phn = t.number;
//        phn=[phn stringByReplacingOccurrencesOfString:@"-" withString:@""];
        if (phn!=nil &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@;%@",phn,t.ext]]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@;%@",phn,t.ext]]];
        }
    }else{
        [_delegate cell:self phonePressedWithIndexPath:_indexPath];
    }
    
}

- (IBAction)emailAction:(id)sender {
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                        action:kActTeamContactEmailPressed
                                                                                         label:nil
                                                                                         value:nil] build]];
    if (_teamAPI.emails.firstObject) {
        TeamEmail* t = _teamAPI.emails.firstObject;
        NSString* email = t.value;
        if (email!=nil &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto://%@",email]]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto://%@",email]]];
        }
    }else{
        [_delegate cell:self emailPressedWithIndexPath:_indexPath];
    }
}

- (IBAction)locateAction:(id)sender {
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                        action:kActTeamContactMapPressed
                                                                                         label:nil
                                                                                         value:nil] build]];
    UIButton* b = sender;
    if (b.tag==1) {
        [self.delegate cell:self editContactPressedWithIndexPath:_indexPath];
    }else{
        if (_teamAPI.offices.firstObject) {
            TeamOffice* t = _teamAPI.offices.firstObject;
            NSString* add = t.office;
            NSString* url = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@",[AppManager urlencode:add]];
            if (add!=nil &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            }
        }
    }
   

}
@end
