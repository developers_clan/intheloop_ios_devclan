//
//  MenuCell.h
//  ChildCare
//
//  Created by Devclan on 23/12/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *lbl;

@end
