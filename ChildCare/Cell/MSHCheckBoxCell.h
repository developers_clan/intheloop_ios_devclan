//
//  MSHCheckBoxCell.h
//  ChildCare
//
//  Created by Devclan on 31/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MSHCheckBoxCell;
@protocol StatusUpdateCallBackDelegate <NSObject>
@required
-(void)updatedWithProgramIndex:(int)programIndex statusIndex:(int)statusIndex completionStatusIndex:(int)completionStatusIndex;
@optional
-(void)tableHeightChangedTo:(int)height indexPath:(NSIndexPath*)indexPath;
@end




@interface MSHCheckBoxCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak,nonatomic) id<StatusUpdateCallBackDelegate>delegate;
@property (strong,nonatomic) NSIndexPath* indexPath;
@property (strong,nonatomic) NSIndexPath* programIndexPath;
@end
