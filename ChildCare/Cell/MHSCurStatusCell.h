//
//  MHSCurStatusCellTableViewCell.h
//  ChildCare
//
//  Created by Devclan on 30/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MHSCurStatusCell;
@protocol MSHCellInteractionCallBackDelegate <NSObject>

@required
-(void)cell:(MHSCurStatusCell*)cell changeStatusClicked:(NSIndexPath*)indexPath;
@end

@interface MHSCurStatusCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *programTitle;
@property (weak, nonatomic) IBOutlet UIButton *changeStatusBtn;
@property (weak, nonatomic) IBOutlet UILabel *statusTitle;
@property (weak, nonatomic) IBOutlet UILabel *stage;
@property (weak, nonatomic) IBOutlet UIView *stageBg;

@property (weak,nonatomic) id<MSHCellInteractionCallBackDelegate>delegate;
@property (strong,nonatomic) NSIndexPath* indexPath;
-(CGFloat)cellHeight;
@end
