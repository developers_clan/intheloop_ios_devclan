//
//  ProgramCell.m
//  ChildCare
//
//  Created by Devclan on 31/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "ProgramCell.h"

@implementation ProgramCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
