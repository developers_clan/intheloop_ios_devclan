//
//  MSHCheckBoxCell.m
//  ChildCare
//
//  Created by Devclan on 31/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "MSHCheckBoxCell.h"

@implementation MSHCheckBoxCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
