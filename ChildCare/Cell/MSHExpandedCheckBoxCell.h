//
//  MSHExpandedCheckBoxCell.h
//  ChildCare
//
//  Created by Devclan on 31/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "MSHCheckBoxCell.h"
#import "PopupViewController.h"
//#import <IGLDropDownMenu.h>

@interface MSHExpandedCheckBoxCell : MSHCheckBoxCell<PopDelegate>//<IGLDropDownMenuDelegate>
@property (weak, nonatomic) IBOutlet UIView *dropBoxVu;
//@property (strong, nonatomic) IGLDropDownMenu *dropDownMenu;
- (IBAction)setStatusAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *setStatusBtn;

@property int dropdownIndex;
@end
