//
//  HistoryCellTableViewCell.h
//  ChildCare
//
//  Created by Devclan on 30/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StatusHistoryObj.h"
@class HistoryCell;
@protocol HistoryCellInteractionDelegate
@required
-(void)cell:(HistoryCell*)cell selectedDate:(NSDate*)date;

@end

@interface HistoryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *stage;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) StatusHistoryObj* histObj;
@property (strong,nonatomic)id<HistoryCellInteractionDelegate>historyDelegate;
- (IBAction)editAction:(UIButton *)sender;

@end
