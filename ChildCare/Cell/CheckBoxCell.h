//
//  CheckBoxCell.h
//  ChildCare
//
//  Created by Devclan on 29/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckBoxCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *lbl;

@end
