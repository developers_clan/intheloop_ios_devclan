//
//  ExpandedMemberCell.m
//  ChildCare
//
//  Created by Devclan on 19/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "ExpandedMemberCell.h"
#import "TeamPhone.h"
#import "AppManager.h"
#import "TeamAgencyLogoCell.h"
#import "Agency.h"
#import "TeamAgency.h"
#import "TeamOffice.h"
#import "TeamPhoneCell.h"


@implementation ExpandedMemberCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.img.layer.cornerRadius = self.img.frame.size.height/2.0;
    self.img.clipsToBounds=YES;
    self.selectionStyle=UITableViewCellSelectionStyleNone;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TeamPhoneCell* ce=[tableView dequeueReusableCellWithIdentifier:@"TeamPhoneCell"];
    [AppManager layoutCellIfNeeded:ce tableView:tableView];
    if (tableView==_phonesTable) {
        TeamPhone* ph = [self.teamAPI.phones objectAtIndex:indexPath.row];
        if (ph.ext.length>0) {
            ce.lbl.text = [NSString stringWithFormat:@"%@%@%@",ph.number,@" Ext. ",ph.ext];
        }else{
            ce.lbl.text = ph.number;
        }
    }else if (tableView == _emailsTable){
        TeamEmail* ph = [self.teamAPI.emails objectAtIndex:indexPath.row];
        ce.lbl.text = ph.value;
    }else if(tableView == _officesTable){
        TeamOffice* ofc = [self.teamAPI.offices objectAtIndex:indexPath.row];
        ce.lbl.text = ofc.office;
    }
    return ce;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView==_phonesTable) {
        return self.teamAPI.phones.count;
    }else if (tableView == _emailsTable){
        return self.teamAPI.emails.count;
    }else if(tableView == _officesTable){
        return self.teamAPI.offices.count;
    }
    return 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==_phonesTable) {
        TeamPhone* ph = [self.teamAPI.phones objectAtIndex:indexPath.row];
        NSString *phn = ph.number;
//      phn=[ph.number stringByReplacingOccurrencesOfString:@"-" withString:@""];
        if (phn!=nil &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@;%@",phn,ph.ext]]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@;%@",phn,ph.ext]]];
        }
    }else if (tableView == _emailsTable){
        TeamEmail* em = [self.teamAPI.emails objectAtIndex:indexPath.row];
        NSString* email = em.value;
        if (email!=nil &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto://%@",email]]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto://%@",email]]];
        }
    }else if(tableView == _officesTable){
        TeamOffice* t = [self.teamAPI.offices objectAtIndex:indexPath.row];
        NSString* add = t.office;
        NSString* url = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@",[AppManager urlencode:add]];
        if (add!=nil &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==_phonesTable) {
        return 15;
    }else if (tableView == _emailsTable){
        return 15;
    }else if(tableView == _officesTable){
        return 50;
    }
    return 0;
}
-(void)reloadData{
    [_phonesTable reloadData];
    [_officesTable reloadData];
    [_emailsTable reloadData];
}

-(CGFloat)cellHeight{
    int height = 154;//109
    if (self.teamAPI.offices.count>0) {
        height += self.teamAPI.offices.count*50+8;
    }
    if (self.teamAPI.emails.count>0) {
        height += self.teamAPI.emails.count*15+16;
    }
    if (self.teamAPI.phones.count>0) {
        height += self.teamAPI.phones.count*15+16;
    }
    return height;
}
@end
