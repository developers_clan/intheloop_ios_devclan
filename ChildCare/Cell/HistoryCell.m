//
//  HistoryCellTableViewCell.m
//  ChildCare
//
//  Created by Devclan on 30/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "HistoryCell.h"
#import <ActionSheetPicker.h>
#import "AppDelegate.h"
#import <Google/Analytics.h>
#import "Constants.h"

@implementation HistoryCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)editAction:(UIButton *)sender {
//    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatStatusHistory
//                                                                                        action:kActEditStatusHistoryDate
//                                                                                         label:nil
//                                                                                         value:nil] build]];
    ActionSheetDatePicker* picker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select date" datePickerMode:UIDatePickerModeDate selectedDate:_histObj.date doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        NSDate* dt = (NSDate*)selectedDate;
        self.histObj.date = dt;
        AppDelegate* d = [[UIApplication sharedApplication] delegate];
        [d.managedObjectContext save:NULL];
        [_historyDelegate cell:self selectedDate:dt];
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self];
    
    [picker showActionSheetPicker];
}
@end
