//
//  ProgramCell.h
//  ChildCare
//
//  Created by Devclan on 31/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "ProgramStatusPath.h"
@interface ProgramCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (strong,nonatomic)ProgramStatusPath* path;
@property (strong,nonatomic) NSIndexPath* indexPath;
@end
