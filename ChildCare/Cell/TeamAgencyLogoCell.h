//
//  AgencyColorCell.h
//  ChildCare
//
//  Created by Devclan on 19/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamAgencyLogoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;

@end
