//
//  AppDelegate.m
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "AppDelegate.h"
#import <IQKeyboardManager.h>
#import "AppManager.h"
#import <Google/Analytics.h>

//#import <OBDragDropManager.h>
#import "UIImage+Additions(Additions).h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "Agency.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <SDWebImageManager.h>
#import "HomeText.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //-----------google analytics-----------//
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    //-----------google analytics-----------//
    
    
    [[IQKeyboardManager sharedManager]setEnable:YES];
    [[SDWebImageManager sharedManager] imageCache].maxCacheAge=kAppDataRefreshInterval;
    
    //    OBDragDropManager *manager = [OBDragDropManager sharedManager];
    //    [manager prepareOverlayWindowUsingMainWindow:self.window];
    
    [Fabric with:@[[Crashlytics class]]];
    
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Lato-Black" size:20.0], NSFontAttributeName, nil]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -110)
                                                         forBarMetrics:UIBarMetricsDefault];
    UIImage *image = [[UIImage imageNamed:@"back_arrow"] imageScaledToSize:CGSizeMake(20, 20)];
    [UINavigationBar appearance].backIndicatorImage = image;
    [UINavigationBar appearance].backIndicatorTransitionMaskImage = image;
    self.window.backgroundColor=[UIColor colorWithPatternImage:[[UIImage imageNamed:@"meu_bar_background"] imageScaledToSize:self.window.frame.size]];
    [self fetchAndLoadAgenciesData];
    [self fetchAndLoadHomeTextData];
    
    //    [self prepareImagesData];
    return YES;
}
-(void)prepareImagesData{
    NSArray* urls = @[@"http://intheloop.dreamhosters.com/sites/default/files/ctn_banner_image.png",@"http://intheloop.dreamhosters.com/sites/default/files/ctn.png",@"http://intheloop.dreamhosters.com/sites/default/files/eis_agency_page_banner.png",@"http://intheloop.dreamhosters.com/sites/default/files/YR_0.png",@"http://intheloop.dreamhosters.com/sites/default/files/cdp_banner_images.png",@"http://intheloop.dreamhosters.com/sites/default/files/MSH.png"];
    for (NSString* url in urls) {
        if ([[AppManager sharedManager] imageFromDBWithURL:url]==nil) {
            UIImage *img;
            if ([url isEqualToString:@"http://intheloop.dreamhosters.com/sites/default/files/ctn_banner_image.png"]) {
                img = [UIImage imageNamed:@"banner5.png"];
            }else if ([url isEqualToString:@"http://intheloop.dreamhosters.com/sites/default/files/ctn.png"]) {
                img = [UIImage imageNamed:@"logo5.png"];
            }else if ([url isEqualToString:@"http://intheloop.dreamhosters.com/sites/default/files/eis_agency_page_banner.png"]) {
                img = [UIImage imageNamed:@"banner28.png"];
            }else if ([url isEqualToString:@"http://intheloop.dreamhosters.com/sites/default/files/YR_0.png"]) {
                img = [UIImage imageNamed:@"logo28.png"];
            }else if ([url isEqualToString:@"http://intheloop.dreamhosters.com/sites/default/files/cdp_banner_images.png"]) {
                img = [UIImage imageNamed:@"banner6.png"];
            }else if ([url isEqualToString:@"http://intheloop.dreamhosters.com/sites/default/files/MSH.png"]) {
                img = [UIImage imageNamed:@"banner6.png"];
            }
            [[AppManager sharedManager] insertOrUpdateImage:UIImagePNGRepresentation(img) url:url];
        }
    }
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark -
#pragma mark Core Data stack

- (NSManagedObjectContext *) managedObjectContext {
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"<Project Name>.sqlite"]];
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                  initWithManagedObjectModel:[self managedObjectModel]];
    if(![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                 configuration:nil URL:storeUrl options:nil error:&error]) {
        /*Error for store creation should be handled in here*/
    }
    
    return persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - loading
-(void)fetchAndLoadAgenciesData{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kGetAgenciesAPI];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSDate* lastFetchedTime=nil;
    double diff = 0.0;
    if (jsonResp!=nil) {
        lastFetchedTime=jsonResp.date;
        diff = [[NSDate date]timeIntervalSinceDate:lastFetchedTime];
    }
    
    
    
    if (lastFetchedTime==nil || diff>kAppDataRefreshInterval) {
        
        //        MBProgressHUD *p=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //        p.labelText=@"Please wait...";
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
        //        manager.responseSerializer=[AFOnoResponseSerializer XMLResponseSerializer];
        [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            BOOL b=YES;
            //            for (ONOXMLElement *element in [responseDocument XPath:@"//item"]) {
            //                NSLog(@"%@", element);
            //            }
            
            //            [p hide:YES];
            NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:responseObject];
            [[AppManager sharedManager] insertOrUpdateResponse:myData url:urlStr];
            [self reloadAgenciesDataLocally:urlStr];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            [p hide:YES];
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }];
    }else{
        [self reloadAgenciesDataLocally:urlStr];
    }
    
}

-(void)reloadAgenciesDataLocally:(NSString*)key{
    
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kGetAgenciesAPI];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSArray* agenciesArray = [NSKeyedUnarchiver unarchiveObjectWithData:jsonResp.value];
    if (agenciesArray==nil) {
        return;
    }
    [[[AppManager sharedManager]agencies] removeAllObjects];
    for (NSDictionary* opt in agenciesArray) {
        Agency* a=[[Agency alloc] initWithDictionary:opt];
        
        //        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:a.image.uri] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        //
        //        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        //
        //        }];
        
        SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
        [downloader downloadImageWithURL:[NSURL URLWithString:a.image.uri]
                                 options:0
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    
                                }
                               completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                   if (image && finished) {
                                       // do something with image
                                       
                                       NSArray *parts = [a.image.uri componentsSeparatedByString:@"/"];
                                       NSString *filename = [parts lastObject];
                                       
                                       [AppManager saveImageLocal:image imgName:filename];
                                       
                                       
                                   }
                               }];
        
        [[[AppManager sharedManager]agencies] addObject:a];
    }
    //moved in contactsMethod
    //    _heightConst.constant=self.headerContainer.frame.size.height+ 112+100*[self tableView:nil numberOfRowsInSection:0];
    //    [_tableView reloadData];
    [self fetchAndLoadAgenciesContactData];
}
-(void)fetchAndLoadAgenciesContactData{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kAgenciesContacts];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSDate* lastFetchedTime=nil;
    double diff = 0.0;
    if (jsonResp!=nil) {
        lastFetchedTime=jsonResp.date;
        diff = [[NSDate date]timeIntervalSinceDate:lastFetchedTime];
    }
    
    
    
    if (lastFetchedTime==nil || diff>kAppDataRefreshInterval) {
        
        //        MBProgressHUD *p=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //        p.labelText=@"Please wait...";
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
        //        manager.responseSerializer=[AFOnoResponseSerializer XMLResponseSerializer];
        [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            BOOL b=YES;
            //            for (ONOXMLElement *element in [responseDocument XPath:@"//item"]) {
            //                NSLog(@"%@", element);
            //            }
            
            //            [p hide:YES];
            NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:responseObject];
            [[AppManager sharedManager] insertOrUpdateResponse:myData url:urlStr];
            [self reloadAgenciesContactsDataLocally:urlStr];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            [p hide:YES];
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }];
    }else{
        [self reloadAgenciesContactsDataLocally:urlStr];
    }
    
}
-(void)reloadAgenciesContactsDataLocally:(NSString*)key{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kAgenciesContacts];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSArray* agenciesArray = [NSKeyedUnarchiver unarchiveObjectWithData:jsonResp.value];
    if (agenciesArray==nil) {
        return;
    }
    [[[AppManager sharedManager]agenciesContacts] removeAllObjects];
    for (NSDictionary* opt in agenciesArray) {
        AgencyContact* a=[[AgencyContact alloc] initWithDictionary:opt];
        //        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:a.agencyImage.uri] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        //
        //        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        //
        //        }];
        [[[AppManager sharedManager]agenciesContacts] addObject:a];
    }
    [[AppManager sharedManager] synchronizeAgenicesAndAgenciesContacts];
    [self fetchAndLoadTeamsData];
}


-(void)fetchAndLoadHomeTextData{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kGetHomeAPI];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSDate* lastFetchedTime=nil;
    double diff = 0.0;
    if (jsonResp!=nil) {
        lastFetchedTime=jsonResp.date;
        diff = [[NSDate date]timeIntervalSinceDate:lastFetchedTime];
    }
    
    
    
    if (lastFetchedTime==nil || diff>kAppDataRefreshInterval) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
        [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:responseObject];
            [[AppManager sharedManager] insertOrUpdateResponse:myData url:urlStr];
            [self reloadHomeTextDataLocally:urlStr];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }];
    }else{
        [self reloadHomeTextDataLocally:urlStr];
    }
    
}
-(void)reloadHomeTextDataLocally:(NSString*)key{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kGetHomeAPI];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSArray* agenciesArray = [NSKeyedUnarchiver unarchiveObjectWithData:jsonResp.value];
    if (agenciesArray==nil) {
        return;
    }
    [[[AppManager sharedManager]homeTextAPI] removeAllObjects];
    for (NSDictionary* opt in agenciesArray) {
        HomeText* a=[[HomeText alloc] initWithDictionary:opt];
        [[[AppManager sharedManager]homeTextAPI] addObject:a];
    }
    
    
}


-(void)fetchAndLoadTeamsData{
    
    
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kGetTeamsAPI];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSDate* lastFetchedTime=nil;
    double diff = 0.0;
    if (jsonResp!=nil) {
        lastFetchedTime=jsonResp.date;
        diff = [[NSDate date]timeIntervalSinceDate:lastFetchedTime];
    }
    
    
    if (lastFetchedTime==nil || diff>kAppDataRefreshInterval) {
        
        //        MBProgressHUD *p=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //        p.labelText=@"Please wait...";
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
        [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            [p hide:YES];
            NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:responseObject];
            [[AppManager sharedManager] insertOrUpdateResponse:myData url:urlStr];
            [self reloadTeamsDataLocally:urlStr];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            [p hide:YES];
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }];
    }else{
        [self reloadTeamsDataLocally:urlStr];
    }
}

-(void)reloadTeamsDataLocally:(NSString*)key{
    
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:key];
    if (jsonResp==nil) {
        return;
    }
    NSArray* teamsArray = [NSKeyedUnarchiver unarchiveObjectWithData:jsonResp.value];
    if (teamsArray==nil) {
        return;
    }
    [[[AppManager sharedManager]teamsAPI] removeAllObjects];
    NSMutableSet* set = [NSMutableSet new];
    for (NSDictionary* opt in teamsArray) {
        Team* a=[[Team alloc] initWithDictionary:opt];
        for (AgencyImage* im in a.agencyThumbnails) {
            [set addObject:im.uri];
        }
        [[[AppManager sharedManager]teamsAPI] addObject:a];
    }
    for (NSString* img in set) {
        //        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:img] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        //
        //        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        //
        //        }];
        
    }
    BOOL termsAccepted=NO;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kTermsAccepted]) {
        termsAccepted = [[NSUserDefaults standardUserDefaults]boolForKey:kTermsAccepted];
    }
    if (termsAccepted==YES) {
        UIViewController* vc=[self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"TabsController"];
        [[[[UIApplication sharedApplication] delegate] window] setRootViewController:vc];
    }else{
        UIViewController* vc=[self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"TermsNavigationController"];
        [[[[UIApplication sharedApplication] delegate] window] setRootViewController:vc];
    }
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
}


@end
