//
//  ImageCache+CoreDataProperties.h
//  ChildCare
//
//  Created by Devclan on 27/01/2016.
//  Copyright © 2016 Devclan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ImageCache.h"

NS_ASSUME_NONNULL_BEGIN

@interface ImageCache (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *url;
@property (nullable, nonatomic, retain) NSData *data;
@property (nullable, nonatomic, retain) NSDate *date;

@end

NS_ASSUME_NONNULL_END
