//
//  WYWStatus.h
//  ChildCare
//
//  Created by Devclan on 27/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppManager.h"
@interface WYWStatus : NSObject
@property (nonatomic,strong)NSString* markup;
- (instancetype)initWithDictionary:(NSDictionary*)dic;
@end
