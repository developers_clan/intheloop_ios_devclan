//
//  DEMORightMenuViewController.m
//  RESideMenuStoryboards
//
//  Created by Roman Efimov on 2/11/14.
//  Copyright (c) 2014 Roman Efimov. All rights reserved.
//

#import "MenuViewController.h"
#import "WebViewController.h"
#import "Agency.h"
#import "WWAAMSHViewController.h"
#import "AppManager.h"
#import "MenuCell.h"
#import "AgencyWebsite.h"
#import "ProgramStatusPath.h"
#import "MSHStatusViewController.h"
#import "ChildSelector.h"
#import <Google/Analytics.h>
#import "Constants.h"

@interface MenuViewController ()<ChildSelectionCompletionDelegate>


@end

@implementation MenuViewController{
    NSArray *titles;
    NSArray *icons;
    __weak IBOutlet UIButton *topLeft;
    __weak IBOutlet UIButton *topRight;
    __weak IBOutlet UIButton *bottomRight;
    __weak IBOutlet UIButton *bottomLeft;
    
    __weak IBOutlet UIButton *firstButton;
    __weak IBOutlet UIButton *secondButton;
    __weak IBOutlet UIButton *thirdButton;
    __weak IBOutlet UIButton *fourthButton;
    
    
    NSIndexPath* selectedPath;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     titles = @[@"MY SERVICES", @"WHILE YOU WAIT"];
     icons = @[@"menu_bar_vector_smat_object", @"while_you_wait"];

    [self makeRound:topLeft];
    [self makeRound:topRight];
    [self makeRound:bottomLeft];
    [self makeRound:bottomRight];
    
}
-(void)makeRound:(UIButton*)btn{
    btn.layer.cornerRadius=btn.frame.size.height/2.0;
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 2;
}
-(void)updateUI{
    UINavigationController* nc=(UINavigationController*)_menuRootViewController.contentViewController;
    if (![[nc.viewControllers lastObject] isKindOfClass:[WebViewController class]]) {
        return;
    }
    WebViewController* agencyWebVC=[nc.viewControllers lastObject];
    Agency* agency=agencyWebVC.agency;
    
    firstButton.hidden=topLeft.hidden=agency.agencyContact.websites.count<=0;
    secondButton.hidden=topRight.hidden=agency.agencyContact.websites.count<=1;
    thirdButton.hidden=bottomLeft.hidden=agency.agencyContact.websites.count<=2;
    fourthButton.hidden=bottomRight.hidden=agency.agencyContact.websites.count<=3;
    
    if (topLeft.hidden==NO) {
        AgencyWebsite* wc = [agency.agencyContact.websites objectAtIndex:0];
        [topLeft setImage:[self iconForTitle:wc.title] forState:UIControlStateNormal];
        [firstButton setTitle:wc.title forState:UIControlStateNormal];
    }
    if (topRight.hidden==NO) {
        AgencyWebsite* wc = [agency.agencyContact.websites objectAtIndex:1];
        [topRight setImage:[self iconForTitle:wc.title] forState:UIControlStateNormal];
        [secondButton setTitle:wc.title forState:UIControlStateNormal];
    }
    if (bottomLeft.hidden==NO) {
        AgencyWebsite* wc = [agency.agencyContact.websites objectAtIndex:2];
        [bottomLeft setImage:[self iconForTitle:wc.title] forState:UIControlStateNormal];
        [thirdButton setTitle:wc.title forState:UIControlStateNormal];
    }
    if (bottomRight.hidden==NO) {
        AgencyWebsite* wc = [agency.agencyContact.websites objectAtIndex:3];
        [bottomRight setImage:[self iconForTitle:wc.title] forState:UIControlStateNormal];
        [fourthButton setTitle:wc.title forState:UIControlStateNormal];
    }
}
-(UIImage*)iconForTitle:(NSString*)title{
    if ([title isEqualToString:@"Facebook"]) {
        return [UIImage imageNamed:@"meu_bar_social_media_fb"];
    }
    if ([title isEqualToString:@"Twitter"]) {
        return [UIImage imageNamed:@"menu_bar_twt"];
    }
    if ([title isEqualToString:@"YouTube"]) {
        return [UIImage imageNamed:@"meu_bar_social_media_youtube"];
    }
    if ([title isEqualToString:@"Pinterest"]) {
        return [UIImage imageNamed:@"meu_bar_social_media_pin"];
    }
    
    return nil;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (IBAction)socialBtnAction:(id)sender {
    
    
    UIButton* b = sender;
    UINavigationController* nc=(UINavigationController*)_menuRootViewController.contentViewController;
//    if (![[nc.viewControllers lastObject] isKindOfClass:[WebViewController class]]) {
//        return;
//    }
    WebViewController* agencyWebVC=[nc.viewControllers lastObject];
    Agency* agency=agencyWebVC.agency;
    AgencyWebsite *wc = [agency.agencyContact.websites objectAtIndex:b.tag];
    
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatSideMenu
                                                                                        action:[NSString stringWithFormat:@"%@ - %@",agency.mainDetails.title,wc.title]
                                                                                         label:nil
                                                                                         value:nil] build]];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:wc.url]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:wc.url]];
    }
}
#pragma mark -
#pragma mark ChildSelectionCompletion Delegate
-(void)viewController:(UIViewController *)viewController completedSelectionWithChild:(Child *)child{
    [self handleMenuClickWithIndexPath:(int)selectedPath.row];
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedPath = indexPath;
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UINavigationController* nc=(UINavigationController*)_menuRootViewController.contentViewController;
    WebViewController* agencyWebVC=[nc.viewControllers lastObject];
    Agency* agency=agencyWebVC.agency;
    if (indexPath.row==1) {
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatSideMenu
                                                                                            action:[NSString stringWithFormat:@"%@-%@",kActMenuSelectedWhileYouWait,agency.mainDetails.title]
                                                                                             label:nil
                                                                                             value:nil] build]];
        [self handleMenuClickWithIndexPath:(int)indexPath.row];
    }else{
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatSideMenu
                                                                                            action:[NSString stringWithFormat:@"%@-%@",kActMenuSelectedMyServices,agency.mainDetails.title]
                                                                                             label:nil
                                                                                             value:nil] build]];
        if ([[AppManager sharedManager] children].count<1) {
            [_menuRootViewController hideMenuViewController];
            if ([[AppManager sharedManager] tabsController]) {
                [[[AppManager sharedManager] tabsController] takeToAccountWithMessage:@"You have not setup your account. To proceed, please add your child/children."];
            }else{
                [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"You have not setup your account. To proceed, please add your child/children." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
            }
            return;
        }else if ([[AppManager sharedManager] children].count==1) {
            [_menuRootViewController hideMenuViewController];
            {
                [self handleMenuClickWithIndexPath:(int)indexPath.row];
            }
        }else{
            ChildSelector* selectVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChildSelector"];
            selectVC.completionDelegate=self;
            [self presentViewController:selectVC animated:YES completion:nil];
        }
    }
}
-(void)handleMenuClickWithIndexPath:(int)index{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        UINavigationController* nc=(UINavigationController*)_menuRootViewController.contentViewController;
        
        [self popAgencyHomeWithNavigationController:nc];
        
        UIViewController* vc=[nc.viewControllers lastObject];
        
        switch (index) {
            case 0:
            {
                WebViewController* agencyWebVC = (WebViewController*)vc;
                Agency* agency=agencyWebVC.agency;
                WWAAMSHViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WWAAMSHViewController"];
                vc.agency=agency;
                [vc reloadData];
                BOOL isAnyStatusSet=vc.indicesOfSetStatuses.count>0;
//                for (ProgramStatusPath* p in vc.programStatusesData) {
//                    if ([p.statusInd intValue]>=0) {
//                        isAnyStatusSet=YES;
//                        break;
//                    }
//                }
                [_menuRootViewController hideMenuViewController];
                if (isAnyStatusSet==YES) {
                    [nc pushViewController:vc animated:NO];
                }else{
                    [nc pushViewController:vc animated:NO];
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        MSHStatusViewController* vc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"MSHStatusViewController"];
                        vc2.agency=vc.agency;
                        vc2.checkBoxesData=vc.agency.wwaa;
                        vc2.expandedIndex=0;
                        [nc pushViewController:vc2 animated:NO];
                    }];
                    
                }
                break;
            }
            case 1:
            {
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    WebViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
                    vc.isWYW=YES;
                    UINavigationController* nc=(UINavigationController*)_menuRootViewController.contentViewController;
                    WebViewController* agencyWebVC=[nc.viewControllers lastObject];
                    vc.agency=agencyWebVC.agency;
                    [_menuRootViewController hideMenuViewController];
                    [nc pushViewController:vc animated:NO];
                }];
                
                break;
            }
            default:
                break;
        }

    }];
}

-(void)popAgencyHomeWithNavigationController:(UINavigationController*)nc{
    
    UIViewController* currentVc = [nc.viewControllers lastObject];
    while (!([currentVc isKindOfClass:[WebViewController class]] && ((WebViewController*)currentVc).isWYW==NO)) {
        [nc popViewControllerAnimated:NO];
        currentVc = [nc.viewControllers lastObject];
    }
    
}
#pragma mark -
#pragma mark UITableView Datasource




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
    
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell.selectedBackgroundView==nil) {
        UIView* v = [[UIView alloc] init];
        v.backgroundColor=[UIColor clearColor];
        cell.selectedBackgroundView = v;
    }
    
    
    
    cell.lbl.text = titles[indexPath.row];
    cell.img.image = [UIImage imageNamed:icons[indexPath.row]];
    
    return cell;
}


@end
