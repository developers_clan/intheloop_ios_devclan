//
//  FirstViewController.m
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "HomeViewController.h"
#import "AgencyCell.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "AppManager.h"
#import "Agency.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WebViewController.h"
#import "JSONResponse.h"
#import "AgencyContact.h"
#import "AppManager.h"
#import <Crashlytics/Crashlytics.h>
#import <Google/Analytics.h>
#import "HomeText.h"



@interface HomeViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *headerContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblText;

@end

@implementation HomeViewController{
    UIBezierPath *_drawPath;
    CALayer      *_pen;
    UIBezierPath *_penPath;
    CAShapeLayer *_rectLayer;
    NSLock *lock;
    BOOL locked;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchAndLoadAgenciesData];
    lock=[NSLock new];
    locked = NO;
    
    
   
    
    // Do any additional setup after loading the view, typically from a nib.
}




- (void) drawRectangleOutline:(AgencyCell*)cell indexPath:(NSIndexPath*)indexPath{
    if (locked==YES) {
        return;
    }
    [lock lock];
    locked=YES;
    _drawPath = [UIBezierPath bezierPath];
    CGRect fr = cell.animImg.frame;
    [_drawPath moveToPoint:CGPointMake(0, 2)];
    [_drawPath addLineToPoint:CGPointMake(fr.size.width-2, 2)];
    [_drawPath addLineToPoint:CGPointMake(fr.size.width-2, fr.size.height-2)];
    [_drawPath addLineToPoint:CGPointMake(0, fr.size.height-2)];
    Agency* a=[[[AppManager sharedManager]agencies] objectAtIndex:indexPath.row];
    _rectLayer = [[CAShapeLayer alloc] init];
    _rectLayer.path = _drawPath.CGPath;
    _rectLayer.strokeColor = a.agencyColor.CGColor;
    _rectLayer.lineWidth = cell.strip.frame.size.width;
    _rectLayer.fillColor = [UIColor clearColor].CGColor;
    _rectLayer.strokeEnd = 0.f;
    [cell.contVu.layer addSublayer:_rectLayer];
    
    
    _rectLayer.strokeEnd = 10.f;
    
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    [CATransaction setCompletionBlock:^{
        [_rectLayer removeFromSuperlayer];
            WebViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
            vc.isWYW=NO;
            vc.agency=a;
            [self.navigationController pushViewController:vc animated:YES];
        [lock unlock];
        locked=NO;
    }];
    anim.fromValue = (id)[NSNumber numberWithFloat:0];
    anim.toValue = (id)[NSNumber numberWithFloat:1.f];
    anim.duration = ANIMATION_DURATION;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [_rectLayer addAnimation:anim forKey:@"drawRectStroke"];
}


-(void)fetchAndLoadAgenciesData{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kGetAgenciesAPI];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSDate* lastFetchedTime=nil;
    double diff = 0.0;
    if (jsonResp!=nil) {
        lastFetchedTime=jsonResp.date;
        diff = [[NSDate date]timeIntervalSinceDate:lastFetchedTime];
    }
    

    
    if (lastFetchedTime==nil || diff>kAppDataRefreshInterval) {
        
        MBProgressHUD *p=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        p.labelText=@"Please wait...";
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
//        manager.responseSerializer=[AFOnoResponseSerializer XMLResponseSerializer];
        [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            BOOL b=YES;
//            for (ONOXMLElement *element in [responseDocument XPath:@"//item"]) {
//                NSLog(@"%@", element);
//            }
            
            [p hide:YES];
            NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:responseObject];
            [[AppManager sharedManager] insertOrUpdateResponse:myData url:urlStr];
            [self reloadAgenciesDataLocally:urlStr];
             
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [p hide:YES];
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }];
    }else{
        [self reloadAgenciesDataLocally:urlStr];
    }
    
}
-(void)reloadAgenciesDataLocally:(NSString*)key{
    
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kGetAgenciesAPI];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSArray* agenciesArray = [NSKeyedUnarchiver unarchiveObjectWithData:jsonResp.value];
    if (agenciesArray==nil) {
        return;
    }
    [[[AppManager sharedManager]agencies] removeAllObjects];
    for (NSDictionary* opt in agenciesArray) {
        Agency* a=[[Agency alloc] initWithDictionary:opt];
        [[[AppManager sharedManager]agencies] addObject:a];
    }
    //moved in contactsMethod
//    _heightConst.constant=self.headerContainer.frame.size.height+ 112+100*[self tableView:nil numberOfRowsInSection:0];
//    [_tableView reloadData];
    [self fetchAndLoadAgenciesContactData];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _heightConst.constant=self.headerContainer.frame.size.height+112+_tableView.contentSize.height;
}
-(void)viewWillAppear:(BOOL)animated{
    
    //---------------googleAnalytics-----------//
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Agency Listing Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    //---------------googleAnalytics-----------//
    
    if([[[AppManager sharedManager] homeTextAPI] count]>0){
        [_lblText setText:[[[[AppManager sharedManager] homeTextAPI] objectAtIndex:0] homeText]];
    }
    
    [super viewWillAppear:animated];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AgencyCell* cell=[tableView dequeueReusableCellWithIdentifier:@"AgencyCell"];
    Agency* a=[[[AppManager sharedManager]agencies] objectAtIndex:indexPath.row];
    
//    if ([a.nid isEqualToString:@"5"]) {
//        cell.img.image=[UIImage imageNamed:@"contact_us_today_logo_ctn"];
//        cell.strip.backgroundColor = [[AppManager sharedManager] appThemeColor];
//    }else if ([a.nid isEqualToString:@"6"]) {
//        cell.img.image=[UIImage imageNamed:@"contact_us_today_logo_msh"];
//        cell.strip.backgroundColor = [[AppManager sharedManager] yellowColor];
//    }else{
//        cell.img.image=[UIImage imageNamed:@"contact_us_today_logo_yr"];
//        cell.strip.backgroundColor = [[AppManager sharedManager] greeColor];
//    }

//    [cell.img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",API_BASE_ADDRESS,API_FILES,a.image.fileName]]
//                      placeholderImage:[UIImage new]];
//    [cell.img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",API_BASE_ADDRESS,API_FILES,a.logo.fileName]]
//                    placeholderImage:[UIImage new]];
    [cell.img sd_setImageWithURL:[NSURL URLWithString:a.logo.uri]
                placeholderImage:[UIImage new]];
    cell.strip.backgroundColor = a.agencyColor;
    cell.contVu.layer.borderColor = a.agencyColor.CGColor;
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[AppManager sharedManager]agencies].count;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //---------------googleAnalytics-----------//
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    Agency* a=[[[AppManager sharedManager]agencies] objectAtIndex:indexPath.row];
    
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyListingScreen
                                                                                        action:[NSString stringWithFormat:@"Agency Listing - Agency selected: %@",a.mainDetails.title]
                                                                                         label:nil
                                                                                         value:nil] build]];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    //---------------googleAnalytics-----------//
    
    
    AgencyCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    [self drawRectangleOutline:cell indexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}
#pragma mark - contact communication
-(void)fetchAndLoadAgenciesContactData{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kAgenciesContacts];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSDate* lastFetchedTime=nil;
    double diff = 0.0;
    if (jsonResp!=nil) {
        lastFetchedTime=jsonResp.date;
        diff = [[NSDate date]timeIntervalSinceDate:lastFetchedTime];
    }
    
    
    
    if (lastFetchedTime==nil || diff>kAppDataRefreshInterval) {
        
        MBProgressHUD *p=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        p.labelText=@"Please wait...";
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
        //        manager.responseSerializer=[AFOnoResponseSerializer XMLResponseSerializer];
        [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            BOOL b=YES;
            //            for (ONOXMLElement *element in [responseDocument XPath:@"//item"]) {
            //                NSLog(@"%@", element);
            //            }
            
            [p hide:YES];
            NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:responseObject];
            [[AppManager sharedManager] insertOrUpdateResponse:myData url:urlStr];
            [self reloadAgenciesContactsDataLocally:urlStr];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [p hide:YES];
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }];
    }else{
        [self reloadAgenciesContactsDataLocally:urlStr];
    }
    
}
-(void)reloadAgenciesContactsDataLocally:(NSString*)key{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kAgenciesContacts];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSArray* agenciesArray = [NSKeyedUnarchiver unarchiveObjectWithData:jsonResp.value];
    if (agenciesArray==nil) {
        return;
    }
    [[[AppManager sharedManager]agenciesContacts] removeAllObjects];
    for (NSDictionary* opt in agenciesArray) {
        AgencyContact* a=[[AgencyContact alloc] initWithDictionary:opt];
        [[[AppManager sharedManager]agenciesContacts] addObject:a];
    }
    [[AppManager sharedManager] synchronizeAgenicesAndAgenciesContacts];
    _heightConst.constant=self.headerContainer.frame.size.height+ 112+100*[self tableView:nil numberOfRowsInSection:0];
    [_tableView reloadData];
    [self fetchAndLoadTeamsData];
}

-(void)fetchAndLoadHomeTextData{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kGetHomeAPI];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSDate* lastFetchedTime=nil;
    double diff = 0.0;
    if (jsonResp!=nil) {
        lastFetchedTime=jsonResp.date;
        diff = [[NSDate date]timeIntervalSinceDate:lastFetchedTime];
    }
    
    
    
    if (lastFetchedTime==nil || diff>kAppDataRefreshInterval) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
        [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:responseObject];
            [[AppManager sharedManager] insertOrUpdateResponse:myData url:urlStr];
            [self reloadHomeTextDataLocally:urlStr];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }];
    }else{
        [self reloadHomeTextDataLocally:urlStr];
    }
    
}
-(void)reloadHomeTextDataLocally:(NSString*)key{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kGetHomeAPI];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSArray* agenciesArray = [NSKeyedUnarchiver unarchiveObjectWithData:jsonResp.value];
    if (agenciesArray==nil) {
        return;
    }
    [[[AppManager sharedManager]homeTextAPI] removeAllObjects];
    for (NSDictionary* opt in agenciesArray) {
        HomeText* a=[[HomeText alloc] initWithDictionary:opt];
        [[[AppManager sharedManager]homeTextAPI] addObject:a];
    }
}


-(void)fetchAndLoadTeamsData{
    

    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kGetTeamsAPI];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSDate* lastFetchedTime=nil;
    double diff = 0.0;
    if (jsonResp!=nil) {
        lastFetchedTime=jsonResp.date;
        diff = [[NSDate date]timeIntervalSinceDate:lastFetchedTime];
    }
    
    
    if (lastFetchedTime==nil || diff>kAppDataRefreshInterval) {
        
        MBProgressHUD *p=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        p.labelText=@"Please wait...";
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
        [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [p hide:YES];
            NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:responseObject];
            [[AppManager sharedManager] insertOrUpdateResponse:myData url:urlStr];
            [self reloadTeamsDataLocally:urlStr];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [p hide:YES];
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }];
    }else{
        [self reloadTeamsDataLocally:urlStr];
    }
}

-(void)reloadTeamsDataLocally:(NSString*)key{
    
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:key];
    if (jsonResp==nil) {
        return;
    }
    NSArray* teamsArray = [NSKeyedUnarchiver unarchiveObjectWithData:jsonResp.value];
    if (teamsArray==nil) {
        return;
    }
    [[[AppManager sharedManager]teamsAPI] removeAllObjects];
    for (NSDictionary* opt in teamsArray) {
        Team* a=[[Team alloc] initWithDictionary:opt];
        [[[AppManager sharedManager]teamsAPI] addObject:a];
    }
}

@end
