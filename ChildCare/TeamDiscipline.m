//
//  TeamDiscipline.m
//  ChildCare
//
//  Created by Devclan on 20/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "TeamDiscipline.h"
#import "AppManager.h"

@implementation TeamDiscipline

-(instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [self init];
    if (self) {
        _value = [AppManager getStringForKey:@"name" fromDic:dic];
    }
    return self;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _value = @" ";
    }
    return self;
}


@end
