//
//  TeamAgency.m
//  ChildCare
//
//  Created by Devclan on 20/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "TeamAgency.h"
#import "AppManager.h"

@implementation TeamAgency

-(instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [self init];
    if (self) {
        _nId = [AppManager getStringForKey:@"#nid" fromDic:dic];
    }
    return self;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _nId = @"";
    }
    return self;
}

@end
