//
//  DEMOViewController.m
//  RESideMenuStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "MenuRootViewController.h"
#import "MenuViewController.h"
#import "AppManager.h"
#import "WebViewController.h"
#import <Google/Analytics.h>

@interface MenuRootViewController ()

@end

@implementation MenuRootViewController

- (void)awakeFromNib
{
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.contentViewShadowColor = [UIColor blackColor];
    self.contentViewShadowOffset = CGSizeMake(0, 0);
    self.contentViewShadowOpacity = 0.6;
    self.contentViewShadowRadius = 12;
    self.contentViewShadowEnabled = YES;
    
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    MenuViewController* mvc = [self.storyboard instantiateViewControllerWithIdentifier:@"rightMenuViewController"];
    mvc.menuRootViewController=self;
    self.rightMenuViewController = mvc;
    
    self.panGestureEnabled=NO;
    self.backgroundImage = [UIImage imageNamed:@"Stars"];
    self.delegate = self;
}

#pragma mark -
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    MenuViewController* mvc = (MenuViewController*)menuViewController;
    [mvc updateUI];
    
    
    UINavigationController* nc=(UINavigationController*)self.contentViewController;
    if ([nc.viewControllers lastObject] && [[nc.viewControllers lastObject] isKindOfClass:[WebViewController class]]) {
        WebViewController* agencyWebVC=[nc.viewControllers lastObject];
        Agency* agency=agencyWebVC.agency;
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Side Menu Screen-%@",agency.mainDetails.title]];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
    
    
    NSLog(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

@end
