//
//  PopupViewController.m
//  Meri Awaz
//
//  Created by Devclan on 13/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "PopupViewController.h"
#import <Google/Analytics.h>
#import "Constants.h"
@interface PopupViewController ()
@property (weak, nonatomic) IBOutlet UIView *cont;
@property (weak, nonatomic) IBOutlet UIButton *img;
@property (weak, nonatomic) IBOutlet UIButton *vid;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
@property (weak, nonatomic) IBOutlet UIImageView *waitingIcon;
@property (weak, nonatomic) IBOutlet UIImageView *assessingIcon;
@property (weak, nonatomic) IBOutlet UIImageView *completedIcon;

@end

@implementation PopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _cont.clipsToBounds=YES;
    _cont.layer.cornerRadius=5;
    _waitingIcon.layer.cornerRadius=7.5;
    _completedIcon.layer.cornerRadius=7.5;
    _assessingIcon.layer.cornerRadius=7.5;
//    _img.layer.cornerRadius=5;
//    _vid.layer.cornerRadius=5;
    _arrow.layer.cornerRadius=4;
    // Do any additional setup after loading the view.
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)trackCompleted{
//    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatSettingStatus
//                                                                                        action:kActCompleted
//                                                                                         label:nil
//                                                                                         value:nil] build]];
}
-(void)trackWaiting{
//    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatSettingStatus
//                                                                                        action:kActWaiting
//                                                                                         label:nil
//                                                                                         value:nil] build]];
}
-(void)trackAssessing{
//    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatSettingStatus
//                                                                                        action:kActAssessing
//                                                                                         label:nil
//                                                                                         value:nil] build]];
}
- (IBAction)completedImageAction:(id)sender {
    [self trackCompleted];
    [_delegate completedButtonTapped];
    
}

- (IBAction)waitingImageAction:(id)sender {
    [self trackWaiting];
    [_delegate waitingButtonTapped];
}
- (IBAction)accessingImageAction:(id)sender {
    [self trackAssessing];
    [_delegate accessingButtonTapped];
}
- (IBAction)imgAct:(id)sender {
    [self trackWaiting];
    [_delegate waitingButtonTapped];
}
- (IBAction)vidAct:(id)sender {
    [self trackAssessing];
    [_delegate accessingButtonTapped];
}
- (IBAction)compAct:(id)sender {
    [self trackCompleted];
    [_delegate completedButtonTapped];
}
@end
