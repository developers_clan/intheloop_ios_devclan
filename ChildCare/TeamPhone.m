//
//  TeamPhone.m
//  ChildCare
//
//  Created by Devclan on 20/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "TeamPhone.h"
#import "AppManager.h"

@implementation TeamPhone

-(instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [self init];
    if (self) {
        _number = [AppManager getStringForKey:@"number" fromDic:dic];
        _code = [AppManager getStringForKey:@"country_codes" fromDic:dic];
        _ext = [AppManager getStringForKey:@"extension" fromDic:dic];
    }
    return self;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _number = @"";
        _code = @"";
        _ext = @"";
    }
    return self;
}


@end
