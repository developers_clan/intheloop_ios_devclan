//
//  IGLDropDownItem.m
//  IGLDropDownMenuDemo
//
//  Created by Galvin Li on 8/30/14.
//  Copyright (c) 2014 Galvin Li. All rights reserved.
//

#import "IGLDropDownItem.h"

@interface IGLDropDownItem ()

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UIImageView *dropImageView;

@property (nonatomic, strong) UILabel *textLabel;

@end

@implementation IGLDropDownItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _paddingLeft = 5;
        _paddingRight=2;
        [self initView];
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    
    
//    if (_text.length>0) {
//        CGRect fr = [_text boundingRectWithSize:CGSizeMake(self.textLabel.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"Lato-Bold" size:18],
//                                                                                                                                                                                                      NSForegroundColorAttributeName : [UIColor redColor]} context:[NSStringDrawingContext new]];
//        CGRect modified = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, fr.size.height+5);
//        [super setFrame:modified];
//
//    }else{
        [super setFrame:frame];
//    }
    
    
    
    [self.bgView setFrame:self.bounds];
    
    
    [self updateLayout];
}

- (void)initView
{
    self.bgView = [[UIView alloc] init];
    self.bgView.clipsToBounds=YES;
    self.clipsToBounds=YES;
    self.bgView.userInteractionEnabled = NO;
    self.backgroundColor=[UIColor clearColor];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.hidden=YES;
//    self.bgView.layer.shadowColor = [UIColor grayColor].CGColor;
//    self.bgView.layer.shadowOffset = CGSizeMake(0, 0);
//    self.bgView.layer.shadowOpacity = 0.2;
    self.bgView.layer.shouldRasterize = YES;
    _bgView.layer.borderColor=[UIColor clearColor].CGColor;
    _bgView.layer.borderWidth=1;
    
    
    [self.bgView setFrame:self.bounds];
    [self addSubview:self.bgView];
    
    
    self.iconImageView = [[UIImageView alloc] init];
    self.dropImageView = [[UIImageView alloc] init];
    self.iconImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.dropImageView.contentMode = UIViewContentModeRight;
    [self addSubview:self.iconImageView];
    [self addSubview:self.dropImageView];
    
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.numberOfLines = 2;
//    self.textLabel.minimumScaleFactor=0.5;
    self.textLabel.font=[UIFont fontWithName:@"Lato-Regular" size:17];
//    self.textLabel.adjustsFontSizeToFitWidth = YES;
    self.textLabel.textColor = [UIColor colorWithWhite:136/255.0 alpha:1];
    [self addSubview:self.textLabel];
    
    
    [self updateLayout];
    
}

- (void)setIconImage:(UIImage *)iconImage
{
    _iconImage = iconImage;
    [self.iconImageView setImage:self.iconImage];
    
    [self updateLayout];
}
- (void)setDropDownIcon:(UIImage *)dropDownIcon
{
    _dropDownIcon = dropDownIcon;
    [self.dropImageView setImage:self.dropDownIcon];
    
    [self updateLayout];
}
-(void)setUnderlineColor:(UIColor *)underlineColor{
    _underlineColor=underlineColor;
    [self updateLayout];
}
-(void)setUpperlineColor:(UIColor *)upperlineColor{
    _upperlineColor=upperlineColor;
    [self updateLayout];
}
-(void)setBorderColor:(UIColor *)borderColor{
    _borderColor=borderColor;
    [self updateLayout];
}
- (void)updateLayout
{
    
    CGFloat selfWidth = CGRectGetWidth(self.bounds);
    CGFloat selfHeight = CGRectGetHeight(self.bounds);
    
    if (_underlineColor) {
        UIView* vu = [[UIView alloc] initWithFrame:CGRectMake(0, self.bgView.frame.size.height-2, self.bgView.frame.size.width, 2)];
        vu.backgroundColor=_underlineColor;
        [self.bgView addSubview:vu];
    }
    if (_upperlineColor) {
        UIView* vu2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bgView.frame.size.width, 2)];
        vu2.backgroundColor=_upperlineColor;
        [self.bgView addSubview:vu2];
    }
    
    [self.dropImageView setFrame:CGRectMake(selfWidth-selfHeight-2-self.paddingRight, 0, selfHeight, selfHeight)];
    if (self.iconImage) {
        [self.iconImageView setFrame:CGRectMake(self.paddingLeft, 5, selfHeight-5, selfHeight-10)];
        if (self.dropDownIcon) {
            [self.textLabel setFrame:CGRectMake(CGRectGetMaxX(self.iconImageView.frame)+self.paddingLeft, 5, selfWidth - CGRectGetMaxX(self.iconImageView.frame)-selfHeight-self.paddingLeft-self.paddingRight, selfHeight-10)];
        }else{
            [self.textLabel setFrame:CGRectMake(CGRectGetMaxX(self.iconImageView.frame)+self.paddingLeft, 5, selfWidth - CGRectGetMaxX(self.iconImageView.frame)-self.paddingLeft-self.paddingRight, selfHeight-10)];
        }
        
    } else {
        
        if (self.dropDownIcon) {
            [self.textLabel setFrame:CGRectMake(self.paddingLeft, 5, selfWidth-selfHeight, selfHeight-10)];
        }else{
            [self.textLabel setFrame:CGRectMake(self.paddingLeft, 5, selfWidth-10, selfHeight-10)];
        }
    }
}

- (void)setPaddingLeft:(CGFloat)paddingLeft
{
    _paddingLeft = paddingLeft;
    
    [self updateLayout];
}

- (void)setObject:(id)object
{
    _object = object;
}

- (void)setText:(NSString *)text
{
    _text = text;
    self.textLabel.text = self.text;
}

- (id)copyWithZone:(NSZone *)zone
{
    IGLDropDownItem *itemCopy = [[IGLDropDownItem alloc] init];
    itemCopy.index = _index;
    itemCopy.iconImage = _iconImage;
    itemCopy.object = _object;
    itemCopy.text = _text;
    itemCopy.paddingLeft = _paddingLeft;
    
    return itemCopy;
}

@end
