//
//  TeamDiscipline.h
//  ChildCare
//
//  Created by Devclan on 20/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamDiscipline : NSObject

@property (nonatomic,strong)NSString* value;
- (instancetype)initWithDictionary:(NSDictionary*)dic;

@end
