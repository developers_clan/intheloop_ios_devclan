//
//  AddChildCell.h
//  ChildCare
//
//  Created by Bilal Tahir on 10/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddChildCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblChildName;
@property (weak, nonatomic) IBOutlet UIImageView *imgChild;

@end
