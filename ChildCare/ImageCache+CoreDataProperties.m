//
//  ImageCache+CoreDataProperties.m
//  ChildCare
//
//  Created by Devclan on 27/01/2016.
//  Copyright © 2016 Devclan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ImageCache+CoreDataProperties.h"

@implementation ImageCache (CoreDataProperties)

@dynamic url;
@dynamic data;
@dynamic date;

@end
