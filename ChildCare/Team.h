//
//  Team.h
//  ChildCare
//
//  Created by Devclan on 20/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TeamBody.h"
#import "TeamDiscipline.h"
#import "TeamEmail.h"
#import "Headshot.h"
#import "TeamName.h"
#import "TeamPhone.h"
#import "AgencyImage.h"

@interface Team : NSObject

@property (nonatomic,strong)NSMutableArray* agencyThumbnails;
@property (nonatomic,strong)TeamBody* body;
@property (nonatomic,strong)NSMutableArray* disciplines;
@property (nonatomic,strong)NSMutableArray* emails;
@property (nonatomic,strong)NSMutableArray* offices;
@property (nonatomic,strong)NSMutableArray* headshots;
@property (nonatomic,strong)NSMutableArray* names;
@property (nonatomic,strong)NSMutableArray* phones;
@property (nonatomic,strong)NSMutableArray* genders;
@property (nonatomic,strong)NSString* title;
@property (nonatomic,strong)NSString* nid;
- (instancetype)initWithDictionary:(NSDictionary*)dic;

@end
