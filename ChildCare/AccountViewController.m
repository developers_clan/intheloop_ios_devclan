//
//  SecondViewController.m
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//
#import "ChildCell.h"
#import "AccountViewController.h"
#import "AppDelegate.h"
#import "Child.h"
#import "AppManager.h"
#import "StatusHistoryObj.h"
#import "Constants.h"
#import "UIImage+Additions(Additions).h"
#import <Google/Analytics.h>



@interface AccountViewController ()<UITableViewDelegate,UITableViewDataSource,IGLDropDownMenuDelegate,ChildCellInteractionCallBackDelegate,UITextFieldDelegate,UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIButton *next;
@property (weak, nonatomic) IBOutlet UIImageView *underline;

@end

@implementation AccountViewController{
    AppDelegate* appDelegate;
    int genderInd;
    Child* editingChild;
    IBOutlet NSLayoutConstraint *btnTrailingConstraint;
    IBOutlet NSLayoutConstraint *transitionVuHeight;
    IBOutlet NSLayoutConstraint *btnCenterConstraint;
    IBInspectable BOOL initialSetup;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    genderInd=-1;
    _addChild.layer.cornerRadius=_addChild.frame.size.height/2.0;
//    [NSLayoutConstraint deactivateConstraints:@[btnCenterConstraint]];
    [self.view removeConstraint:btnCenterConstraint];
//    _genderFrame.layer.borderColor=[UIColor darkGrayColor].CGColor;
//    _genderFrame.layer.borderWidth=1;
    
    
    //---------------set_UITextfield Delegate---------------//
    [self.childName setDelegate:self];
    //---------------set_UITextfield Delegate---------------//
    
    
    NSArray* genders = @[@"Boy",@"Girl"];
    appDelegate=[[UIApplication sharedApplication]delegate];
    self.genderDropDownMenu = [[IGLDropDownMenu alloc] init];
    self.genderDropDownMenu.delegate=self;
    self.genderDropDownMenu.menuText = @"Set Gender (Boy or Girl)";
    self.genderDropDownMenu.paddingLeft = 2;
    self.genderDropDownMenu.layer.shadowColor=[UIColor clearColor].CGColor;
    self.genderDropDownMenu.type = IGLDropDownMenuTypeNormal;
    self.genderDropDownMenu.dropdownIconImage = [[UIImage imageNamed:@"dropdown"] imageScaledToSize:CGSizeMake(20, 11)];
    [self setItems:genders ToMenu:self.genderDropDownMenu];
    [self.genderFrame.superview addSubview:self.genderDropDownMenu];
    self.genderFrame.hidden = YES;
    
    
    
//    _childName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Child's Name" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithWhite:136/255.0 alpha:1],NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:18]}];
    
    //--------------child's first name-------------//
    _childName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Child's First Name" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithWhite:136/255.0 alpha:1],NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:18]}];
    //--------------child's first name-------------//
    
    
    
    
    [self.tableView reloadData];
    if ([[AppManager sharedManager] children].count>0) {
        if (_tableView.hidden==YES) {
            [self toggleViews:NO];
        }else{
            _addChild.hidden=NO;
            [_next.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:13]];
            [_next setTitle:@"Cancel" forState:UIControlStateNormal];
            _next.hidden=NO;
            _underline.hidden=NO;
        }
    }else{
        if (_addChildView.hidden==YES) {
            [self toggleViews:NO];
        }else{
            editingChild = [NSEntityDescription
                            insertNewObjectForEntityForName:@"Child"
                            inManagedObjectContext:appDelegate.managedObjectContext];
            _addChild.hidden=NO;
        }
        if (initialSetup==YES) {
            _next.hidden=NO;
            _underline.hidden=NO;
        }else{
            _next.hidden=YES;
            _underline.hidden=YES;
            [_next setTitle:@"" forState:UIControlStateNormal];
        }
    }
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.genderDropDownMenu setFrame:_genderFrame.frame];
    [_genderDropDownMenu reloadView];
    if (genderInd>-1) {
        [_genderDropDownMenu selectItemAtIndex:genderInd];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    Child* c = [[[AppManager sharedManager]children] objectAtIndex:indexPath.row];
    Child* c2=[[AppManager sharedManager]activeChild];
    if (c2!=nil) {
        c2.active=@NO;
    }
    c.active=@YES;
    [appDelegate.managedObjectContext save:NULL];
    [tableView reloadData];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChildCell* cell=[tableView dequeueReusableCellWithIdentifier:@"ChildCell"];
    Child* c = [[[AppManager sharedManager]children] objectAtIndex:indexPath.row];
    
    
    
    cell.name.text=c.name;
    cell.delegate=self;
    cell.indexPath=indexPath;
    cell.selectedBackgroundView=[UIView new];
    if ([c.active boolValue]==YES) {
//        cell.contentView.backgroundColor=[UIColor colorWithWhite:0.8 alpha:1];
        cell.container.layer.borderColor = [[AppManager sharedManager] appThemeColor].CGColor;
    }else{
//        cell.contentView.backgroundColor=[UIColor colorWithWhite:0.9 alpha:1];
        cell.container.layer.borderColor = [UIColor colorWithWhite:230/255.0 alpha:1].CGColor;
    }
    if ([c.gender integerValue]==0) {
        cell.img.image = [UIImage imageNamed:@"account_setting2_user_placeholder_boy"];
    }else{
        cell.img.image = [UIImage imageNamed:@"account_setting2_user_placeholder_girl"];
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[AppManager sharedManager]children].count;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([[AppManager sharedManager] children].count<1 && _tableView.hidden==NO) {
        [self toggleViews:NO];
    }else{
        [self adjustTransitioningVuHeight:NO];
    }
    [[self tableView]reloadData];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (_tableView.hidden) {
        [self toggleViews:NO];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)adjustTransitioningVuHeight:(BOOL)animated{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        long height = MAX([[AppManager sharedManager]children].count*60, 200) ;
        height = MIN(height, self.view.frame.size.height-220);
        [self.view layoutIfNeeded];
        if (animated) {
            [UIView animateWithDuration:ANIMATION_DURATION
                             animations:^{
                                 _addChild.hidden=NO;
                                 transitionVuHeight.constant=height;
                                 if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                                     if (self.next.hidden==YES) {
//                                         [NSLayoutConstraint activateConstraints:@[btnCenterConstraint]];
//                                         [NSLayoutConstraint deactivateConstraints:@[btnTrailingConstraint]];
                                         [self.view removeConstraint:btnTrailingConstraint];
                                         [self.view addConstraint:btnCenterConstraint];
                                     }else{
//                                         [NSLayoutConstraint deactivateConstraints:@[btnCenterConstraint]];
//                                         [NSLayoutConstraint activateConstraints:@[btnTrailingConstraint]];
                                         [self.view removeConstraint:btnCenterConstraint];
                                         [self.view addConstraint:btnTrailingConstraint];
                                     }
                                 }else{
                                     btnCenterConstraint.active = self.next.hidden;
                                     btnTrailingConstraint.active = !btnCenterConstraint.active;
                                 }

                                 [self.view layoutIfNeeded];
                                 // Called on parent view
                             }];
        }else{
            _addChild.hidden=NO;
            transitionVuHeight.constant=height;
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                if (self.next.hidden==YES) {
                    //                                         [NSLayoutConstraint activateConstraints:@[btnCenterConstraint]];
                    //                                         [NSLayoutConstraint deactivateConstraints:@[btnTrailingConstraint]];
                    [self.view removeConstraint:btnTrailingConstraint];
                    [self.view addConstraint:btnCenterConstraint];
                }else{
                    //                                         [NSLayoutConstraint deactivateConstraints:@[btnCenterConstraint]];
                    //                                         [NSLayoutConstraint activateConstraints:@[btnTrailingConstraint]];
                    [self.view removeConstraint:btnCenterConstraint];
                    [self.view addConstraint:btnTrailingConstraint];
                }
            }else{
                
                btnCenterConstraint.active = self.next.hidden;
                btnTrailingConstraint.active = !btnCenterConstraint.active;
            }
            [self.view layoutIfNeeded];
        }
    }];
    
}
- (IBAction)addChildAction:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:@"SAVE"]) {
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAccountSettings
                                                                                            action:kActSave
                                                                                             label:nil
                                                                                             value:nil] build]];
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAccountSettings
                                                                                            action:kActUpdate
                                                                                             label:nil
                                                                                             value:nil] build]];
        
        if (_childName.text.length<2) {
            [[[UIAlertView alloc]initWithTitle:@"" message:@"Please tell us the name of your child" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            return;
        }
        if (genderInd==-1) {
            [[[UIAlertView alloc]initWithTitle:@"" message:@"Please tell us either your child is boy or a girl" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            return;
        }
        if (editingChild==nil) {
            [[[UIAlertView alloc]initWithTitle:@"" message:@"An error occured" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            return;
        }

        if ([[AppManager sharedManager]childExists:_childName.text]==YES) {
            if (editingChild.inserted==YES) {
                [[[UIAlertView alloc]initWithTitle:@"" message:@"Child with same name already exists." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
                return;
            }
        }
        if ([editingChild.name isEqualToString:_childName.text]==NO) {
            editingChild.name = _childName.text;
        }
        if (editingChild.gender!=[NSNumber numberWithInt:genderInd]) {
            editingChild.gender= [NSNumber numberWithInt:genderInd];
        }
        if ([[AppManager sharedManager] activeChild]==nil) {
            editingChild.active=@YES;
        }
        NSError *error;
        if (![appDelegate.managedObjectContext save:&error]) {
            [[[UIAlertView alloc]initWithTitle:@"" message:@"An error occured" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        }else{
            editingChild=nil;
            [[AppManager sharedManager]populateChildrenData];
            [_tableView reloadData];
            [self toggleViews:YES];
        }
    }else{
        [self toggleViews:YES];
    }
    
}
-(void)hideKeyboard{
    [_childName resignFirstResponder];
}

-(void)toggleViews:(BOOL)animated{
    [self adjustTransitioningVuHeight:animated];
    if (_tableView.hidden==NO) {
        
        //---------------googleAnalytics-----------//
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"Add Child Screen"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        //---------------googleAnalytics-----------//
        
        
        [_addChild setTitle:@"SAVE" forState:UIControlStateNormal];
        
        _addChildView.hidden=NO;
        _tableView.hidden=YES;
        

        if ([[AppManager sharedManager]children].count>0) {
            [_next.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:13]];
            [_next setTitle:@"Cancel" forState:UIControlStateNormal];
            _next.hidden=NO;
            _underline.hidden=NO;
        }else{
            _next.hidden=YES;
            _underline.hidden=YES;
            [_next setTitle:@"" forState:UIControlStateNormal];
        }
        

        
        if (editingChild!=nil) {
            _childName.text=editingChild.name;
            [_genderDropDownMenu selectItemAtIndex:[editingChild.gender integerValue]];
        }else{
            [_childName setText:@""];
            genderInd=-1;
            editingChild = [NSEntityDescription
                            insertNewObjectForEntityForName:@"Child"
                            inManagedObjectContext:appDelegate.managedObjectContext];
            [_genderDropDownMenu reloadView];

        }
        
        
        if (animated==YES) {
            [UIView transitionWithView:_tableView duration:ANIMATION_DURATION options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
                
            } completion:^(BOOL finished) {
                
            }];
            
            
            
            [UIView transitionWithView:_addChildView duration:ANIMATION_DURATION options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
                
            } completion:^(BOOL finished) {
            }];
        }
        
        
    }else{//acount setting
        
        //---------------googleAnalytics-----------//
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"Account Settings Screen"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        //---------------googleAnalytics-----------//
        
        
        if (editingChild!=nil && (editingChild.name==nil || editingChild.active==nil || editingChild.gender==nil)) {
            [appDelegate.managedObjectContext deleteObject:editingChild];
        }
        editingChild=nil;
        
        [_addChild setTitle:@"ADD ANOTHER CHILD" forState:UIControlStateNormal];
        if (initialSetup==YES) {
            [_next.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:13]];
            [_next setTitle:@"Continue" forState:UIControlStateNormal];
            _next.hidden=NO;
            _underline.hidden=NO;
        }else{
            _next.hidden=YES;
            _underline.hidden=YES;
            [_next setTitle:@"" forState:UIControlStateNormal];
        }
        _tableView.hidden=NO;
        _addChildView.hidden=YES;
        
        if (animated==YES) {
            [UIView transitionWithView:_addChildView duration:ANIMATION_DURATION options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
                
            } completion:^(BOOL finished) {
                
            }];
            
            
            
            [UIView transitionWithView:_tableView duration:ANIMATION_DURATION options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
                
            } completion:^(BOOL finished) {
            }];
        }
        
    }
}
- (IBAction)setupSkipped:(id)sender {
    if (initialSetup==NO) {
        [self toggleViews:YES];
        return;
    }
    if (editingChild!=nil && (editingChild.name==nil || editingChild.name.length<1)) {
        [appDelegate.managedObjectContext deleteObject:editingChild];
    }
    UIViewController* vc=[self.storyboard instantiateViewControllerWithIdentifier:@"TabsController"];
    [self.view addSubview:vc.view];
    [UIView transitionWithView:self.view duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        
    } completion:^(BOOL finished) {
        [vc.view removeFromSuperview];
        [[[[UIApplication sharedApplication] delegate] window] setRootViewController:vc];
    }];
}
-(void)deleteEditingChild{
    [appDelegate.managedObjectContext deleteObject:editingChild];
    
    NSEntityDescription* entity=[NSEntityDescription entityForName:@"StatusHistoryObj" inManagedObjectContext:[appDelegate managedObjectContext]];
    NSFetchRequest* request = [NSFetchRequest new];
    [request setEntity:entity];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"childId=%@",editingChild.objectID.URIRepresentation.absoluteString];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray* objs = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
    if (error!=nil) {
        [[[UIAlertView alloc]initWithTitle:@"" message:@"There was an error" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil]show];
        return;
    }
    for (StatusHistoryObj* o in objs) {
        [appDelegate.managedObjectContext deleteObject:o];
    }
    
    if (![appDelegate.managedObjectContext save:&error]) {
        [[[UIAlertView alloc]initWithTitle:@"" message:@"There was an error" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil]show];
        return;
    }
    editingChild=nil;
    [[AppManager sharedManager] populateChildrenData];
    
    if ([[AppManager sharedManager]children].count<1 && _addChildView.hidden==YES) {
        [self toggleViews:YES];
    }else{
        [self adjustTransitioningVuHeight:YES];
    }
    if ([[AppManager sharedManager]children].count>0 && [[AppManager sharedManager] activeChild]==nil) {
        Child* c = [[[AppManager sharedManager] children] objectAtIndex:0];
        c.active=@YES;
        [appDelegate.managedObjectContext save:&error];
    }
    [_tableView reloadData];
}

#pragma mark UITextfield Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 11;
}

#pragma mark - IGLDropdown
- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu selectedItemAtIndex:(NSInteger)index
{
    if (dropDownMenu==self.genderDropDownMenu) {  
        genderInd=(int)index;
    }
}
-(void)setItems:(NSArray*)items ToMenu:(IGLDropDownMenu*)menu{
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    
    

    
    for (int i = 0; i < items.count; i++) {
        
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        item.bgView.backgroundColor=[UIColor colorWithWhite:246/255.0 alpha:1];
        item.borderColor= [UIColor colorWithWhite:220/255.0 alpha:1];
        NSString* text = items[i];
        if ([text isEqualToString:@"Boy"]) {
            item.iconImage = [[UIImage imageNamed:@"account_setting2_user_placeholder_boy"] imageScaledToSize:CGSizeMake(15, 15)];
        }else if([text isEqualToString:@"Girl"]){
            item.iconImage = [[UIImage imageNamed:@"account_setting2_user_placeholder_girl"] imageScaledToSize:CGSizeMake(15, 15)];
        }
        
        [item setText:items[i]];
        [dropdownItems addObject:item];
    }
    
    menu.dropDownItems=dropdownItems;
    [menu reloadView];
    [menu foldView];
}
#pragma mark - ChildCellInteractionDelegate
-(void)cell:(ChildCell *)cell deleteClicked:(NSIndexPath *)indexPath{
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAccountSettings
                                                                                        action:kActDeleteChildPressed
                                                                                         label:nil
                                                                                         value:nil] build]];
    editingChild=[[[AppManager sharedManager]children] objectAtIndex:indexPath.row];
    
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Warning!"
//                                  message:[NSString stringWithFormat:@"Are you sure to delete record of %@",editingChild.name]
//                                  preferredStyle:UIAlertControllerStyleActionSheet];
//    UIAlertAction* ok = [UIAlertAction
//                         actionWithTitle:@"OK"
//                         style:UIAlertActionStyleDestructive
//                         handler:^(UIAlertAction * action)
//                         {
//                             [alert dismissViewControllerAnimated:YES completion:nil];
//                             [self deleteEditingChild];
//                         }];
//    UIAlertAction* cancel = [UIAlertAction
//                             actionWithTitle:@"Cancel"
//                             style:UIAlertActionStyleDefault
//                             handler:^(UIAlertAction * action)
//                             {
//                                 [alert dismissViewControllerAnimated:YES completion:nil];
//                                 
//                             }];
//    
//    [alert addAction:ok];
//    [alert addAction:cancel];
//    [self presentViewController:alert animated:YES completion:nil];
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:@"Delete"
                                              otherButtonTitles:nil];
    [sheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        
        [self deleteEditingChild];
    }
}
-(void)cell:(ChildCell *)cell editClicked:(NSIndexPath *)indexPath{
    editingChild=[[[AppManager sharedManager]children] objectAtIndex:indexPath.row];
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAccountSettings
                                                                                        action:kActEditChildPressed
                                                                                         label:nil
                                                                                         value:nil] build]];
    
    [self toggleViews:YES];
}





@end
