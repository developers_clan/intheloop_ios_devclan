//
//  Constants.h
//  ChildCare
//
//  Created by Devclan on 21/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//
#define kAppDataRefreshInterval 86400.0//seconds-> 1 days
#define ANIMATION_DURATION 0.4
#define ANIMATION_DURATION_LONG 0.7
#define ANIMATION_DURATION_SHORT 0.2
#define STATUSBAR_NOTIFICATION_DURATION 2.0

#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define API_BASE_ADDRESS @"http://intheloop.dreamhosters.com" //@"http://childapi.fractus.ws"
#define API_PATH @"/api"
#define API_FILES @"/sites/default/files"
#define kGetAgenciesAPI @"/agency"
#define kGetHomeAPI @"/home"
#define kGetTeamsAPI @"/team"
#define kAgenciesContacts @"/contact"
#define kFieldCollectionAPI @"/field-col?fid=%@"
#define kAgencyTitlePlaceholder @"agencyTitlePlaceholder"
#define kAgencyBodyPlaceholder @"agencyBodyPlaceholder"
#define kAgencyResourcesPlaceholder @"agencyResourcesPlaceholder"
#define kAgencySubtitlePlaceholder @"agencySubtitlePlaceholder"
#define kSectionTitlePlaceholder @"sectionTitlePlaceholder"
#define kSectionBodyPlaceholder @"sectionBodyPlaceholder"
#define kAgencyImagePlaceholder @"agencyImagePlaceholder"
#define kSectionItemsPlaceholder @"sectionItemsPlaceholder"

#define kAgencyBannerPlaceholder @"agencyBannerPlaceholder"
#define kContactBorderPlaceholder @"contactBorderPlaceholder"

#define kAgencyPhoneNumberPlaceholder @"agencyPhoneNumberPlaceholder"
#define kAgencyPhoneNumberAliasPlaceholder @"agencyPhoneNumberAliasPlaceholder"
#define kTermsAccepted @"termsAccepted"

#define kAgencyWebsiteTitlePlaceholder @"agencyWebsiteTitlePlaceholder"
#define kAgencyConnectTitlePlaceholder @"agencyConnectTitlePlaceholder"

#define kAgencyWebsiteUrlPlaceholder @"agencyWebsiteUrlPlaceholder"
#define kAgencyConnectUrlPlaceholder @"agencyConnectUrlPlaceholder"
#define kAgencyConnectIconPlaceholder @"agencyConnectIconPlaceholder"

#define kSelectedChildIndex @"kSelectedChildIndex"
#define kResourcesDiv @"resourcesDiv"
#define kBorderColorPlaceholder @"borderColorPlaceholder"
#define SEPARATOR @"-#,#-"
#define ResourcesDivHTML @"<div class=\"resources\"><div class=\"resources_inner\">agencyResourcesPlaceholder</div></div>"

//Google analytics
#define kCatSettingStatus @"SettingStatus"
#define kCatStatusHistory @"StatusHistory"
#define kCatSideMenu @"SideMenu"
#define kCatAgencyContact @"AgencyContact"
#define kCatAgencyListingScreen @"AgencyListingScreen"
#define kCatAccountSettings @"AccountSettings"
#define kCatTeam @"Team"
#define kCatContactUs @"ContactUs"

#define kActSave @"Save on AccountSettings"
#define kActUpdate @"Update on AccountSettings"
#define kActHistory @"HistoryButtonPressed"
#define kActTeamSearch @"Search Team Member"
#define kActTeamContactRemoved @"TeamContactRemoved"
#define kActContactUsAgencySelected @"ContactUsAgencySelected"
#define kActTeamAddContactPressed @"TeamAddContactPressed"
#define kActTeamContactMapPressed @"TeamContactMapPressed"
#define kActTeamContactEmailPressed @"TeamContactEmailPressed"
#define kActTeamContactPhonePressed @"TeamContactPhonePressed"
#define kActTeamContactEditPressed @"TeamContactEditPressed"

#define kActTeamContactAdded @"TeamContactAdded"
#define kActTeamContactExpanded @"TeamContactExpanded"
#define kActTeamContactCollapsed @"TeamContactCollapsed"
#define kActAgencyPhonePressed @"Phone"
#define kActDeleteChildPressed @"DeleteChildPressed"
#define kActEditChildPressed @"EditChildPressed"
#define kActMenuSelectedMyServices @"MenuSelectedMyServices"
#define kActMenuSelectedWhileYouWait @"MenuSelectedWhileYouWait"
#define kActEditStatusHistoryDate @"ButtonPressedEditStatusHistoryDate"
#define kActCompleted @"ButtonPressedStatusCompleted"
#define kActWaiting @"ButtonPressedStatusWaiting"
#define kActAssessing @"ButtonPressedStatusAssessing"
#define kActSaveStatus @"ButtonPressedSaveStatus"