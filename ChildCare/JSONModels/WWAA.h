//
//  WWAA.h
//  ChildCare
//
//  Created by Devclan on 29/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WWAAStatus.h"
#import "WWAATitle.h"
#import "AppManager.h"
@interface WWAA : NSObject
@property (nonatomic,strong)WWAATitle *title;
@property (nonatomic,strong)NSMutableArray *status;
- (instancetype)initWithDictionary:(NSDictionary*)dic;
@end
