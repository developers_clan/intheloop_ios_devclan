//
//  AgencyWebsite.h
//  ChildCare
//
//  Created by Devclan on 08/01/2016.
//  Copyright (c) 2016 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AgencyWebsite : NSObject
@property (nonatomic,strong)NSString* url;
@property (nonatomic,strong)NSString* title;
- (instancetype)initWithDictionary:(NSDictionary*)dic;
@end
