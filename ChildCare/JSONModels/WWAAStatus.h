//
//  WWAATitle.h
//  ChildCare
//
//  Created by Devclan on 29/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WWAAStatus : NSObject
@property (nonatomic,strong)NSString* markup;
- (instancetype)initWithDictionary:(NSDictionary*)dic;
@end
