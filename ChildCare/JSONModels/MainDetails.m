//
//  MainHomeDetails.m
//  ChildCare
//
//  Created by Devclan on 07/01/2016.
//  Copyright (c) 2016 Devclan. All rights reserved.
//

#import "MainDetails.h"

@implementation MainDetails

- (instancetype)initWithDictionary:(NSDictionary*)dic titleFld:(NSString*)titleFld bodyFld:(NSString*)bodyFld
{
    self=[self init];
    if (self) {
        _title=[AppManager getStringForKey:titleFld fromDic:dic];
        _body=[AppManager getStringForKey:bodyFld fromDic:dic];
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _title=@"";
        _body=@"";
    }
    return self;
}
@end
