//
//  AgencyHomeBody.m
//  ChildCare
//
//  Created by Devclan on 22/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "AgencyHomeBody.h"
#import "AppManager.h"

@implementation AgencyHomeBody
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        _markup=[AppManager getStringForKey:@"#markup" fromDic:dic];
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _markup=@"";
    }
    return self;
}
@end
