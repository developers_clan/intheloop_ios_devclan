//
//  AgencyPhone.h
//  ChildCare
//
//  Created by Devclan on 22/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppManager.h"

@interface AgencyPhone : NSObject
@property (nonatomic,strong)NSString* number;
@property (nonatomic,strong)NSString* number_alias;
@property (nonatomic,strong)NSString* countryCodes;
@property (nonatomic,strong)NSString* extension;

- (instancetype)initWithDictionary:(NSDictionary*)dic;
@end
