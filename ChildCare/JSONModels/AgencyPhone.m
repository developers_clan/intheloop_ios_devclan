//
//  AgencyPhone.m
//  ChildCare
//
//  Created by Devclan on 22/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "AgencyPhone.h"

@implementation AgencyPhone
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        _countryCodes=[AppManager getStringForKey:@"country_codes" fromDic:dic];
        _number=[AppManager getStringForKey:@"number" fromDic:dic];
        _extension=[AppManager getStringForKey:@"extension" fromDic:dic];
        if ([[dic allKeys] containsObject:@"number_alias"]) {
             _number_alias=[AppManager getStringForKey:@"number_alias" fromDic:dic];
        }
        else{
            _number_alias=[AppManager getStringForKey:@"number" fromDic:dic];
        }
        
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _extension=@"";
        _number=@"";
        _countryCodes=@"";
    }
    return self;
}
@end
