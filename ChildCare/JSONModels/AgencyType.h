//
//  AgencyType.h
//  ChildCare
//
//  Created by Devclan on 22/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AgencyType : NSObject
@property (nonatomic,strong)NSString* value;
- (instancetype)initWithDictionary:(NSDictionary*)dic;

@end
