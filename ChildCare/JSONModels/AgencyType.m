//
//  AgencyType.m
//  ChildCare
//
//  Created by Devclan on 22/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "AgencyType.h"
#import "AppManager.h"

@implementation AgencyType
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        _value=[AppManager getStringForKey:@"value" fromDic:dic];
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _value=@"";
    }
    return self;
}
@end
