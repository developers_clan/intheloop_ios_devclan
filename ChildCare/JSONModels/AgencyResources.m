//
//  AgencyResources.m
//  ChildCare
//
//  Created by Devclan on 27/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "AgencyResources.h"
#import "AppManager.h"

@implementation AgencyResources
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        _markup=[AppManager getStringForKey:@"value" fromDic:dic];
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _markup=@"";
    }
    return self;
}
@end
