//
//  AgencySection.m
//  ChildCare
//
//  Created by Devclan on 22/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//
#import "AppManager.h"
#import "AgencyHomeSection.h"

@implementation AgencyHomeSection
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        _body=[[AgencyHomeBody alloc] initWithDictionary:[dic objectForKey:@"Home Body"]];
        _title=[[AgencyHomeTitle alloc] initWithDictionary:[dic objectForKey:@"Home Title"]];
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _body=[AgencyHomeBody new];
        _title=[AgencyHomeTitle new];
    }
    return self;
}
@end
