//
//  WYW.m
//  ChildCare
//
//  Created by Devclan on 27/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "WYW.h"

@implementation WYW
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
//        _status=[[WYWStatus alloc]initWithDictionary:[dic objectForKey:@"WYW Status"]];
        
        _body=[[WYWBody alloc]initWithDictionary:[dic objectForKey:@"WYW Body"]];
        _title=[[WYWTitle alloc]initWithDictionary:[dic objectForKey:@"WYW Title"]];
        NSArray* sec=[dic objectForKey:@"WYW Status"];
        for(NSDictionary* d in sec){
            [_status addObject:[[WYWStatus alloc] initWithDictionary:d]];
        }
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _status=[NSMutableArray new];
        _body=[WYWBody new];
        _title=[WYWTitle new];
    }
    return self;
}
@end
