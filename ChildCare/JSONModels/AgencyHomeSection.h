//
//  AgencySection.h
//  ChildCare
//
//  Created by Devclan on 22/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AgencyHomeBody.h"
#import "AgencyHomeTitle.h"
@interface AgencyHomeSection : NSObject
//@property (nonatomic,strong)NSString* fid;
@property (nonatomic,strong)AgencyHomeBody* body;
@property (nonatomic,strong)AgencyHomeTitle* title;
- (instancetype)initWithDictionary:(NSDictionary*)dic;
@end
