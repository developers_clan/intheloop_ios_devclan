//
//  AgencyImage.m
//  ChildCare
//
//  Created by Devclan on 22/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "AgencyImage.h"
#import "AppManager.h"
@implementation AgencyImage
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        if ([[dic allKeys] containsObject:@"uri"]) {
            _uri=[AppManager getStringForKey:@"uri" fromDic:dic];
        }
        if ([[dic allKeys] containsObject:@"agency"]) {
            _agency=[[TeamAgency alloc] initWithDictionary:[dic objectForKey:@"agency"]];
        }

    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _uri=@"";
        _agency=[TeamAgency new];
    }
    return self;
}
@end
