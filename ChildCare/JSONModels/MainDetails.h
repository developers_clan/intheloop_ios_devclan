//
//  MainHomeDetails.h
//  ChildCare
//
//  Created by Devclan on 07/01/2016.
//  Copyright (c) 2016 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppManager.h"

@interface MainDetails : NSObject
@property (nonatomic,strong)NSString* title;
@property (nonatomic,strong)NSString* body;
- (instancetype)initWithDictionary:(NSDictionary*)dic titleFld:(NSString*)titleFld bodyFld:(NSString*)bodyFld;

@end
