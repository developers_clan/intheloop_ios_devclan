//
//  AgencyContact.m
//  ChildCare
//
//  Created by Devclan on 22/12/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "AgencyContact.h"
#import "AgencyWebsite.h"

@implementation AgencyContact
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        
        if (![AppManager isValueNULLForKey:@"phone" inDic:dic]) {
            if ([[dic objectForKey:@"phone"] isKindOfClass:[NSDictionary class]]) {
                _agencyPhone=[[AgencyPhone alloc] initWithDictionary:[dic objectForKey:@"phone"]];
            }else if([[dic objectForKey:@"phone"] isKindOfClass:[NSArray class]]){
                NSArray* a = [dic objectForKey:@"phone"];
                if (a.count>0) {
                    _agencyPhone=[[AgencyPhone alloc] initWithDictionary:[a objectAtIndex:0]];
                }
            }
        }
        
        if (![AppManager isValueNULLForKey:@"contact_logo" inDic:dic]) {
            if ([[dic objectForKey:@"contact_logo"] isKindOfClass:[NSDictionary class]]) {
                _agencyImage=[[AgencyImage alloc] initWithDictionary:[dic objectForKey:@"contact_logo"]];
            }else if([[dic objectForKey:@"contact_logo"] isKindOfClass:[NSArray class]]){
                NSArray* a = [dic objectForKey:@"contact_logo"];
                if (a.count>0) {
                    _agencyImage=[[AgencyImage alloc] initWithDictionary:[a objectAtIndex:0]];
                }
            }
        }
        _agencyId = [AppManager getStringForKey:@"agency" fromDic:dic];
        _agencyColorCode = [AppManager getStringForKey:@"color_code" fromDic:dic];
        _agencyColor = [AppManager colorFromHexString:[dic objectForKey:@"color_code"]];
        [_websites removeAllObjects];
        NSArray* websites=[dic objectForKey:@"social_links"];
        for(NSDictionary* d in websites){
            [_websites addObject:[[AgencyWebsite alloc] initWithDictionary:d]];
        }
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _agencyId=@"";
        _websites=[NSMutableArray new];
        _agencyImage=[AgencyImage new];
        _agencyPhone=[AgencyPhone new];
        _agencyColor=[UIColor clearColor];
        _agencyColorCode = @"#00000000";
    }
    return self;
}
@end
