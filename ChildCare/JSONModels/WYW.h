//
//  WYW.h
//  ChildCare
//
//  Created by Devclan on 27/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WYWBody.h"
#import "WYWStatus.h"
#import "WYWTitle.h"
@interface WYW : NSObject
@property (nonatomic,strong)WYWBody* body;
@property (nonatomic,strong)WYWTitle* title;
@property (nonatomic,strong)NSMutableArray *status;
- (instancetype)initWithDictionary:(NSDictionary*)dic;
@end
