//
//  AgencyImage.h
//  ChildCare
//
//  Created by Devclan on 22/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TeamAgency.h"
@interface AgencyImage : NSObject
@property (nonatomic,strong)NSString* uri;
@property (nonatomic,strong)TeamAgency* agency;
- (instancetype)initWithDictionary:(NSDictionary*)dic;
@end
