//
//  Agency.m
//  ChildCare
//
//  Created by Devclan on 21/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "Agency.h"
#import "AgencyHomeSection.h"
#import "StatusCheckBox.h"
#import "AgencyWebsite.h"

@implementation Agency
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        _nid=[AppManager getStringForKey:@"nid" fromDic:dic];
//        _title=[AppManager getStringForKey:@"title" fromDic:dic];
//        _body=[AppManager getStringForKey:@"body" fromDic:dic];
        _category=[AppManager getStringForKey:@"category" fromDic:dic];
        if (![AppManager isValueNULLForKey:@"image" inDic:dic]) {
            if ([[dic objectForKey:@"image"] isKindOfClass:[NSDictionary class]]) {
                _image=[[AgencyImage alloc] initWithDictionary:[dic objectForKey:@"image"]];
            }else if([[dic objectForKey:@"image"] isKindOfClass:[NSArray class]]){
                NSArray* a = [dic objectForKey:@"image"];
                if (a.count>0) {
                    _image=[[AgencyImage alloc] initWithDictionary:[a objectAtIndex:0]];
                }
            }
        }
        if (![AppManager isValueNULLForKey:@"contact_logo" inDic:dic]) {
            if ([[dic objectForKey:@"contact_logo"] isKindOfClass:[NSDictionary class]]) {
                _logo=[[AgencyImage alloc] initWithDictionary:[dic objectForKey:@"contact_logo"]];
            }else if([[dic objectForKey:@"contact_logo"] isKindOfClass:[NSArray class]]){
                NSArray* a = [dic objectForKey:@"contact_logo"];
                if (a.count>0) {
                    _logo=[[AgencyImage alloc] initWithDictionary:[a objectAtIndex:0]];
                }
            }
        }
        NSArray* arr = [dic objectForKey:@"home_main_details"];
        if (arr.count>0) {
            _mainDetails = [_mainDetails initWithDictionary:[arr objectAtIndex:0] titleFld:@"home_main_title" bodyFld:@"home_main_body"];
        }
        
        NSArray* arr2 = [dic objectForKey:@"wyw_main_details"];
        if (arr2.count>0) {
            _WYWMainDetails = [_WYWMainDetails initWithDictionary:[arr2 objectAtIndex:0] titleFld:@"wyw_main_title" bodyFld:@"wyw_main_body"];
        }
        
        NSArray* arr3 = [dic objectForKey:@"wwaa_main_details"];
        if (arr3.count>0) {
            _WWAAMainDetails = [_WWAAMainDetails initWithDictionary:[arr3 objectAtIndex:0] titleFld:@"wwaa_main_title" bodyFld:@"wwaa_main_body"];
        }
        
        
        if ([[dic objectForKey:@"color_code"] isKindOfClass:[NSArray class]]) {
            _agencyColorCode = [[dic objectForKey:@"color_code"] objectAtIndex:0];
        }
        _agencyColor = [AppManager colorFromHexString:_agencyColorCode];
        
        
        
        
        NSArray* arr5 = [dic objectForKey:@"agency_phone"];
        if (arr5.count>0) {
            _agencyPhone=[_agencyPhone initWithDictionary:[arr5 objectAtIndex:0]];
        }
        
        
        NSArray* arr4 = [dic objectForKey:@"resources"];
        if (arr4.count>0) {
            id res = [arr4 objectAtIndex:0];
            if ([res isKindOfClass:[NSDictionary class]]) {
                _agencyResources=[[AgencyResources alloc] initWithDictionary:[arr4 objectAtIndex:0]];
            }
        }
        
        NSArray* sec=[dic objectForKey:@"agency_home"];
        for(NSDictionary* d in sec){
            [_homeSections addObject:[[AgencyHomeSection alloc] initWithDictionary:d]];
        }
        NSArray* wyw=[dic objectForKey:@"wyw"];
        for(NSDictionary* d in wyw){
            [_wyw addObject:[[WYW alloc] initWithDictionary:d]];
        }
        if ([[dic objectForKey:@"wwaa"] isKindOfClass:[NSDictionary class]]) {
            NSDictionary* wwaaDic = [dic objectForKey:@"wwaa"];
            NSArray* keys = [wwaaDic allKeys];
            for (NSString* key in keys) {
                if ([AppManager isNumeric:key]) {
                    [_wwaa addObject:[[WWAA alloc] initWithDictionary:[wwaaDic objectForKey:key]]];
                }
            }
        }else if([[dic objectForKey:@"wwaa"] isKindOfClass:[NSArray class]]){
            NSArray* wwaa=[dic objectForKey:@"wwaa"];
            for(NSDictionary* d in wwaa){
                [_wwaa addObject:[[WWAA alloc] initWithDictionary:d]];
            }
        }
        NSArray* websites=[dic objectForKey:@"website"];
        [_websites removeAllObjects];
        for(NSDictionary* d in websites){
            [_websites addObject:[[AgencyWebsite alloc] initWithDictionary:d]];
        }
        
        NSArray* cnnects=[dic objectForKey:@"cdp_connect"];
        [_connects removeAllObjects];
        for(NSDictionary* d in cnnects){
            [_connects addObject:[[AgencyWebsite alloc] initWithDictionary:d]];
        }
        
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _mainDetails = [MainDetails new];
        _WYWMainDetails = [MainDetails new];
        _WWAAMainDetails = [MainDetails new];
        
//        _title=@"";
//        _body=@"";
        _image=[[AgencyImage alloc] init];
        _homeSections=[NSMutableArray new];
        _wyw=[NSMutableArray new];
        _wwaa=[NSMutableArray new];
        _nid=@"";
        _category=@"";
        _agencyPhone=[AgencyPhone new];
        _agencyResources=[AgencyResources new];
        _websites = [NSMutableArray new];
        _connects = [NSMutableArray new];
        _agencyColor=[UIColor clearColor];
        _agencyColorCode = @"#00000000";
        _logo=[AgencyImage new];
    }
    return self;
}
@end
