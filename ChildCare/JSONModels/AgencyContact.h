//
//  AgencyContact.h
//  ChildCare
//
//  Created by Devclan on 22/12/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AgencyPhone.h"
#import "AgencyImage.h"
@class AgencyPhone;
@interface AgencyContact : NSObject
@property (strong,nonatomic)AgencyImage* agencyImage;
@property (strong,nonatomic)AgencyPhone* agencyPhone;
@property (strong,nonatomic)NSString* agencyId;
@property (strong,nonatomic)NSString* agencyColorCode;
@property (strong,nonatomic)UIColor* agencyColor;
@property (nonatomic,strong)NSMutableArray* websites;


- (instancetype)initWithDictionary:(NSDictionary*)dic;
@end
