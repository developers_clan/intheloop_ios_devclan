//
//  AgencyWebsite.m
//  ChildCare
//
//  Created by Devclan on 08/01/2016.
//  Copyright (c) 2016 Devclan. All rights reserved.
//

#import "AgencyWebsite.h"
#import "AppManager.h"

@implementation AgencyWebsite
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        _url=[AppManager getStringForKey:@"url" fromDic:dic];
        _title=[AppManager getStringForKey:@"title" fromDic:dic];
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _url=@"";
        _title=@"";
    }
    return self;
}

@end
