//
//  WWAA.m
//  ChildCare
//
//  Created by Devclan on 29/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "WWAA.h"

@implementation WWAA
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        if (![AppManager isValueNULLForKey:@"WWAA Title" inDic:dic]) {
            _title=[[WWAATitle alloc]initWithDictionary:[dic objectForKey:@"WWAA Title"]];
        }
        NSArray* wwaa=[dic objectForKey:@"WWAA Status"];
        for(NSDictionary* d in wwaa){
            [_status addObject:[[WWAAStatus alloc] initWithDictionary:d]];
        }
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _title = [WWAATitle new];
        _status=[NSMutableArray new];
    }
    return self;
}
@end
