//
//  AgencySection.m
//  ChildCare
//
//  Created by Devclan on 22/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "AgencySectionID.h"
#import "AppManager.h"

@implementation AgencySectionID
- (instancetype)initWithDictionary:(NSDictionary*)dic
{
    self=[self init];
    if (self) {
        _revisionId=[AppManager getStringForKey:@"revision_id" fromDic:dic];
        _value=[AppManager getStringForKey:@"value" fromDic:dic];
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _revisionId=@"";
        _value=@"";
    }
    return self;
}
@end
