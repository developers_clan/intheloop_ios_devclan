//
//  Agency.h
//  ChildCare
//
//  Created by Devclan on 21/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppManager.h"
#import "AgencyImage.h"
#import "AgencySectionID.h"
#import "AgencyType.h"
#import "AgencyPhone.h"
#import "WYW.h"
#import "WWAA.h"
#import "AgencyResources.h"
#import "MainDetails.h"
#import "AgencyContact.h"

@interface Agency : NSObject
@property (nonatomic,strong)NSString* nid;
//@property (nonatomic,strong)NSString* title;
@property (nonatomic,strong)AgencyImage* image;
@property (nonatomic,strong)AgencyImage* logo;
@property (nonatomic,strong)NSString* category;
@property (nonatomic,strong)AgencyPhone* agencyPhone;
@property (nonatomic,strong)AgencyResources* agencyResources;
@property (nonatomic,strong)NSMutableArray* wyw;
@property (nonatomic,strong)NSMutableArray* wwaa;
@property (nonatomic,strong)NSMutableArray* websites;
@property (nonatomic,strong)NSMutableArray* connects;
@property (nonatomic,strong)MainDetails* mainDetails;
@property (nonatomic,strong)MainDetails* WWAAMainDetails;
@property (nonatomic,strong)MainDetails* WYWMainDetails;
@property (strong,nonatomic)NSString* agencyColorCode;
@property (strong,nonatomic)UIColor* agencyColor;
@property (nonatomic,strong)AgencyContact* agencyContact;
- (instancetype)initWithDictionary:(NSDictionary*)dic;
@property (nonatomic,strong)NSMutableArray* homeSections;
@end
