//
//  SecondViewController.h
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "IGLDropDownMenu.h"
#import "Agency.h"
@interface CreateTeamViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) Agency* agency;
@end

