//
//  DEMORightMenuViewController.h
//  RESideMenuStoryboards
//
//  Created by Roman Efimov on 2/11/14.
//  Copyright (c) 2014 Roman Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "MenuRootViewController.h"

@interface MenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak,nonatomic) MenuRootViewController* menuRootViewController;
-(void)updateUI;
@end
