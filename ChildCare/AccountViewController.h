//
//  SecondViewController.h
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "Tab1TierViewController.h"
#import "IGLDropDownMenu.h"

@interface AccountViewController : Tab1TierViewController
@property (weak, nonatomic) IBOutlet UIButton *addChild;
- (IBAction)addChildAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *childName;
@property (weak, nonatomic) IBOutlet UIView *addChildView;
@property (weak, nonatomic) IBOutlet UIView *genderFrame;
@property (strong, nonatomic) IGLDropDownMenu *genderDropDownMenu;

@end

