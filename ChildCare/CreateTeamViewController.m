//
//  SecondViewController.m
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//
#import "ChildCell.h"
#import "CreateTeamViewController.h"
#import "AppDelegate.h"
#import "Child.h"
#import "AppManager.h"
#import "StatusHistoryObj.h"
#import "Constants.h"
#import "UIImage+Additions(Additions).h"
#import "TeamEditorViewController.h"
#import "ChildTeam.h"
#import "UIView+Genie.h"
@interface CreateTeamViewController ()<UITableViewDelegate,UITableViewDataSource>
@end

@implementation CreateTeamViewController{
    AppDelegate* appDelegate;
    __weak IBOutlet UIButton *createBtn;
    NSIndexPath* selectedCell;
    NSLock* lock;
    BOOL locked;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate=[[UIApplication sharedApplication]delegate];
    createBtn.layer.cornerRadius = createBtn.frame.size.height/2.0;
    lock=[NSLock new];
    locked=NO;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (locked==YES) {
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    selectedCell = indexPath;
    [tableView reloadData];
    
    //--------change------//
    createBtn.enabled = YES;
    
    
    createBtn.backgroundColor=[[AppManager sharedManager] appThemeColor];
    
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    Child* c = [[[AppManager sharedManager]children] objectAtIndex:indexPath.row];
    Child* c2=[[AppManager sharedManager]activeChild];
    if (c2!=nil) {
        c2.active=@NO;
    }
    c.active=@YES;
    [appDelegate.managedObjectContext save:NULL];
    
        locked=YES;
        ChildCell* cel=[_tableView cellForRowAtIndexPath:selectedCell];
        UIView* vu = cel.container;
        UIGraphicsBeginImageContext(vu.bounds.size);
        [vu.layer renderInContext:UIGraphicsGetCurrentContext()];
        CGRect rect = [vu convertRect:vu.frame toView:self.view];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    
    
        UIImageView* imgVu = [[UIImageView alloc] initWithFrame:rect];
        imgVu.image = image;
        [createBtn.superview addSubview:imgVu];
        [imgVu genieInTransitionWithDuration:ANIMATION_DURATION_LONG
                            destinationRect:CGRectMake(createBtn.frame.origin.x+createBtn.frame.size.width/2.0, createBtn.frame.origin.y-5, 5, 5)
                            destinationEdge:BCRectEdgeTop
                                 completion:^{
                                     locked=NO;
                                     [imgVu removeFromSuperview];
                                     [self createTeam:createBtn];
                                 }];
    

    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChildCell* cell=[tableView dequeueReusableCellWithIdentifier:@"ChildCell"];

    cell.edit.hidden=YES;
    cell.del.hidden=YES;
    Child* c = [[[AppManager sharedManager]children] objectAtIndex:indexPath.row];
    cell.name.text=c.name;
    cell.indexPath=indexPath;
    cell.selectedBackgroundView=[UIView new];

    if (selectedCell!=nil && selectedCell.row == indexPath.row) {
        cell.container.layer.borderColor = [[AppManager sharedManager] appThemeColor].CGColor;
        cell.icon.image = [UIImage imageNamed:@"team_plceholder_selected"];
    }else{
        cell.container.layer.borderColor = [UIColor colorWithWhite:230/255.0 alpha:1].CGColor;
        cell.icon.image = [UIImage imageNamed:@"team_plceholder_unselected"];
    }
    

    if ([c.gender integerValue]==0) {
        cell.img.image = [UIImage imageNamed:@"account_setting2_user_placeholder_boy"];
    }else{
        cell.img.image = [UIImage imageNamed:@"account_setting2_user_placeholder_girl"];
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[AppManager sharedManager]children].count;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
    
    createBtn.enabled = NO;
    createBtn.backgroundColor=[[AppManager sharedManager] appThemeDisabledColor];
    
    int ind = [[AppManager sharedManager]indexForChild:[[AppManager sharedManager] activeChild]];
    if (ind>-1) {
        //        [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:ind inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        //        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        selectedCell = [NSIndexPath indexPathForRow:ind inSection:0];
        [self.tableView reloadData];
        
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (IBAction)createTeam:(id)sender {

    if (selectedCell==nil) {
        return;
    }
    Child* c = [[[AppManager sharedManager]children] objectAtIndex:selectedCell.row];
    TeamEditorViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamEditorViewController"];
    vc.child = c;
    vc.childTeam=[[AppManager sharedManager] teamForChild:c];
    long n = [[AppManager sharedManager] ContactsForChildTeam:vc.childTeam].count;
    n += [[AppManager sharedManager] teamsAPIForChildTeam:vc.childTeam].count;
    vc.isEditor= n<1;
    if (vc.childTeam==nil) {
        [[[UIAlertView alloc]initWithTitle:@"Error!" message:@"An error occured" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        return;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

@end
