//
//  TeamBody.m
//  ChildCare
//
//  Created by Devclan on 20/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "TeamBody.h"
#import "AppManager.h"

@implementation TeamBody

-(instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [self init];
    if (self && (id)dic != [NSNull null]) {
        _value = [AppManager getStringForKey:@"value" fromDic:dic];
    }
    return self;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _value = @"";
    }
    return self;
}

@end
