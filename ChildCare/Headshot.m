//
//  TeamImage.m
//  ChildCare
//
//  Created by Devclan on 20/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "Headshot.h"
#import "AppManager.h"

@implementation Headshot

-(instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [self init];
    if (self && (id)dic != [NSNull null]) {
        _uri = [AppManager getStringForKey:@"uri" fromDic:dic];
    }
    return self;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _uri = @"";
    }
    return self;
}

@end
