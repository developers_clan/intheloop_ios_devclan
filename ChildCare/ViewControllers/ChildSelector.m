//
//  SecondViewController.m
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//
#import "ChildCell.h"
#import "ChildSelector.h"
#import "AppDelegate.h"
#import "Child.h"
#import "AppManager.h"
#import "StatusHistoryObj.h"
#import "Constants.h"
#import "UIImage+Additions(Additions).h"
#import "TeamEditorViewController.h"
#import "ChildTeam.h"
#import "UIView+Genie.h"
#import <Google/Analytics.h>

@interface ChildSelector ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation ChildSelector{
    AppDelegate* appDelegate;
    __weak IBOutlet UIButton *createBtn;
    NSIndexPath* selectedCell;
    NSLock* lock;
    BOOL locked;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    locked=NO;
    lock = [NSLock new];
    appDelegate=[[UIApplication sharedApplication]delegate];
    createBtn.layer.cornerRadius = createBtn.frame.size.height/2.0;
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (locked==YES) {
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    selectedCell = indexPath;
    [tableView reloadData];
    createBtn.enabled = YES;
    createBtn.backgroundColor=[[AppManager sharedManager] appThemeColor];
    
    //---------------googleAnalytics-----------//
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    Agency* a=[[[AppManager sharedManager]agencies] objectAtIndex:indexPath.row];
    
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyListingScreen
                                                                                        action:[NSString stringWithFormat:@"Select Child %@",a.mainDetails.title]
                                                                                         label:nil
                                                                                         value:nil] build]];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    //---------------googleAnalytics-----------//

    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    Child* c = [[[AppManager sharedManager]children] objectAtIndex:indexPath.row];
    Child* c2=[[AppManager sharedManager]activeChild];
    if (c2!=nil) {
        c2.active=@NO;
    }
    c.active=@YES;
    [appDelegate.managedObjectContext save:NULL];
    
    
            locked=YES;
            ChildCell* cel=[_tableView cellForRowAtIndexPath:selectedCell];
            UIView* vu = cel.container;
            UIGraphicsBeginImageContext(vu.bounds.size);
            [vu.layer renderInContext:UIGraphicsGetCurrentContext()];
            CGRect rect = [vu convertRect:vu.frame toView:self.view];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
    
    
            UIImageView* imgVu = [[UIImageView alloc] initWithFrame:rect];
            imgVu.image = image;
            [createBtn.superview addSubview:imgVu];
            [imgVu genieInTransitionWithDuration:ANIMATION_DURATION_LONG
                                destinationRect:CGRectMake(createBtn.frame.origin.x+createBtn.frame.size.width/2.0, createBtn.frame.origin.y-5, 5, 5)
                                destinationEdge:BCRectEdgeTop
                                     completion:^{
                                         locked=NO;
                                         [imgVu removeFromSuperview];
                                         [self createTeam:createBtn];
                                     }];

    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChildCell* cell=[tableView dequeueReusableCellWithIdentifier:@"ChildCell"];
    cell.edit.hidden=YES;
    cell.del.hidden=YES;
    Child* c = [[[AppManager sharedManager]children] objectAtIndex:indexPath.row];
    cell.name.text=c.name;
    cell.indexPath=indexPath;
    cell.selectedBackgroundView=[UIView new];

    if (selectedCell!=nil && selectedCell.row == indexPath.row) {
        cell.container.layer.borderColor = [[AppManager sharedManager] appThemeColor].CGColor;
        cell.icon.image = [UIImage imageNamed:@"team_plceholder_selected"];
    }else{
        cell.container.layer.borderColor = [UIColor colorWithWhite:230/255.0 alpha:1].CGColor;
        cell.icon.image = [UIImage imageNamed:@"team_plceholder_unselected"];
    }
    

    if ([c.gender integerValue]==0) {
        cell.img.image = [UIImage imageNamed:@"account_setting2_user_placeholder_boy"];
    }else{
        cell.img.image = [UIImage imageNamed:@"account_setting2_user_placeholder_girl"];
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[AppManager sharedManager]children].count;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    [self.tableView reloadData];
    
    
    //---------------googleAnalytics-----------//
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Select Child Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    //---------------googleAnalytics-----------//
    
//    int ind = [[AppManager sharedManager]indexForChild:[[AppManager sharedManager] activeChild]];
//    if (ind>-1) {
//        //        [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:ind inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
//        //        [tableView deselectRowAtIndexPath:indexPath animated:NO];
//        selectedCell = [NSIndexPath indexPathForRow:ind inSection:0];
//        [self.tableView reloadData];
//        createBtn.enabled = YES;
//        createBtn.backgroundColor=[[AppManager sharedManager] appThemeColor];
//    }
    
    
    //----------------------change----------------------//
    createBtn.enabled = NO;
    createBtn.backgroundColor=[[AppManager sharedManager] appThemeDisabledColor];
    //----------------------change----------------------//
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
- (IBAction)createTeam:(id)sender {
    if (_completionDelegate!=nil) {
        [self.completionDelegate viewController:self completedSelectionWithChild:[[AppManager sharedManager] activeChild]];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
