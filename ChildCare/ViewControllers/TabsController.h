//
//  TabsController.h
//  ChildCare
//
//  Created by Devclan on 15/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabsController : UITabBarController<UITabBarControllerDelegate>
-(void)takeToAccountWithMessage:(NSString*)message;
@end
