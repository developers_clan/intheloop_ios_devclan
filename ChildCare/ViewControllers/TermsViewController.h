//
//  TermsViewController.h
//  ChildCare
//
//  Created by Devclan on 15/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *agreeCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *continueBtn;
- (IBAction)chkBoxAction:(id)sender;
- (IBAction)contAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *scrollContentVu;

@end
