//
//  AgencyViewController.h
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "BaseViewController.h"
#import "Agency.h"
#import "AgencyHomeSection.h"
@interface WebViewController : BaseViewController
@property (strong,nonatomic) Agency* agency;
@property BOOL isWYW;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
