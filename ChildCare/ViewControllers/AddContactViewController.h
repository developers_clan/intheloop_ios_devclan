//
//  AddContactViewController.h
//  ChildCare
//
//  Created by Bilal Tahir on 07/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//
#import "ChildTeam.h"
#import <UIKit/UIKit.h>
#import "Contact.h"

@interface AddContactViewController : UIViewController
@property (nonatomic,strong)ChildTeam* childTeam;
@property (nonatomic,strong)Contact* contact;
@end
