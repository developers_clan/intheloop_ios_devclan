//
//  ContactViewController.h
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "Tab1TierViewController.h"

@interface ContactViewController : Tab1TierViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
