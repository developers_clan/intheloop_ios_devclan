//
//  TeamEditorViewController.h
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "BaseViewController.h"
#import "Child.h"
#import "ChildTeam.h"
@interface TeamEditorViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tablwVu;
@property (strong, nonatomic) Child *child;
@property (strong, nonatomic) ChildTeam *childTeam;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *headerContainerVu;
@property (weak, nonatomic) IBOutlet UIImageView *headerBgImg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerBgImgHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *srchFldRightConstraint;
@property BOOL isEditor;
@property (weak, nonatomic) IBOutlet UIButton *srchBtn;
@property (weak, nonatomic) IBOutlet UIImageView *srchUnderline;
- (IBAction)viewTeamAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIView *countBg;
@property (weak, nonatomic) IBOutlet UILabel *teamMemberCount;

@property (strong, nonatomic) IBOutlet UIView *titleView;
@end
