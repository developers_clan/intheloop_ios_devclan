//
//  ToolsPopupViewController.m
//  ChildCare
//
//  Created by Devclan on 18/01/2016.
//  Copyright © 2016 Devclan. All rights reserved.
//
#import "Constants.h"
#import "ToolsPopupViewController.h"

@interface ToolsPopupViewController ()

@end

@implementation ToolsPopupViewController{
    UITapGestureRecognizer* gesture;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self dismiss:NO];
}
- (IBAction)notes:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"calshow://"]];
    [self dismiss:NO];
}
- (IBAction)calendar:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"calshow://"]];
    [self dismiss:NO];
}
-(void)dismiss:(BOOL)animated{
    if (self.view.superview!=nil) {
        [self.view.superview removeGestureRecognizer:gesture];
    }
    if (!animated) {
        [self.view removeFromSuperview];
        return;
    }
    [UIView animateWithDuration:ANIMATION_DURATION_SHORT animations:^{
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+70, self.view.frame.size.width, self.view.frame.size.height)];
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
    
}
-(void)showInViewController:(UIViewController*)vc withFrame:(CGRect)frame{
    [vc.view addSubview:self.view];
    [self.view setFrame:CGRectMake(frame.origin.x, frame.origin.y+70, frame.size.width, frame.size.height)];
    
    [vc.view layoutIfNeeded];
    [vc.view addGestureRecognizer:gesture];
    [UIView animateWithDuration:ANIMATION_DURATION_SHORT animations:^{
        [self.view setFrame:frame];
    } completion:^(BOOL finished) {
        
    }];
}
-(void)dismiss{
    [self dismiss:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
