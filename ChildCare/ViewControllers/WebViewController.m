//
//  AgencyViewController.m
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "WebViewController.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "AgencyWebsite.h"
#import "UIImage+Additions(Additions).h"
#import <SDWebImageManager.h>
#import <Google/Analytics.h>
#import "HomeViewController.h"
#import "MenuRootViewController.h"


@interface WebViewController ()<UIWebViewDelegate>

@end

@implementation WebViewController{
    NSMutableArray* expansionFlags;
    int cellCountTemp;
    MBProgressHUD *p;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _webView.hidden=YES;
    p=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    p.labelText=@"Please wait...";
    //    if (_isWYW==YES) {
    //        self.navigationItem.rightBarButtonItem=nil;
    //    }
    [self populateHTMLAndRefesh];
    //    cellCountTemp=3;
    //    expansionFlags=[NSMutableArray new];
    //    [self resetExpansionFlags];
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back_arrow"] imageScaledToSize:CGSizeMake(20, 20)] style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
    self.navigationItem.leftBarButtonItem=back;
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(goBack:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
}
-(void)goBack:(id)sender{
    MenuRootViewController* mrvc = [[[[AppManager sharedManager]tabsController]viewControllers] objectAtIndex:0];
    UINavigationController* nc=(UINavigationController*)mrvc.contentViewController;
    if ([[nc.viewControllers objectAtIndex:0] isKindOfClass:[HomeViewController class]] && nc.viewControllers.count<3) {
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            self.view.backgroundColor=[[AppManager sharedManager] appThemeColor];
            nc.navigationBarHidden=YES;
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
}



#pragma mark - HTML
-(void)populateHTMLAndRefesh{
    NSString *path;
    if (_isWYW==YES) {
        self.title = _agency.WYWMainDetails.title;
        
        //---------------googleAnalytics-----------//
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"While You Wait Screen-%@",_agency.mainDetails.title]];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        //---------------googleAnalytics-----------//
        
        if (_agency.wyw.count<1) {
            [p hide:YES];
            return;
        }
        
        path = [[NSBundle mainBundle] pathForResource:@"agency" ofType:@"html"];
    }else{
        self.title = _agency.mainDetails.title;
        
        //---------------googleAnalytics-----------//
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Agency Detail Screen-%@",_agency.mainDetails.title]];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        //---------------googleAnalytics-----------//
        
        if (_agency.homeSections.count<1) {
            [p hide:YES];
            return;
        }
        path = [[NSBundle mainBundle] pathForResource:@"agency" ofType:@"html"];
    }
    
    
    NSString *agencyHTMLStr = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    //    [self loadWebviewWithString:agencyHTMLStr];
    //    return;
    path = [[NSBundle mainBundle] pathForResource:@"sectionListItem" ofType:@"html"];
    NSString *sectionListItemTemplate = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    
    NSString* sectionItemsStr=@"";
    if (_isWYW==YES) {
        for (WYW* w in _agency.wyw) {
            sectionItemsStr=[NSString stringWithFormat:@"%@%@",sectionItemsStr,[self populateSectionItemWithTemplate:sectionListItemTemplate wyw:w]];
        }
        if (_agency.agencyResources.markup.length>0) {
            NSString* resourcesHtml = [ResourcesDivHTML stringByReplacingOccurrencesOfString:kAgencyResourcesPlaceholder withString:_agency.agencyResources.markup];
            agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kResourcesDiv withString:resourcesHtml];
        }else{
            agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kResourcesDiv withString:@""];
        }
        
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyBodyPlaceholder withString:_agency.WYWMainDetails.body];
    }else{
        for (AgencyHomeSection* sec in _agency.homeSections) {
            sectionItemsStr=[NSString stringWithFormat:@"%@%@",sectionItemsStr,[self populateSectionItemWithTemplate:sectionListItemTemplate section:sec]];
        }
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kResourcesDiv withString:@""];
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyBodyPlaceholder withString:_agency.mainDetails.body];
    }
    //    NSString* imgURL = [NSString stringWithFormat:@"%@%@/%@",API_BASE_ADDRESS,API_FILES,_agency.image.fileName];
    //    agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyImagePlaceholder withString:imgURL];
    //    agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyTitlePlaceholder withString:_agency.title];
    //    agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencySubtitlePlaceholder withString:_agency.category];
    
    agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyPhoneNumberPlaceholder withString:[NSString stringWithFormat:@"%@%@",_agency.agencyPhone.number,_agency.agencyPhone.extension]];
    agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyPhoneNumberAliasPlaceholder withString:_agency.agencyPhone.number_alias];
    
    
    if (_agency.websites.count>0) {
        AgencyWebsite* ws =[_agency.websites objectAtIndex:0];
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyWebsiteTitlePlaceholder withString:ws.title];
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyWebsiteUrlPlaceholder withString:ws.url];
    }else{
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:@"<li class=\"website-background\"><p><a href=\"agencyWebsiteUrlPlaceholder\">agencyWebsiteTitlePlaceholder</a></p></li>" withString:@""];
    }
    
    if (_agency.connects.count>0){
        AgencyWebsite* ws =[_agency.connects objectAtIndex:0];
        
        if ([ws.title isEqualToString:@"Events"]) {
            agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyConnectIconPlaceholder withString:@"cdp-background"];
        }else{
            agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyConnectIconPlaceholder withString:@"events-background"];
        }
        //
        
        
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyConnectTitlePlaceholder withString:ws.title];
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyConnectUrlPlaceholder withString:ws.url];
    }else{
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:@"<li class=\"agencyConnectIconPlaceholder\"><p><a href=\"agencyConnectUrlPlaceholder\">agencyConnectTitlePlaceholder</a></p></li>" withString:@""];
    }
    
    
    
    
    agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kSectionItemsPlaceholder withString:sectionItemsStr];
    
    NSArray *parts = [_agency.image.uri componentsSeparatedByString:@"/"];
    NSString *filename = [parts lastObject];
    NSString *imgPath = [AppManager loadImageWithName:filename];
    if (![imgPath isEqualToString:@""]) {
        
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyBannerPlaceholder withString:imgPath];
        
    } else {
        if ([[SDWebImageManager sharedManager] diskImageExistsForURL:[NSURL URLWithString:_agency.image.uri]]) {
            agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyBannerPlaceholder withString:[[[SDWebImageManager sharedManager] imageCache]defaultCachePathForKey:_agency.image.uri]];
        }else
        {
            agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyBannerPlaceholder withString:_agency.image.uri];
        }
    }
    
    if (_agency.agencyContact!=nil) {
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kBorderColorPlaceholder withString:_agency.agencyContact.agencyColorCode];
    }else{
        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kBorderColorPlaceholder withString:@"#00000000"];
    }
    
    
    //    if ([_agency.nid isEqualToString:@"5"]) {
    //        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyBannerPlaceholder withString:@"background-ctn-banner.png"];
    //        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kContactBorderPlaceholder withString:@"contact_area border_top_red"];
    //    }else if ([_agency.nid isEqualToString:@"6"]) {
    //        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyBannerPlaceholder withString:@"background_cdp_banner.png"];
    //        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kContactBorderPlaceholder withString:@"contact_area border_top_yellow"];
    //    }else{
    //        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kAgencyBannerPlaceholder withString:@"background-yr-banner.png"];
    //        agencyHTMLStr=[agencyHTMLStr stringByReplacingOccurrencesOfString:kContactBorderPlaceholder withString:@"contact_area border_top_blue"];
    //    }
    [self loadWebviewWithString:agencyHTMLStr];
}
-(void)loadWebviewWithString:(NSString*)str{
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:bundlePath];
    [_webView loadHTMLString:str baseURL:baseURL];
}
-(NSString*)populateSectionItemWithTemplate:(NSString*)template section:(AgencyHomeSection*)section{
    NSString* item=@"";
    item=[template stringByReplacingOccurrencesOfString:kSectionTitlePlaceholder withString:section.title.markup];
    item=[item stringByReplacingOccurrencesOfString:kSectionBodyPlaceholder withString:section.body.markup];
    return item;
}
-(NSString*)populateSectionItemWithTemplate:(NSString*)template wyw:(WYW*)wyw{
    NSString* item=@"";
    item=[template stringByReplacingOccurrencesOfString:kSectionTitlePlaceholder withString:wyw.title.markup];
    item=[item stringByReplacingOccurrencesOfString:kSectionBodyPlaceholder withString:wyw.body.markup];
    return item;
}


# pragma mark - UIWebViewDelegate
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [p hide:YES];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    _webView.hidden=NO;
    [p hide:YES];
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        
        AgencyWebsite* aws = nil;
        /////
        if (_agency.websites.count>0) {
            AgencyWebsite* ws =[_agency.websites objectAtIndex:0];
            if ([request.URL.absoluteString isEqualToString:ws.url]) {
                aws=ws;
            }
        }
        
        if (aws==nil && _agency.connects.count>0){
            AgencyWebsite* ws =[_agency.connects objectAtIndex:0];
            if ([request.URL.absoluteString isEqualToString:ws.url]) {
                aws=ws;
            }
        }
        if (aws!=nil) {
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                                action:[NSString stringWithFormat:@"%@ -Contact Block Footer- %@",_agency.mainDetails.title,aws.title]
                                                                                                 label:_agency.mainDetails.title
                                                                                                 value:nil] build]];
        }
        //////
        if ([request.URL.scheme isEqualToString:@"telprompt"]) {
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                                action:[NSString stringWithFormat:@"%@ -Contact Block Footer- %@",_agency.mainDetails.title,kActAgencyPhonePressed]
                                                                                                 label:_agency.mainDetails.title
                                                                                                 value:nil] build]];
            
            [[UIApplication sharedApplication] openURL:request.URL];
        }
        if ([[UIApplication sharedApplication] canOpenURL:request.URL]) {
            [[UIApplication sharedApplication] openURL:request.URL];
            return NO;
        }
    }
    return YES;
}

@end
