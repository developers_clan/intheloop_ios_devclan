//
//  TermsViewController.m
//  ChildCare
//
//  Created by Devclan on 15/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "TermsViewController.h"
#import "AppManager.h"
#import "UIImage+Additions(Additions).h"
#import "Constants.h"
#import <Google/Analytics.h>

@interface TermsViewController ()
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollVuHeight;
@property (weak, nonatomic) IBOutlet UITextView *txtVu;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *txtVuHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *txtVuTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *headerTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *headerCenterConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollVu;

@property (weak, nonatomic) IBOutlet UIButton *termsBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopConstraint;
@end

@implementation TermsViewController{
    NSString* terms ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    terms = @"This \"In The Loop\" app is merely to inform end users of the services provided by the agencies and to provide some tools allowing the end user to store specific information about their child or children.\n\nAll information that is inputted by the end user is stored on your individual devices only and is not connected to any agency’s databases.\n\nIf you uninstall the app from your device, all the information you inputted will all be erased. If you reinstall the app, the default settings of the app will apply and all the information previously stored on your device will need to be inputted again.\n\nThe agencies are not responsible for information that might be leaked, stolen or captured from your individual devices.\n\nThe end user is taking full responsibility for the data that is inputted and stored on their individual devices.";
//    
    terms = @"This “In The Loop” app is merely to inform end users of the services provided by the agencies and to provide some tools allowing the end user to store specific information about their child or children onto their personal phones.\n\nAll information that is inputted by the end user is stored on the phone only and is not connected to any agency’s database.\n\nIf you uninstall the app from your phone, all the information you inputted will be erased. If you reinstall the app, the default settings of the app will apply and all the information previously stored on your smartphone will need to be inputted again.\n\nThe agencies are not responsible for information that might be leaked, stolen or captured from your individual smartphones.\n\nThe end user is taking full responsibility for the data that is inputted and stored on their individual smartphones.";
            
    _scrollVuHeight.constant = MAX(self.view.frame.size.height-415,150);
    if (self.view.frame.size.height<700) {
        _logoTopConstraint.constant=8;
    }
    _agreeCheckBox.titleLabel.adjustsFontSizeToFitWidth=YES;
    _agreeCheckBox.titleLabel.minimumScaleFactor = 8./_agreeCheckBox.titleLabel.font.pointSize;
    // Do any additional setup after loading the view.
    _continueBtn.layer.cornerRadius=_continueBtn.frame.size.height/2.0;
    
    [self.view layoutIfNeeded];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    
    //---------------googleAnalytics-----------//
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Terms & Condition Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    //---------------googleAnalytics-----------//
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    [_scrollVu setContentOffset:CGPointMake(0, 100)];
}
- (IBAction)showTerms:(id)sender {
    
    CGFloat fixedWidth = _txtVu.frame.size.width;
    _txtVu.text = terms;
    CGSize newSize = [_txtVu sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = _txtVu.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:ANIMATION_DURATION_SHORT animations:^{
        _scrollVuHeight.constant = 150 + newFrame.size.height;
        
        
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            [self.scrollContentVu removeConstraint:_headerCenterConstraint];
            [self.scrollContentVu addConstraint:_headerTopConstraint];
            [self.txtVu removeConstraint:_txtVuHeightConstraint];
            [self.scrollContentVu addConstraint:_txtVuTopConstraint];
        }else{
            _headerCenterConstraint.active=NO;
            _headerTopConstraint.active=YES;
            _txtVuHeightConstraint.active=NO;
            _txtVuTopConstraint.active=YES;
        }
        [_scrollVu setContentOffset:CGPointMake(0, _txtVu.frame.origin.y)];
        [self.view layoutIfNeeded];
    }];
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)chkBoxAction:(id)sender {
    _agreeCheckBox.selected=!_agreeCheckBox.selected;
    if (_agreeCheckBox.selected==YES) {
        _continueBtn.backgroundColor=[[AppManager sharedManager] appThemeColor];
    }else{
        _continueBtn.backgroundColor=[[AppManager sharedManager] appThemeDisabledColor];
    }
}

- (IBAction)contAction:(id)sender {
    if (_agreeCheckBox.selected==NO) {
        [[[UIAlertView alloc] initWithTitle:@"Information!" message:@"You need to agree with terms and conditions before you continue" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:kTermsAccepted];
    [ud synchronize];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController* vc=[self.storyboard instantiateViewControllerWithIdentifier:@"InitialSetupNavigationController"];
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            [[[[UIApplication sharedApplication] delegate] window] setRootViewController:vc];
        }else{
            [self.view addSubview:vc.view];
            [UIView transitionWithView:self.view duration:ANIMATION_DURATION options:UIViewAnimationOptionTransitionCurlUp animations:^{
                
            } completion:^(BOOL finished) {
                [vc.view removeFromSuperview];
                [[[[UIApplication sharedApplication] delegate] window] setRootViewController:vc];
            }];
        }
    });
}
@end
