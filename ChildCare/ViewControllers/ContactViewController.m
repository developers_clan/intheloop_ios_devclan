//
//  ContactViewController.m
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "ContactViewController.h"
#import "ContactCell.h"
#import "AppManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constants.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "JSONResponse.h"
#import "AgencyContact.h"
#import <Google/Analytics.h>
#import "Agency.h"


@interface ContactViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchAndLoadAgenciesContactData];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    //---------------googleAnalytics-----------//
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Contact Us Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    //---------------googleAnalytics-----------//
    
    [super viewWillAppear:animated];
    [_tableView reloadData];
}
# pragma mark - tableview
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ContactCell* t=[tableView dequeueReusableCellWithIdentifier:@"ContactCell"];
    AgencyContact* ag=[[[AppManager sharedManager] agenciesContacts] objectAtIndex:indexPath.row];
    NSString* urlStr = ag.agencyImage.uri;
//    NSString* urlStr = [NSString stringWithFormat:@"%@%@/%@",API_BASE_ADDRESS,API_FILES,ag.agencyImage.fileName];
    
    [t.img sd_setImageWithURL:[NSURL URLWithString:urlStr]
                placeholderImage:[UIImage imageNamed:@"contact_unselected"]];
    t.phoneBg.backgroundColor = ag.agencyColor;
    t.number.text = ag.agencyPhone.number_alias;
    t.containerVu.layer.borderColor = ag.agencyColor.CGColor;
    return t;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[AppManager sharedManager]agenciesContacts]count];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    AgencyContact* ag=[[[AppManager sharedManager] agenciesContacts] objectAtIndex:indexPath.row];
    

    Agency *a = [[AppManager sharedManager] agencyForId:ag.agencyId];
    
    
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatContactUs
                                                                                        action:[NSString stringWithFormat:@"%@-%@",kActContactUsAgencySelected,a.mainDetails.title]
                                                                                         label:nil
                                                                                         value:nil] build]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@%@",ag.agencyPhone.number,ag.agencyPhone.extension]]];
}
#pragma mark - communication
-(void)fetchAndLoadAgenciesContactData{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kAgenciesContacts];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSDate* lastFetchedTime=nil;
    double diff = 0.0;
    if (jsonResp!=nil) {
        lastFetchedTime=jsonResp.date;
        diff = [[NSDate date]timeIntervalSinceDate:lastFetchedTime];
    }
    
    
    
    if (lastFetchedTime==nil || diff>kAppDataRefreshInterval) {
        
        MBProgressHUD *p=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        p.labelText=@"Please wait...";
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
        [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [p hide:YES];
            NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:responseObject];
            [[AppManager sharedManager] insertOrUpdateResponse:myData url:urlStr];
            [self reloadAgenciesContactsDataLocally:urlStr];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [p hide:YES];
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }];
    }else{
        [self reloadAgenciesContactsDataLocally:urlStr];
    }
    
}
-(void)reloadAgenciesContactsDataLocally:(NSString*)key{
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kAgenciesContacts];
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:urlStr];
    NSArray* agenciesArray = [NSKeyedUnarchiver unarchiveObjectWithData:jsonResp.value];
    if (agenciesArray==nil) {
        return;
    }
    [[[AppManager sharedManager]agenciesContacts] removeAllObjects];
    for (NSDictionary* opt in agenciesArray) {
        AgencyContact* a=[[AgencyContact alloc] initWithDictionary:opt];
        [[[AppManager sharedManager]agenciesContacts] addObject:a];
    }
    [_tableView reloadData];
}

@end
