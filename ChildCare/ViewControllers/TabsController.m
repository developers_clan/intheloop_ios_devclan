//
//  TabsController.m
//  ChildCare
//
//  Created by Devclan on 15/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "TabsController.h"
#import "Tab1TierViewController.h"
#import "UIView+Genie.h"
#import "AppManager.h"
#import "ToolsViewController.h"
#import "MenuRootViewController.h"
#import "ToolsPopupViewController.h"
#import "TeamEditorViewController.h"
#import "CreateTeamViewController.h"


@interface TabsController ()<UIAlertViewDelegate>

@end

@implementation TabsController{
        CGRect currentTabFrame;
    BOOL locked;
    NSLock *lock;
    ToolsPopupViewController* popVC;
    UINavigationController* selectedNVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    popVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ToolsPopupViewController"];
    lock = [NSLock new];
    currentTabFrame=CGRectZero;
    self.delegate=self;
    [[UITabBar appearance] setTintColor:[[AppManager sharedManager] appThemeColor]];
    [[AppManager sharedManager]setTabsController:self];
    for(UITabBarItem * tabBarItem in self.tabBar.items){
        tabBarItem.title = @"";
        tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    if (selectedNVC != nil) {
        [selectedNVC popToRootViewControllerAnimated:NO];
    }
    locked=YES;
    
    
    if ([viewController isKindOfClass:[UINavigationController class]] && ([[((UINavigationController*)viewController).viewControllers lastObject] isKindOfClass:[CreateTeamViewController class]] || [[((UINavigationController*)viewController).viewControllers lastObject] isKindOfClass:[TeamEditorViewController class]])) {
        [self updateTeamTab];
        
    }
    [viewController.view genieOutTransitionWithDuration:0.4 startRect:currentTabFrame startEdge:BCRectEdgeTop completion:^{
        locked=NO;
    }];
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    int tabWidth=tabBar.frame.size.width/4;
    int one3rdWidth=tabWidth/3;
    int two3rdWidth=tabWidth*2/3;
    int y=tabBar.frame.origin.y;
    currentTabFrame=CGRectMake(item.tag*tabWidth+one3rdWidth, y, tabWidth-two3rdWidth, tabBar.frame.size.height);
}
-(void)takeToAccountWithMessage:(NSString*)message{
    [[[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"Go To Account Settings" otherButtonTitles:nil, nil] show];
}
-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    [popVC dismiss:NO];
    if (locked) {
        return NO;
    }
    if([self.selectedViewController isKindOfClass:[UINavigationController class]]){
        selectedNVC = (UINavigationController*)self.selectedViewController;
    }
    else if ([self.selectedViewController isKindOfClass:[MenuRootViewController class]]) {
        MenuRootViewController* mrc = (MenuRootViewController*)self.selectedViewController;
        selectedNVC = (UINavigationController*)mrc.contentViewController;
    }
    
    if (selectedNVC==nil || selectedNVC.viewControllers.count<1 || ![selectedNVC isKindOfClass:[UINavigationController class]]) {
        return NO;
    }
    //UIViewController *cnt = [selectedNVC.viewControllers lastObject];
    
    if ([viewController isKindOfClass:[UINavigationController class]] && ([[((UINavigationController*)viewController).viewControllers lastObject] isKindOfClass:[CreateTeamViewController class]] || [[((UINavigationController*)viewController).viewControllers lastObject] isKindOfClass:[TeamEditorViewController class]])) {
        if ([[AppManager sharedManager]children].count<1) {
            [self takeToAccountWithMessage:@"You have not setup your account. To proceed, please add your child/children."];
            return NO;
        }
        
    }
    return YES;
}
-(BOOL)updateTeamTab{
    UINavigationController* nvc = [self.viewControllers objectAtIndex:2];
    UIViewController* lastOb = [nvc.viewControllers lastObject];
    if([[AppManager sharedManager]children].count==1){
        
        if (![lastOb isKindOfClass:[TeamEditorViewController class]]) {
            Child* c = [[AppManager sharedManager] activeChild];
            if (c==nil) {
                Child* c2 = [[[AppManager sharedManager] children] objectAtIndex:0];
                c2.active=@YES;
                [[[[AppManager sharedManager]appDelegate]managedObjectContext]save:NULL];
            }
            TeamEditorViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamEditorViewController"];
            vc.child = c;
            vc.childTeam=[[AppManager sharedManager] teamForChild:c];
            long n = [[AppManager sharedManager] ContactsForChildTeam:vc.childTeam].count;
            n += [[AppManager sharedManager] teamsAPIForChildTeam:vc.childTeam].count;
            vc.isEditor= n<1;
            
            UINavigationController* nvc = [self.viewControllers objectAtIndex:2];
            [nvc setViewControllers:[NSArray arrayWithObject: vc] animated:NO];
        }
    }else{
        if (![lastOb isKindOfClass:[CreateTeamViewController class]]) {
            CreateTeamViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateTeamViewController"];
            UINavigationController* nvc = [self.viewControllers objectAtIndex:2];
            [nvc setViewControllers:[NSArray arrayWithObject: vc] animated:NO];
        }
    }
    return YES;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    self.selectedIndex=1;
}
/*

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
 
 [popVC.view setFrame:CGRectMake(fr.origin.x-25, fr.origin.y-100, 70, 100)];
 [cnt.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:popVC action:@selector(dismiss)]];
 [cnt.view addSubview:popVC.view];
 
}
*/

@end
