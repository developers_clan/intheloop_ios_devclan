//
//  ToolsPopupViewController.h
//  ChildCare
//
//  Created by Devclan on 18/01/2016.
//  Copyright © 2016 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToolsPopupViewController : UIViewController
-(void)dismiss:(BOOL)animated;
-(void)showInViewController:(UIViewController*)vc withFrame:(CGRect)frame;
@end
