//
//  AddContactViewController.m
//  ChildCare
//
//  Created by Bilal Tahir on 07/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "AddContactViewController.h"
#import "Contact.h"
#import "AppDelegate.h"
#import "AppManager.h"
#import <JDStatusBarNotification.h>
#import "AppManager.h"
#import "Constants.h"
#import "IGLDropDownMenu.h"
#import "UIImage+Additions(Additions).h"
#import <Google/Analytics.h>

@interface AddContactViewController ()<IGLDropDownMenuDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UIView *lineName;
@property (weak, nonatomic) IBOutlet UILabel *errName;
@property (weak, nonatomic) IBOutlet UIImageView *errorIcon;

@property (weak, nonatomic) IBOutlet UITextField *txtDiscipline;
@property (weak, nonatomic) IBOutlet UIView *lineDiscipline;
@property (weak, nonatomic) IBOutlet UILabel *errDiscipline;
@property (weak, nonatomic) IBOutlet UIImageView *errorIconDiscipline;

@property (weak, nonatomic) IBOutlet UITextField *txtAgency;

@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UIView *linePhone;
@property (weak, nonatomic) IBOutlet UILabel *errPhone;
@property (weak, nonatomic) IBOutlet UIImageView *errorIconPhone;


@property (weak, nonatomic) IBOutlet UITextField *txtExtesnion;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIView *lineEmail;
@property (weak, nonatomic) IBOutlet UILabel *errEmail;
@property (weak, nonatomic) IBOutlet UIImageView *errorIconEmail;



@property (weak, nonatomic) IBOutlet UIButton *addContactBtn;

@property (weak, nonatomic) IBOutlet UIView *genderFrame;
@property (weak, nonatomic) IBOutlet UIView *lineGender;
@property (weak, nonatomic) IBOutlet UILabel *errGender;
@property (weak, nonatomic) IBOutlet UIImageView *errorIconGender;

@property (strong, nonatomic) IGLDropDownMenu *genderDropDownMenu;
@end

@implementation AddContactViewController
{
    AppDelegate *appDelegate;
    int genderInd;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _addContactBtn.layer.cornerRadius=_addContactBtn.frame.size.height/2.0;
    // Do any additional setup after loading the view.
    appDelegate=[[UIApplication sharedApplication]delegate];
    
    genderInd=-1;
    NSArray* genders = @[@"Male",@"Female"];
    self.genderDropDownMenu = [[IGLDropDownMenu alloc] init];
    self.genderDropDownMenu.delegate=self;
    self.genderDropDownMenu.menuText = @"Set Gender* ";
    self.genderDropDownMenu.paddingLeft = 2;
    self.genderDropDownMenu.layer.shadowColor=[UIColor clearColor].CGColor;
    
    self.genderDropDownMenu.type = IGLDropDownMenuTypeNormal;
    self.genderDropDownMenu.dropdownIconImage = [[UIImage imageNamed:@"dropdown"] imageScaledToSize:CGSizeMake(20, 11)];
    [self setItems:genders ToMenu:self.genderDropDownMenu];
    [self.genderFrame.superview addSubview:self.genderDropDownMenu];
    self.genderFrame.hidden = YES;
    
    _txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name (e.g. Dr. John Smith)*" attributes:@{NSForegroundColorAttributeName: [[AppManager sharedManager]placeholderColor]}];
    _txtDiscipline.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Discipline (e.g. Physiotherapist)" attributes:@{NSForegroundColorAttributeName: [[AppManager sharedManager]placeholderColor]}];
    _txtAgency.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Agency (e.g. Boxgrove Medical Center)" attributes:@{NSForegroundColorAttributeName: [[AppManager sharedManager]placeholderColor]}];
    _txtPhone.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Phone Number (e.g. 1-123-123-1234)*" attributes:@{NSForegroundColorAttributeName: [[AppManager sharedManager]placeholderColor]}];
    _txtExtesnion.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Extension (If applicable)" attributes:@{NSForegroundColorAttributeName: [[AppManager sharedManager]placeholderColor]}];
    _txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email (e.g. john.smith@boxgrove.com)" attributes:@{NSForegroundColorAttributeName: [[AppManager sharedManager]placeholderColor]}];
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(btnCancel_Tapd:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    if (_contact!=nil) {
        _txtName.text=_contact.name;
        genderInd = [_contact.gender intValue];
        [self.genderDropDownMenu selectItemAtIndex:genderInd];
        _txtDiscipline.text=_contact.discipline;
        _txtAgency.text=_contact.agency;
        _txtPhone.text=_contact.phone;
        _txtExtesnion.text=_contact.extension;
        _txtEmail.text=_contact.email;
        [_addContactBtn setTitle:@"SAVE" forState:UIControlStateNormal];
        self.title=@"EDIT CONTACT";
    }else{
        self.title=@"ADD CONTACT";
    }
}
- (IBAction)editing:(id)sender {
    UITextField* fld = (UITextField*)sender;
    switch (fld.tag) {
        case 0:
            [self validateTxtField:_txtName lbl:_errName icon:_errorIcon line:_lineName];
            break;
        case 1:
            //[self validateTxtField:_txtDiscipline lbl:_errDiscipline icon:_errorIconDiscipline line:_lineDiscipline];
            break;
        case 2:
            [self validateTxtField:_txtPhone lbl:_errPhone icon:_errorIconPhone line:_linePhone];
            break;
        case 3:
//            [self validateTxtField:_txtEmail lbl:_errEmail icon:_errorIconEmail line:_lineEmail];
            break;
        default:
            break;
    }
    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //---------------googleAnalytics-----------//
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"AddContact Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    //---------------googleAnalytics-----------//
}

-(BOOL)validateTxtField:(UITextField*)txt lbl:(UILabel*)lbl icon:(UIImageView*)icon line:(UIView*)line{
    if ((txt==_txtEmail && ![self isValidEmail:txt.text]) || txt.text.length<1) {
        lbl.alpha=0;
        icon.alpha=0;
        lbl.hidden=NO;
        icon.hidden=NO;
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            lbl.alpha=1;
            icon.alpha=1;
            line.backgroundColor = [[AppManager sharedManager] appThemeColor];
        } completion:^(BOOL finished) {
            
        }];
        return NO;
    }else{
        lbl.hidden=YES;
        icon.hidden=YES;
        line.backgroundColor = [UIColor colorWithWhite:230/255.0 alpha:1];
        return YES;
    }
}
-(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)btnSave_Tapd:(id)sender {
    if (![self validateTxtField:_txtName lbl:_errName icon:_errorIcon line:_lineName] ) {
        return;
    }
    if (genderInd<0) {
        _errGender.alpha=0;
        _errorIconGender.alpha=0;
        _errGender.hidden=NO;
        _errorIconGender.hidden=NO;
        _addContactBtn.transform = CGAffineTransformMakeScale(0.5,0.5);
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            _errGender.alpha=1;
            _errorIconGender.alpha=1;
            _addContactBtn.transform = CGAffineTransformMakeScale(1,1);
            _lineGender.backgroundColor = [[AppManager sharedManager] appThemeColor];
        } completion:^(BOOL finished) {
            
        }];
        return;
    }else{
        _errGender.hidden=YES;
        _errorIconGender.hidden=YES;
        _lineGender.backgroundColor = [UIColor colorWithWhite:230/255.0 alpha:1];
    }

    
    
    if (
//        ![self validateTxtField:_txtDiscipline lbl:_errDiscipline icon:_errorIconDiscipline line:_lineDiscipline] ||
        ![self validateTxtField:_txtPhone lbl:_errPhone icon:_errorIconPhone line:_linePhone]// ||
//        ![self validateTxtField:_txtEmail lbl:_errEmail icon:_errorIconEmail line:_lineEmail] ||
//        ![self isValidEmail:_txtEmail.text]
        ) {
        return;
    }
    
//    if (_txtDiscipline.text.length<2) {
//        _txtDiscipline.text=@" ";
//    }
//    if (_txtPhone.text.length<2) {
//        _txtPhone.text=@" ";
//    }
    if (_contact==nil) {
        _contact = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:appDelegate.managedObjectContext];
    }
    
    
    _contact.name = _txtName.text;
    _contact.discipline = _txtDiscipline.text;
    _contact.agency = _txtAgency.text;
    _contact.phone = _txtPhone.text;
    _contact.extension = _txtExtesnion.text;
    _contact.email = _txtEmail.text;
    _contact.gender=[NSNumber numberWithInt:genderInd];
//    contact.identity = [NSNumber numberWithInt:[[AppManager sharedManager]getNextContactId]];
    NSError *error;
    if (![appDelegate.managedObjectContext save:&error]) {
        [[[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
    }else{
        [JDStatusBarNotification showWithStatus:@"Contact saved" dismissAfter:STATUSBAR_NOTIFICATION_DURATION styleName:JDStatusBarStyleDefault];
        [[AppManager sharedManager] populateDB];
        
        if (_childTeam!=nil && ![AppManager team:_childTeam containsContact:_contact]) {
            _childTeam.contactsIdsCSV=[_childTeam.contactsIdsCSV stringByAppendingString:[NSString stringWithFormat:@"%@%@",_contact.objectID.URIRepresentation.absoluteString,SEPARATOR]];
            if (![appDelegate.managedObjectContext save:&error]) {
                [[[UIAlertView alloc]initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            }
            [[AppManager sharedManager] populateDB];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    

}

- (IBAction)btnCancel_Tapd:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.genderDropDownMenu setFrame:_genderFrame.frame];
    [_genderDropDownMenu reloadView];
    if (genderInd>-1) {
        [_genderDropDownMenu selectItemAtIndex:genderInd];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - IGLDropdown
- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu selectedItemAtIndex:(NSInteger)index
{
    if (dropDownMenu==self.genderDropDownMenu) {
        genderInd=(int)index;
    }
}
-(void)setItems:(NSArray*)items ToMenu:(IGLDropDownMenu*)menu{
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < items.count; i++) {
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        item.bgView.backgroundColor=[UIColor colorWithWhite:246/255.0 alpha:1];
        item.borderColor= [UIColor colorWithWhite:220/255.0 alpha:1];
        NSString* text = items[i];
        if ([text isEqualToString:@"Male"]) {
            item.iconImage = [[UIImage imageNamed:@"my_team_user_boy_placeholder"] imageScaledToSize:CGSizeMake(15, 15)];
        }else if([text isEqualToString:@"Female"]){
            item.iconImage = [[UIImage imageNamed:@"my_team_user_girl_placeholder"] imageScaledToSize:CGSizeMake(15, 15)];
        }
        [item setText:items[i]];
        [dropdownItems addObject:item];
    }
    menu.dropDownItems=dropdownItems;
    [menu reloadView];
    [menu foldView];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
