//
//  MSHStatusViewController.m
//  ChildCare
//
//  Created by Devclan on 30/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//
#import "ProgramCell.h"
#import "ExpandedProgramCell.h"
#import "MSHStatusViewController.h"
#import "MSHCheckBoxCell.h"
#import "AgencyHomeSection.h"
#import "StatusHistoryObj.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+Additions(Additions).h"
#import "AgencyWebsite.h"
#import "ChildSelector.h"
#import "MenuRootViewController.h"
#import "WebViewController.h"
#import <Google/Analytics.h>
#import <Google/Analytics.h>




@interface MSHStatusViewController ()<UITableViewDataSource,UITableViewDelegate,StatusUpdateCallBackDelegate>

@end

@implementation MSHStatusViewController{

    __weak IBOutlet UILabel *desc;
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIImageView *telIcon;
    __weak IBOutlet UIView *headerStrip;
    __weak IBOutlet UIButton *saveBtn;
    __weak IBOutlet UIImageView *webIcon;
    __weak IBOutlet UILabel *name;
    __weak IBOutlet UIImageView *childIcon;
    __weak IBOutlet UIImageView *agencyLogo;
    __weak IBOutlet UIButton *connectBtn;
    __weak IBOutlet UIButton *websiteBtn;
    __weak IBOutlet UIButton *phoneBtn;
    __weak IBOutlet UITableView *tableVu;
    __weak IBOutlet UIView *footerStrip;
    __weak IBOutlet UIImageView *connectIcon;
}

-(void)showChildSelector{
    [[[[AppManager sharedManager]appDelegate]managedObjectContext]rollback];
    ChildSelector* selectVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChildSelector"];
    [self presentViewController:selectVC animated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [childIcon addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showChildSelector)]];
    [name addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showChildSelector)]];

    if (_agency.connects.count>0) {
        AgencyWebsite* st = [_agency.connects objectAtIndex:0];
        [connectBtn setTitle:st.title forState:UIControlStateNormal];
        if ([st.title isEqualToString:@"Events"]) {
            connectIcon.image=[UIImage imageNamed:@"agency_events_bg.png"];
        }else{
            connectIcon.image=[UIImage imageNamed:@"agency_cdp_connect_bg.png"];
        }
    }else{
        connectBtn.hidden=YES;
        connectIcon.hidden=YES;
    }
    if (_agency.websites.count>0) {
        AgencyWebsite* st = [_agency.websites objectAtIndex:0];
        [websiteBtn setTitle:st.title forState:UIControlStateNormal];
    }else{
        webIcon.hidden=YES;
        websiteBtn.hidden=YES;
    }
    if (_agency.agencyPhone) {
        [phoneBtn setTitle:_agency.agencyPhone.number_alias forState:UIControlStateNormal];
    }else{
        phoneBtn.hidden=YES;
        telIcon.hidden=YES;
    }
    self.title = _agency.WWAAMainDetails.title;
    NSAttributedString* str = [[NSAttributedString alloc]
                               initWithData: [_agency.WWAAMainDetails.body dataUsingEncoding:NSUTF8StringEncoding]
                               options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                               documentAttributes: nil
                               error: NULL];
    desc.text = [str.string stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceCharacterSet]];
    
    
    _programStatusesData=[NSMutableArray new];
    saveBtn.layer.cornerRadius = saveBtn.frame.size.height/2.0;
    // Do any additional setup after loading the view.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back_arrow"] imageScaledToSize:CGSizeMake(20, 20)] style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
    self.navigationItem.leftBarButtonItem=back;
    headerStrip.backgroundColor = _agency.agencyColor;
    footerStrip.backgroundColor = _agency.agencyColor;
    [agencyLogo sd_setImageWithURL:[NSURL URLWithString:_agency.logo.uri]
                  placeholderImage:[UIImage new]];
    
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(goBack:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
}
-(void)goBack:(id)sender{
    [[[[AppManager sharedManager]appDelegate]managedObjectContext]rollback];
    _WWAAMSHVC.shouldGoBackIfNoStatusSet=YES;
    
    [_WWAAMSHVC reloadData];
    if (_WWAAMSHVC.indicesOfSetStatuses.count<1) {
        MenuRootViewController* mrvc = [[[[AppManager sharedManager]tabsController]viewControllers] objectAtIndex:0];
        UINavigationController* nc=(UINavigationController*)mrvc.contentViewController;
        [nc setViewControllers:@[[nc.viewControllers objectAtIndex:0],[nc.viewControllers objectAtIndex:1]] animated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(BOOL)isAnyStatusSet{
    for (int i=0;i<_programStatusesData.count;i++) {
        ProgramStatusPath* pt = [_programStatusesData objectAtIndex:i];
        if ([pt.programInd intValue] > -1 && [pt.stageInd intValue] > -1) {
            return YES;
        }
    }
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //---------------googleAnalytics-----------//
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"My Services Selection Screen-%@",_agency.mainDetails.title]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    //---------------googleAnalytics-----------//

    _activeChild=[[AppManager sharedManager]activeChild];
    if (_activeChild==nil) {
        [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Please set active child from account" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        return;
    }
    
    if ([_activeChild.gender integerValue]==0) {
        childIcon.image = [UIImage imageNamed:@"where_are_banner_boy_user_placeholder"];
    }else{
        childIcon.image = [UIImage imageNamed:@"where_are_banner_girl_user_placeholder"];
    }
    name.text = _activeChild.name;
    [self populateStatuses];
    [tableVu reloadData];
    [self setViewHeightAnimated:NO];
}
-(void)setViewHeightAnimated:(BOOL)animated{
    int height = [AppManager frameForTextInConstraint:CGSizeMake(self.view.frame.size.width-32, CGFLOAT_MAX) string:desc.text fontName:@"Lato-Regular" fontSize:18.0].size.height;;
    for (int i=0; i<_programStatusesData.count; i++) {
        height+=[self tableView:tableVu heightForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    if (animated==YES) {
        [self.view layoutIfNeeded];

        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [UIView animateWithDuration:ANIMATION_DURATION animations:^{

                
                
                _totalHeight.constant=MAX(516+height,self.view.frame.size.height);
                [self.view layoutIfNeeded];
            }];
        }];
    }else{
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            _totalHeight.constant=MAX(516+height,self.view.frame.size.height);
            [self.view layoutIfNeeded];
        }];
    }
    

}
-(void)populateStatuses{
    [_programStatusesData removeAllObjects];
    
//    childName.text=_activeChild.name;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ProgramStatusPath"
                                              inManagedObjectContext:[[AppManager sharedManager] appDelegate].managedObjectContext];
    [fetchRequest setEntity:entity];//agencyId=%@ AND childId=%@ AND programInd=%li
    NSPredicate* predi = [NSPredicate predicateWithFormat:@"childId=%@ AND agencyId=%@",_activeChild.objectID.URIRepresentation.absoluteString,_agency.nid];
    NSError* error;
    [fetchRequest setPredicate:predi];
    NSArray *fetchedObjects = [[[AppManager sharedManager] appDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0) {
        [_programStatusesData addObjectsFromArray:fetchedObjects];
    }
    while (_programStatusesData.count<_agency.wwaa.count) {
        ProgramStatusPath* pt = [NSEntityDescription insertNewObjectForEntityForName:@"ProgramStatusPath" inManagedObjectContext:[[[AppManager sharedManager] appDelegate] managedObjectContext]];
        pt.programInd=[NSNumber numberWithInt:-1];
        pt.stageInd=[NSNumber numberWithInt:-1];
        pt.statusInd=[NSNumber numberWithInt:-1];
        pt.childId=_activeChild.objectID.URIRepresentation.absoluteString;
        pt.agencyId=_agency.nid;
        [_programStatusesData addObject:pt];
    }
    for (int i=0; i<_programStatusesData.count; i++) {
        ProgramStatusPath* path = [_programStatusesData objectAtIndex:i];
        path.programInd=[NSNumber numberWithInt:i];
    }
    
    [[[[AppManager sharedManager] appDelegate] managedObjectContext]save:NULL];
}

#pragma mark - TableViewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProgramCell* cell = nil;
    ProgramStatusPath* path = [_programStatusesData objectAtIndex:indexPath.row];
    WWAA* sec = [_agency.wwaa objectAtIndex:indexPath.row];
    if (_expandedIndex==indexPath.row) {
        ExpandedProgramCell* expCell = [tableView dequeueReusableCellWithIdentifier:@"ExpandedProgramCell"];
        [AppManager layoutCellIfNeeded:expCell tableView:tableView];
        expCell.checkBoxes= sec.status;
        expCell.delegate=self;
        expCell.path=path;
        [expCell reloadData];
        cell=expCell;
        
    }else {
        cell=[tableView dequeueReusableCellWithIdentifier:@"ProgramCell"];
        [AppManager layoutCellIfNeeded:cell tableView:tableView];
        cell.path=path;
    }
    cell.indexPath=indexPath;
    
    cell.title.text=[sec.title.markup uppercaseString];
    
    
    
    
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _agency.wwaa.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==_expandedIndex) {
        ExpandedProgramCell* cel = (ExpandedProgramCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return [cel cellHeight];
    }
    return 60;
}
-(IBAction)back:(id)sender{
    [[[[AppManager sharedManager] appDelegate] managedObjectContext]reset];
    [[AppManager sharedManager]populateDB];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.row==_expandedIndex) {
        _expandedIndex=-1;
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }else{
        int previous=_expandedIndex;
        _expandedIndex=(int)indexPath.row;
        if (previous>-1) {
            [tableView reloadRowsAtIndexPaths:@[indexPath,[NSIndexPath indexPathForRow:previous inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }else{
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
    [self setViewHeightAnimated:YES];
    
    //-------------take scrollview at top------------------//
    [scrollView setContentOffset:CGPointMake(0,0) animated:NO];
    //-------------take scrollview at top------------------//
}


- (IBAction)saveAction:(id)sender {
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatSettingStatus
                                                                                        action:[NSString stringWithFormat:@"%@-%@",kActSaveStatus,_agency.mainDetails.title]
                                                                                         label:nil
                                                                                         value:nil] build]];
    for (int i=0;i<_programStatusesData.count;i++) {
        ProgramStatusPath* path = [_programStatusesData objectAtIndex:i];
        if (path.hasChanges==YES) {
            if ([path.stageInd integerValue] >= 0) {
                
                if ([path.programInd integerValue]<0) {
                    
                }else{
                    StatusHistoryObj* obj = nil;
                    WWAA* checkBoxes = [_checkBoxesData objectAtIndex:i];
                    
                    NSFetchRequest* request = [[NSFetchRequest alloc]init];
                    NSEntityDescription* e = [NSEntityDescription entityForName:@"StatusHistoryObj" inManagedObjectContext:[[[AppManager sharedManager] appDelegate] managedObjectContext]];
                    [request setEntity:e];
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"agencyId=%@ AND childId=%@ AND programInd=%li",_agency.nid,_activeChild.objectID.URIRepresentation.absoluteString,[path.programInd integerValue]];
                    [request setPredicate:predicate];
                    NSArray* fetchedObjects = [[[[AppManager sharedManager] appDelegate] managedObjectContext] executeFetchRequest:request error:NULL];
                    
                    for (StatusHistoryObj* h in fetchedObjects) {
                        if ([h.statusInd integerValue]==[path.statusInd integerValue] && [h.programInd integerValue]==[path.programInd integerValue]) {
                            obj=h;
                            break;
                        }
                    }
                    if (obj==nil) {
                        obj = [NSEntityDescription insertNewObjectForEntityForName:@"StatusHistoryObj" inManagedObjectContext:[[[AppManager sharedManager] appDelegate] managedObjectContext]];
                    }
                    obj.title=[[checkBoxes.status objectAtIndex:[path.statusInd intValue]] markup];
                    obj.programInd=path.programInd;
                    obj.statusInd=path.statusInd;
                    obj.stageInd=path.stageInd;
                    obj.date=[NSDate date];
                    obj.childId=_activeChild.objectID.URIRepresentation.absoluteString;
                    obj.agencyId=_agency.nid;
                }
                
            }
            
        }
    }
    
    [[[[AppManager sharedManager]appDelegate]managedObjectContext]save:NULL];
    
    _WWAAMSHVC.shouldGoBackIfNoStatusSet=YES;
    if (![self isAnyStatusSet]) {
        MenuRootViewController* mrvc = [[[[AppManager sharedManager]tabsController]viewControllers] objectAtIndex:0];
        UINavigationController* nc=(UINavigationController*)mrvc.contentViewController;
        [nc setViewControllers:@[[nc.viewControllers objectAtIndex:0],[nc.viewControllers objectAtIndex:1]] animated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark - statusupdatedCheckbox
-(void)updatedWithProgramIndex:(int)programIndex statusIndex:(int)statusIndex completionStatusIndex:(int)completionStatusIndex{
    ProgramStatusPath* p = [_programStatusesData objectAtIndex:programIndex];
    p.statusInd=[NSNumber numberWithInt:statusIndex];
    p.stageInd=[NSNumber numberWithInt:completionStatusIndex];
    p.childId=_activeChild.objectID.URIRepresentation.absoluteString;
    
        
}
-(void)tableHeightChangedTo:(int)height indexPath:(NSIndexPath *)indexPath{
    [tableVu reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
    [self setViewHeightAnimated:NO];
}
#pragma mark - Actions
- (IBAction)connectAction:(id)sender {
    if (_agency.connects.count>0) {
        AgencyWebsite* st = [_agency.connects objectAtIndex:0];
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                            action:[NSString stringWithFormat:@"%@ -Contact Block Footer- %@",_agency.mainDetails.title,st.title]
                                                                                             label:_agency.mainDetails.title
                                                                                             value:nil] build]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:st.url]];
    }
}
- (IBAction)websiteAction:(id)sender {
    if (_agency.websites.count>0) {
        AgencyWebsite* st = [_agency.websites objectAtIndex:0];
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                            action:[NSString stringWithFormat:@"%@ -Contact Block Footer- %@",_agency.mainDetails.title,st.title]
                                                                                             label:_agency.mainDetails.title
                                                                                             value:nil] build]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:st.url]];
    }
}
- (IBAction)phoneAction:(id)sender {
    if (_agency.agencyPhone) {
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                            action:[NSString stringWithFormat:@"%@ -Contact Block Footer- %@",_agency.mainDetails.title,kActAgencyPhonePressed]
                                                                                             label:_agency.mainDetails.title
                                                                                             value:nil] build]];
        NSString *phn=[_agency.agencyPhone.number stringByReplacingOccurrencesOfString:@"-" withString:@""];
        if (phn!=nil &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@%@",phn,_agency.agencyPhone.extension]]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@%@",phn,_agency.agencyPhone.extension]]];
        }
    }
}

@end
