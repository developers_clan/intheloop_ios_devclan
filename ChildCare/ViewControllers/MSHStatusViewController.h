//
//  MSHStatusViewController.h
//  ChildCare
//
//  Created by Devclan on 30/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//
#import "Child.h"
#import <UIKit/UIKit.h>
#import "Agency.h"
#import "ProgramStatusPath.h"
#import "WWAAMSHViewController.h"
@interface MSHStatusViewController : UIViewController
@property (strong,nonatomic)Agency* agency;
@property (strong,nonatomic)WWAAMSHViewController* WWAAMSHVC;
@property (strong,nonatomic)NSMutableArray* checkBoxesData;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *totalHeight;
@property (strong,nonatomic) Child* activeChild;
@property int expandedIndex;
@property (strong,nonatomic) NSMutableArray* programStatusesData;
@end
