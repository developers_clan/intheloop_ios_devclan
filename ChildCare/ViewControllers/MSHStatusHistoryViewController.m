//
//  MSHStatusHistoryViewController.m
//  ChildCare
//
//  Created by Devclan on 30/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//
#import "AppManager.h"
#import "MSHStatusHistoryViewController.h"
#import "HistoryCell.h"
#import "StatusHistoryObj.h"
#import "AgencyHomeSection.h"
#import "UIImage+Additions(Additions).h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AgencyWebsite.h"
#import "ChildSelector.h"
#import "MenuRootViewController.h"
#import <Google/Analytics.h>
#import <Google/Analytics.h>


@interface MSHStatusHistoryViewController ()<UITableViewDelegate,UITableViewDataSource,IGLDropDownMenuDelegate,HistoryCellInteractionDelegate>

@end

@implementation MSHStatusHistoryViewController{
    
    __weak IBOutlet UIImageView *connectIcon;
    __weak IBOutlet UIImageView *webIcon;
    __weak IBOutlet UIImageView *telIcon;
    __weak IBOutlet UIView *footerStrip;
    __weak IBOutlet UIView *headerStip;
    __weak IBOutlet NSLayoutConstraint *totalHeight;
    __weak IBOutlet UITableView *tableVu;
    __weak IBOutlet UIView *dropdownVu;
    int programInd;
    NSArray* history;
    NSDateFormatter* dateFormatter;
    NSDateFormatter* timeFormatter;
    __weak IBOutlet UILabel *name;
    __weak IBOutlet UIImageView *childImage;
    __weak IBOutlet UIImageView *agencyLogo;
    __weak IBOutlet UIButton *connectBtn;
    __weak IBOutlet UIButton *websiteBtn;
    __weak IBOutlet UIButton *phoneBtn;
    CGFloat titleWidth;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    headerStip.backgroundColor = _agency.agencyColor;
    footerStrip.backgroundColor = _agency.agencyColor;
    
    
    //---------------googleAnalytics-----------//
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"View History Screen-%@",_agency.mainDetails.title]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    //---------------googleAnalytics-----------//
    
    
//    if (history.count>0) {
//        programInd=0;
//    }else{
        programInd=-1;
//    }
    tableVu.layer.borderColor=[UIColor clearColor].CGColor;
    [self reloadData];
    
}
-(void)reloadData{
    history=[NSArray new];
    _activeChild=[[AppManager sharedManager] activeChild];
    if (_activeChild==nil) {
        return;
    }
    
    if ([_activeChild.gender integerValue]==0) {
        childImage.image = [UIImage imageNamed:@"where_are_banner_boy_user_placeholder"];
    }else{
        childImage.image = [UIImage imageNamed:@"where_are_banner_girl_user_placeholder"];
    }
    name.text = _activeChild.name;
//    [agencyLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",API_BASE_ADDRESS,API_FILES,_agency.logo.fileName]]
//                  placeholderImage:[UIImage new]];
    [agencyLogo sd_setImageWithURL:[NSURL URLWithString:_agency.logo.uri]
                  placeholderImage:[UIImage new]];
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* e = [NSEntityDescription entityForName:@"StatusHistoryObj" inManagedObjectContext:[[[AppManager sharedManager] appDelegate] managedObjectContext]];
    [request setEntity:e];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"agencyId=%@ AND childId=%@ AND programInd=%li",_agency.nid,_activeChild.objectID.URIRepresentation.absoluteString,(long)programInd];
    [request setPredicate:predicate];
        NSArray* fetchedObjects = [[[[AppManager sharedManager] appDelegate] managedObjectContext] executeFetchRequest:request error:NULL];
    if (fetchedObjects.count>0) {
        history = [fetchedObjects sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSDate *first = [(StatusHistoryObj*)a date];
            NSDate *second = [(StatusHistoryObj*)b date];
            return [first compare:second];
        }];
    }
    // Do any additional setup after loading the view.
    
    [self.view layoutIfNeeded];
    if (history.count>0) {
        tableVu.backgroundColor=[UIColor colorWithWhite:247/255.0 alpha:1];
        tableVu.layer.borderColor = [UIColor colorWithWhite:230/255.0 alpha:1].CGColor;
    }else{
        tableVu.backgroundColor=[UIColor clearColor];
        tableVu.layer.borderColor = [UIColor clearColor].CGColor;
    }
    [tableVu reloadData];
    
    
    totalHeight.constant= 552+tableVu.contentSize.height;
    _tableHeightConst.constant = tableVu.contentSize.height;
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.dropdown setFrame:dropdownVu.frame];
    [self.dropdown reloadView];
    if (programInd>-1) {
        [self.dropdown selectItemAtIndex:programInd];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HistoryCell* cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
    
    [AppManager layoutCellIfNeeded:cell tableView:tableView];
    StatusHistoryObj* obj = [history objectAtIndex:indexPath.row];
    cell.title.text = obj.title;
    cell.historyDelegate=self;
    cell.histObj=obj;
    cell.date.text=[dateFormatter stringFromDate:obj.date];
    switch ([obj.stageInd intValue]) {
        case 0:
            cell.stage.text=@"Waiting";
            cell.stage.textColor=[[AppManager sharedManager] greeColor];
            break;
        case 1:
            cell.stage.text=@"Accessing";
            cell.stage.textColor=[[AppManager sharedManager] yellowColor];
            break;
        case 2:
            cell.stage.text=@"Completed";
            cell.stage.textColor=[[AppManager sharedManager] appThemeColor];
            break;
        default:
            cell.stage.text=@"";
            cell.stage.textColor=[UIColor clearColor];
            break;
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    StatusHistoryObj* obj = [history objectAtIndex:indexPath.row];
    CGRect fr = [AppManager frameForTextInConstraint:CGSizeMake(titleWidth, CGFLOAT_MAX) string:obj.title fontName:@"Lato-Bold" fontSize:18];
    return 69+fr.size.height;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return history.count;
}
-(void)showChildSelector{
    ChildSelector* selectVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChildSelector"];
    [self presentViewController:selectVC animated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    tableVu.bounds=CGRectMake(0, 0, self.view.frame.size.width-16, 10);
    HistoryCell* cell = [tableVu dequeueReusableCellWithIdentifier:@"HistoryCell"];
    [AppManager layoutCellIfNeeded:cell tableView:tableVu];
    
    titleWidth=cell.title.frame.size.width;
    [childImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showChildSelector)]];
    [name addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showChildSelector)]];
    [super viewDidLoad];

    if (_agency.connects.count>0) {
        AgencyWebsite* st = [_agency.connects objectAtIndex:0];
        [connectBtn setTitle:st.title forState:UIControlStateNormal];
        if ([st.title isEqualToString:@"Events"]) {
            connectIcon.image=[UIImage imageNamed:@"agency_events_bg.png"];
        }else{
            connectIcon.image=[UIImage imageNamed:@"agency_cdp_connect_bg.png"];
        }
    }else{
        connectBtn.hidden=YES;
        connectIcon.hidden=YES;
    }
    if (_agency.websites.count>0) {
        AgencyWebsite* st = [_agency.websites objectAtIndex:0];
        [websiteBtn setTitle:st.title forState:UIControlStateNormal];
    }else{
        webIcon.hidden=YES;
        websiteBtn.hidden=YES;
    }
    if (_agency.agencyPhone) {
        [phoneBtn setTitle:_agency.agencyPhone.number_alias forState:UIControlStateNormal];
    }else{
        phoneBtn.hidden=YES;
        telIcon.hidden=YES;
    }
    history=[NSArray new];
    self.title = _agency.WWAAMainDetails.title;
    programInd=-1;
    dateFormatter=[[NSDateFormatter alloc]init];
    timeFormatter=[[NSDateFormatter alloc]init];
    dateFormatter.dateFormat=@"MMM dd, yyyy";
    timeFormatter.dateFormat=@"hh:mm a";
    
    tableVu.layer.borderWidth=1;
    
    self.dropdown = [[IGLDropDownMenu alloc] init];
    
    [self.dropdown setFrame:dropdownVu.bounds];
    self.dropdown.delegate=self;
    self.dropdown.menuText = @"Select a program";
    self.dropdown.paddingLeft = 8;
    self.dropdown.layer.shadowColor=[UIColor clearColor].CGColor;
    
    self.dropdown.type = IGLDropDownMenuTypeNormal;
    self.dropdown.dropdownIconImage = [[UIImage imageNamed:@"dropdown"] imageScaledToSize:CGSizeMake(20, 11)];
    NSMutableArray* programs = [NSMutableArray new];
    for (WWAA* sec in _agency.wwaa) {
        [programs addObject:sec.title.markup];
    }
    [self setItems:programs ToMenu:self.dropdown];
    self.dropdown.menuButton.bgView.backgroundColor=[UIColor colorWithWhite:247/255.0 alpha:1];
    self.dropdown.menuButton.bgView.layer.borderColor = [UIColor colorWithWhite:230/255.0 alpha:1].CGColor;
    self.dropdown.menuButton.bgView.layer.borderWidth=1;
    [dropdownVu.superview addSubview:self.dropdown];
    [self reloadData];
    dropdownVu.hidden = YES;
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back_arrow"] imageScaledToSize:CGSizeMake(20, 20)] style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
    self.navigationItem.leftBarButtonItem=back;
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(goBack:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
}
-(void)goBack:(id)sender{
    _WWAAMSHVC.shouldGoBackIfNoStatusSet=YES;
    [_WWAAMSHVC reloadData];
    if (_WWAAMSHVC.indicesOfSetStatuses.count<1) {
        MenuRootViewController* mrvc = [[[[AppManager sharedManager]tabsController]viewControllers] objectAtIndex:0];
        UINavigationController* nc=(UINavigationController*)mrvc.contentViewController;
        [nc setViewControllers:@[[nc.viewControllers objectAtIndex:0],[nc.viewControllers objectAtIndex:1]] animated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - HistoryInteractionDelegate
-(void)cell:(HistoryCell *)cell selectedDate:(NSDate *)date{
    [self reloadData];
}
#pragma mark - IGLDropdown
- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu selectedItemAtIndex:(NSInteger)index
{
    programInd=(int)index;
    [self reloadData];
    
}
-(void)setItems:(NSArray*)items ToMenu:(IGLDropDownMenu*)menu{
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < items.count; i++) {
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        item.paddingRight=16;
        item.bgView.backgroundColor=[UIColor colorWithWhite:246/255.0 alpha:1];
        item.borderColor= [UIColor colorWithWhite:220/255.0 alpha:1];
//        item.iconImage = [[UIImage imageNamed:@"dropdown"] imageScaledToSize:CGSizeMake(20, 11)];
        [item setText:items[i]];
        item.bgView.backgroundColor=[UIColor colorWithWhite:247/255.0 alpha:1];
        [dropdownItems addObject:item];
    }
    menu.dropDownItems=dropdownItems;
    [menu reloadView];
    [menu foldView];
}
#pragma mark - Actions
- (IBAction)connectAction:(id)sender {
    if (_agency.connects.count>0) {
        AgencyWebsite* st = [_agency.connects objectAtIndex:0];
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                            action:[NSString stringWithFormat:@"%@ -Contact Block Footer- %@",_agency.mainDetails.title,st.title]
                                                                                             label:_agency.mainDetails.title
                                                                                             value:nil] build]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:st.url]];
    }
}
- (IBAction)websiteAction:(id)sender {
    if (_agency.websites.count>0) {
        AgencyWebsite* st = [_agency.websites objectAtIndex:0];
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                            action:[NSString stringWithFormat:@"%@ -Contact Block Footer- %@",_agency.mainDetails.title,st.title]
                                                                                             label:_agency.mainDetails.title
                                                                                             value:nil] build]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:st.url]];
    }
}
- (IBAction)phoneAction:(id)sender {
    if (_agency.agencyPhone) {
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                            action:[NSString stringWithFormat:@"%@ -Contact Block Footer- %@",_agency.mainDetails.title,kActAgencyPhonePressed]
                                                                                             label:_agency.mainDetails.title
                                                                                             value:nil] build]];
        NSString *phn=[_agency.agencyPhone.number stringByReplacingOccurrencesOfString:@"-" withString:@""];
        if (phn!=nil &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@%@",phn,_agency.agencyPhone.extension]]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@%@",phn,_agency.agencyPhone.extension]]];
        }
    }
}
@end
