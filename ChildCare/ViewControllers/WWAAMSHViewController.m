//
//  WWAAMSHViewController.m
//  ChildCare
//
//  Created by Devclan on 30/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "WWAAMSHViewController.h"
#import "MHSCurStatusCell.h"
#import "MSHStatusHistoryViewController.h"
#import "MSHStatusViewController.h"
#import "Child.h"
#import "ProgramStatusPath.h"
#import "AgencyHomeSection.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AgencyWebsite.h"
#import "ChildSelector.h"
#import "UIImage+Additions(Additions).h"
#import <Google/Analytics.h>



@interface WWAAMSHViewController ()<UITableViewDataSource,UITableViewDelegate,MSHCellInteractionCallBackDelegate,ChildSelectionCompletionDelegate>

@end

@implementation WWAAMSHViewController{
    __weak IBOutlet UIView *footerStrip;
    __weak IBOutlet NSLayoutConstraint *tableVuHeightConstraint;
    __weak IBOutlet UIImageView *telIcon;
    __weak IBOutlet UIView *headerStrip;
    __weak IBOutlet UIButton *connectBtn;
    __weak IBOutlet UIButton *websiteBtn;
    __weak IBOutlet UIButton *phoneBtn;
    __weak IBOutlet UIImageView *agencyLogo;
    __weak IBOutlet UIImageView *childImage;
    __weak IBOutlet UIImageView *webIcon;
    __weak IBOutlet UILabel *name;
    __weak IBOutlet UIImageView *connectIcon;
    __weak IBOutlet NSLayoutConstraint *totalHeight;
    __weak IBOutlet UIButton *historyBtn;
    __weak IBOutlet UITableView *tableView;
    Child* activeChild;
}

-(void)viewController:(UIViewController *)viewController completedSelectionWithChild:(Child *)child{
    _shouldGoBackIfNoStatusSet=NO;
    [self reloadData];
    [self moveToOtherViewControllerIfNeeded];
}
-(void)showChildSelector{
    ChildSelector* selectVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChildSelector"];
    selectVC.completionDelegate=self;
    [self presentViewController:selectVC animated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _shouldGoBackIfNoStatusSet=NO;
    _indicesOfSetStatuses = [NSMutableArray new];
    [childImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showChildSelector)]];
    [name addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showChildSelector)]];
    self.title = _agency.WWAAMainDetails.title;
    _checkBoxesData=[NSMutableArray new];
    historyBtn.layer.cornerRadius=historyBtn.frame.size.height/2.0;
    _programStatusesData = [NSMutableArray new];
    headerStrip.backgroundColor = _agency.agencyColor;
    footerStrip.backgroundColor = _agency.agencyColor;
    if (_agency.connects.count>0) {
        AgencyWebsite* st = [_agency.connects objectAtIndex:0];
        if ([st.title isEqualToString:@"Events"]) {
            connectIcon.image=[UIImage imageNamed:@"agency_events_bg.png"];
        }else{
            connectIcon.image=[UIImage imageNamed:@"agency_cdp_connect_bg.png"];
        }
        [connectBtn setTitle:st.title forState:UIControlStateNormal];
    }else{
        connectBtn.hidden=YES;
        connectIcon.hidden=YES;
    }
    if (_agency.websites.count>0) {
        AgencyWebsite* st = [_agency.websites objectAtIndex:0];
        [websiteBtn setTitle:st.title forState:UIControlStateNormal];
    }else{
        webIcon.hidden=YES;
        websiteBtn.hidden=YES;
    }
    if (_agency.agencyPhone) {
        [phoneBtn setTitle:_agency.agencyPhone.number_alias forState:UIControlStateNormal];
    }else{
        phoneBtn.hidden=YES;
        telIcon.hidden=YES;
    }
//    tableView.layer.borderColor = [[AppManager sharedManager] placeholderColor].CGColor;
//    tableView.layer.borderWidth=1;
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back_arrow"] imageScaledToSize:CGSizeMake(20, 20)] style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
    self.navigationItem.leftBarButtonItem=back;
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(goBack:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
}
-(void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)populateCheckBoxesData{
    
    _checkBoxesData = _agency.wwaa;
}

-(void)reloadData{
    if (_indicesOfSetStatuses==nil) {
        _indicesOfSetStatuses = [NSMutableArray new];
    }
    activeChild=[[AppManager sharedManager]activeChild];
    if (activeChild==nil) {
        [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Please set active child from account" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        return;
    }
    if ([activeChild.gender integerValue]==0) {
        childImage.image = [UIImage imageNamed:@"where_are_banner_boy_user_placeholder"];
    }else{
        childImage.image = [UIImage imageNamed:@"where_are_banner_girl_user_placeholder"];
    }
    [agencyLogo sd_setImageWithURL:[NSURL URLWithString:_agency.logo.uri]
                  placeholderImage:[UIImage new]];
    name.text = activeChild.name;
    [self populateCheckBoxesData];
    [self populateStatuses];
    [self populateIndicesOfSetStatuses];
}
-(void)moveToOtherViewControllerIfNeeded{
    if (_indicesOfSetStatuses.count<1) {
        if (_shouldGoBackIfNoStatusSet==YES) {
            [self.navigationController popViewControllerAnimated:NO];
        }else{
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                MSHStatusViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MSHStatusViewController"];
                vc.agency=_agency;
                vc.checkBoxesData=_checkBoxesData;
                vc.expandedIndex=0;
                vc.WWAAMSHVC = self;
                [self.navigationController pushViewController:vc animated:NO];
            }];
            
        }
    }else{
        _shouldGoBackIfNoStatusSet=NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)historyAction:(id)sender {
    
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                        action:[NSString stringWithFormat:@"%@-%@",kActHistory,_agency.mainDetails.title]
                                                                                         label:nil
                                                                                         value:nil] build]];
    
    MSHStatusHistoryViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MSHStatusHistoryViewController"];
    vc.agency=_agency;
    vc.WWAAMSHVC=self;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //---------------googleAnalytics-----------//
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"My Services Listing Screen-%@",_agency.mainDetails.title]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    //---------------googleAnalytics-----------//
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self reloadData];
        [tableView reloadData];
        totalHeight.constant=MAX(tableView.contentSize.height+532,self.view.frame.size.height);
        tableVuHeightConstraint.constant=tableView.contentSize.height;

    }];
}
-(void)populateStatuses{
    if (_programStatusesData==nil) {
        _programStatusesData=[NSMutableArray new];
    }else{
        [_programStatusesData removeAllObjects];        
    }


    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ProgramStatusPath"
                                              inManagedObjectContext:[[AppManager sharedManager] appDelegate].managedObjectContext];
    [fetchRequest setEntity:entity];
    NSPredicate* predi = [NSPredicate predicateWithFormat:@"childId=%@ AND agencyId=%@",activeChild.objectID.URIRepresentation.absoluteString,_agency.nid];
    NSError* error;
    [fetchRequest setPredicate:predi];
    NSArray *fetchedObjects = [[[AppManager sharedManager] appDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0) {
        [_programStatusesData addObjectsFromArray:fetchedObjects];
    }
    while (_programStatusesData.count<_agency.wwaa.count) {
        ProgramStatusPath* pt = [NSEntityDescription insertNewObjectForEntityForName:@"ProgramStatusPath" inManagedObjectContext:[[[AppManager sharedManager] appDelegate] managedObjectContext]];
        pt.stageInd=[NSNumber numberWithInt:-1];
        pt.statusInd=[NSNumber numberWithInt:-1];
        pt.childId=activeChild.objectID.URIRepresentation.absoluteString;
        pt.agencyId=_agency.nid;
        [_programStatusesData addObject:pt];
    }
    for (int i=0; i<_programStatusesData.count; i++) {
        ProgramStatusPath* path = [_programStatusesData objectAtIndex:i];
        path.programInd=[NSNumber numberWithInt:i];
    }
    
    [[[[AppManager sharedManager] appDelegate] managedObjectContext]save:NULL];
}
-(void)populateIndicesOfSetStatuses{
    [_indicesOfSetStatuses removeAllObjects];
    for (int i=0;i<_programStatusesData.count;i++) {
        ProgramStatusPath* pt = [_programStatusesData objectAtIndex:i];
        if ([pt.programInd intValue] > -1 && [pt.stageInd intValue] > -1) {
            [_indicesOfSetStatuses addObject:[NSNumber numberWithInt:i]];
        }
    }
}
#pragma mark - TableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _indicesOfSetStatuses.count;
}

-(CGFloat)tableView:(UITableView *)tableView2 heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    int realInd = [_indicesOfSetStatuses[indexPath.row] intValue];
    WWAA* wwaa = [_agency.wwaa objectAtIndex:realInd];
    ProgramStatusPath* path = [_programStatusesData objectAtIndex:realInd];
    CGFloat height = 64;
    CGSize labelContraints              = CGSizeMake(tableView.frame.size.width-64, 5500);
    NSStringDrawingContext *context     = [[NSStringDrawingContext alloc] init];
    CGRect labelRect=CGRectZero;
    
    
    if ([path.statusInd intValue]<0) {
        labelRect = [@"NO STATUS SET" boundingRectWithSize:labelContraints
                            options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin
                                                attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"Lato-Bold" size:13],
                                                              NSForegroundColorAttributeName : [UIColor redColor]}
                            context:context];
        height += labelRect.size.height;
    }else{
        labelRect = [[[wwaa.status objectAtIndex:[path.statusInd intValue]] markup] boundingRectWithSize:labelContraints
                                                   options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin
                                                                                              attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"Lato-Bold" size:13],
                                                                                                            NSForegroundColorAttributeName : [UIColor redColor]}
                                                   context:context];
        height += labelRect.size.height;
    }
    labelContraints              = CGSizeMake(tableView.frame.size.width-16, 5500);
    labelRect = [[wwaa.title.markup uppercaseString] boundingRectWithSize:labelContraints
                                                                                             options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin
                                                               attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"Lato-Bold" size:14],
                                                                             NSForegroundColorAttributeName : [UIColor redColor]}
                                                                                             context:context];
    height += labelRect.size.height;

    return height;
}
-(UITableViewCell *)tableView:(UITableView *)tableView2 cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MHSCurStatusCell* cell = [tableView2 dequeueReusableCellWithIdentifier:@"MHSCurStatusCell"];
    [AppManager layoutCellIfNeeded:cell tableView:tableView];
//    NSArray* checkBoxData = [checkBoxesData objectAtIndex:indexPath.row];
    int realInd = [_indicesOfSetStatuses[indexPath.row] intValue];
    WWAA* wwaa = [_agency.wwaa objectAtIndex:realInd];
    ProgramStatusPath* path = [_programStatusesData objectAtIndex:realInd];
    
    if ([path.statusInd intValue]<0) {
        cell.statusTitle.text=@"NO STATUS SET";
    }else{
        cell.statusTitle.text=[[wwaa.status objectAtIndex:[path.statusInd intValue]] markup];
    }
    cell.programTitle.text=[wwaa.title.markup uppercaseString];
    cell.delegate=self;
    cell.indexPath=indexPath;
    switch ([path.stageInd intValue]) {
        case 0:
            cell.stage.text=@"Waiting";
            cell.stageBg.backgroundColor=[[AppManager sharedManager] greeColor];
            break;
        case 1:
            cell.stage.text=@"Accessing";
            cell.stageBg.backgroundColor=[[AppManager sharedManager] yellowColor];
            break;
        case 2:
            cell.stage.text=@"Completed";
            cell.stageBg.backgroundColor=[[AppManager sharedManager] appThemeColor];
            break;
        default:
            cell.stage.text=@"";
            cell.stageBg.backgroundColor=[UIColor clearColor];
            break;
    }
    return cell;
}

#pragma mark - cellInteraction
-(void)cell:(MHSCurStatusCell *)cell changeStatusClicked:(NSIndexPath *)indexPath{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        MSHStatusViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MSHStatusViewController"];
        vc.agency=_agency;
        vc.checkBoxesData=_checkBoxesData;
        vc.WWAAMSHVC=self;
        vc.expandedIndex=[_indicesOfSetStatuses[indexPath.row] intValue];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
}
#pragma mark - Actions
- (IBAction)connectAction:(id)sender {
    if (_agency.connects.count>0) {
        AgencyWebsite* st = [_agency.connects objectAtIndex:0];
        
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                            action:[NSString stringWithFormat:@"%@ -Contact Block Footer- %@",_agency.mainDetails.title,st.title]
                                                                                             label:_agency.mainDetails.title
                                                                                             value:nil] build]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:st.url]];
    }
}
- (IBAction)websiteAction:(id)sender {
    if (_agency.websites.count>0) {
        AgencyWebsite* st = [_agency.websites objectAtIndex:0];
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                            action:[NSString stringWithFormat:@"%@ -Contact Block Footer- %@",_agency.mainDetails.title,st.title]
                                                                                             label:_agency.mainDetails.title
                                                                                             value:nil] build]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:st.url]];
    }
}
- (IBAction)phoneAction:(id)sender {
    if (_agency.agencyPhone) {
        NSString *phn=[_agency.agencyPhone.number stringByReplacingOccurrencesOfString:@"-" withString:@""];
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatAgencyContact
                                                                                            action:[NSString stringWithFormat:@"%@ -Contact Block Footer- %@",_agency.mainDetails.title,kActAgencyPhonePressed]
                                                                                             label:_agency.mainDetails.title
                                                                                             value:nil] build]];
        if (phn!=nil &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@%@",phn,_agency.agencyPhone.extension]]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@%@",phn,_agency.agencyPhone.extension]]];
        }
    }
}
@end
