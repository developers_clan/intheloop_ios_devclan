//
//  WWAAMSHViewController.h
//  ChildCare
//
//  Created by Devclan on 30/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Agency.h"
@interface WWAAMSHViewController : UIViewController
@property (nonatomic,strong)Agency* agency;
@property BOOL shouldGoBackIfNoStatusSet;
-(void)reloadData;
@property (nonatomic,strong) NSMutableArray* programStatusesData;
@property (nonatomic,strong) NSMutableArray* checkBoxesData;
@property (nonatomic,strong) NSMutableArray* indicesOfSetStatuses;
@end
