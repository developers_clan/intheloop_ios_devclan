//
//  TeamEditorViewController.m
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "TeamEditorViewController.h"
#import "TeamMemberCell.h"
#import "ExpandedMemberCell.h"
#import "AppManager.h"
#import "Contact.h"
#import "ChildTeam.h"
#import "Constants.h"
#import "AppDelegate.h"
#import <MBProgressHUD.h>
#import <AFNetworking.h>
#import "Team.h"
#import "UIImage+Additions(Additions).h"
#import "TeamPhone.h"
#import "AppDelegate.h"
#import "AddContactViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <JDStatusBarNotification.h>
#import "TeamEmail.h"
#import "TeamDiscipline.h"
#import "TeamName.h"
#import "TeamAPIImage+CoreDataProperties.h"
#import "ChildSelector.h"
#import "UIView+Genie.h"
#import <Google/Analytics.h>




@interface TeamEditorViewController ()<CellInteractionCallBackDelegate,UIAlertViewDelegate, UITextFieldDelegate,UIImagePickerControllerDelegate,ChildSelectionCompletionDelegate,UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIButton *viewTeamBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *srchBtnHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *clearBtnHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;

@property (weak, nonatomic) IBOutlet UIButton *addContactBtn;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLbl;
@property (weak, nonatomic) IBOutlet UIButton *clearBtn;

@end

@implementation TeamEditorViewController{
    NSMutableArray* expansionFlags;
    NSMutableSet* teamImagesDbHitIds;
    NSMutableArray *search;
    BOOL isSearch;
    NSIndexPath *pickingImageIndexPath;
    AppDelegate* appDelegate;
    NSMutableArray* teamsAPI;
    NSMutableArray* contacts;
    __weak IBOutlet UITextField *srchFld;
    float yOffset;
    NSLock *lock;
    BOOL locked;
    NSMutableArray* cachedImages;
    TeamMemberCell* deletingCell;
    CGFloat teamNameLabelWidth;
    CGFloat teamDisciplineLabelWidth;
    UISwipeGestureRecognizer *gestureRecognizer;
}
-(void)showChildSelector{
    ChildSelector* selectVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChildSelector"];
    selectVC.completionDelegate = self;
    [self presentViewController:selectVC animated:YES completion:nil];
}
-(void)viewController:(UIViewController *)viewController completedSelectionWithChild:(Child *)child{
    _child=child;
    _childTeam=[[AppManager sharedManager] teamForChild:_child];
    [self switchVCifNeeded];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_img addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showChildSelector)]];
    [_name addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showChildSelector)]];
    gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(goBack:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
//    [_name addGestureRecognizer:t];
//    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
//        UIImage* img = [self blur:_headerBgImg.image];
//        dispatch_async(dispatch_get_main_queue(), ^(void){
//            _headerBgImg.image=img;
//        });
//    });
//    
//    
//    UIView* vu = [[UIView alloc] initWithFrame:_headerBgImg.frame];
//    vu.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
//    [_headerBgImg addSubview:vu];
    cachedImages = [NSMutableArray new];
    teamImagesDbHitIds = [NSMutableSet new];
    _titleView=[[[NSBundle mainBundle] loadNibNamed:@"TeamTitle" owner:self options:nil] objectAtIndex:0];
    lock = [NSLock new];
    locked = NO;
    yOffset=0;
    appDelegate = [[UIApplication sharedApplication] delegate];
    [_addContactBtn setImage:[[UIImage imageNamed:@"my_team_add_friend_icon"] imageScaledToSize:CGSizeMake(20, 15)] forState:UIControlStateNormal];
    _viewTeamBtn.layer.cornerRadius=_viewTeamBtn.frame.size.height/2.0;
    expansionFlags=[NSMutableArray new];
    srchFld.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search Name of Professional" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    teamsAPI=[NSMutableArray new];
    contacts=[NSMutableArray new];
    self.navigationItem.titleView=_titleView;
    _countBg.layer.cornerRadius=7;
    _countBg.clipsToBounds=YES;
    _teamMemberCount.text=@"0";
    
    _tablwVu.bounds=CGRectMake(0, 0, self.view.frame.size.width-32, 10);
    TeamMemberCell* cell = [_tablwVu dequeueReusableCellWithIdentifier:@"TeamMemberCell"];
    [AppManager layoutCellIfNeeded:cell tableView:_tablwVu];
    teamNameLabelWidth=cell.name.frame.size.width;
    teamDisciplineLabelWidth=cell.discipline.frame.size.width;
}

- (UIImage*) blur:(UIImage*)theImage
{
    // ***********If you need re-orienting (e.g. trying to blur a photo taken from the device camera front facing camera in portrait mode)
    // theImage = [self reOrientIfNeeded:theImage];
    
    // create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:theImage.CGImage];
    
    // setting up Gaussian Blur (we could use one of many filters offered by Core Image)
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:15.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    // CIGaussianBlur has a tendency to shrink the image a little,
    // this ensures it matches up exactly to the bounds of our original image
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *returnImage = [UIImage imageWithCGImage:cgImage];//create a UIImage for this function to "return" so that ARC can manage the memory of the blur... ARC can't manage CGImageRefs so we need to release it before this function "returns" and ends.
    CGImageRelease(cgImage);//release CGImageRef because ARC doesn't manage this on its own.
    
    return returnImage;
    
    // *************** if you need scaling
    // return [[self class] scaleIfNeeded:cgImage];
}


-(void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self adjustSearchBtnSize];
}
- (void)viewWillAppear:(BOOL)animated{
    

    
    
    [super viewWillAppear:animated];
    Child* activeChild = [[AppManager sharedManager] activeChild];
    if (activeChild!=nil) {
        _child=activeChild;
    }
    _childTeam=[[AppManager sharedManager] teamForChild:_child];
    _name.text =  _child.name;
    if ([_child.gender integerValue]==0) {
        _img.image = [UIImage imageNamed:@"my_team_user_boy_place_holder"];
    }else{
        _img.image = [UIImage imageNamed:@"my_team_user_girl_place_holder"];
    }
    isSearch = NO;
    
    if (_isEditor) {
        //---------------googleAnalytics-----------//
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"Team Contacts listing Screen"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        //---------------googleAnalytics-----------//
        srchFld.hidden=NO;
        _srchBtn.hidden=NO;
        _srchUnderline.hidden=NO;
    }else{
        //---------------googleAnalytics-----------//
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"My Team Listing Screen"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        //---------------googleAnalytics-----------//
        srchFld.hidden=YES;
        _srchBtn.hidden=YES;
        _srchUnderline.hidden=YES;
    }
    _clearBtn.hidden=!_isEditor;
    if ([self.navigationController.viewControllers objectAtIndex:0]==self) {
        self.navigationItem.leftBarButtonItem=nil;
        [self.view removeGestureRecognizer:gestureRecognizer];
    }else{
        UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back_arrow"] imageScaledToSize:CGSizeMake(20, 20)] style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
        self.navigationItem.leftBarButtonItem=back;
        
        
        [self.view addGestureRecognizer:gestureRecognizer];
    }
    [self fetchAndLoadTeamsData];
}
-(void)fetchAndLoadTeamsData{
    
    NSString* urlStr =[NSString stringWithFormat:@"%@%@%@",API_BASE_ADDRESS,API_PATH,kGetTeamsAPI];
    if ([[AppManager sharedManager] teamsAPI].count>0) {
            [self reloadTeamsDataLocally:urlStr];
            return;
    }

        MBProgressHUD *p=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        p.labelText=@"Please wait...";
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
        [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [p hide:YES];
            NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:responseObject];
            [[AppManager sharedManager] insertOrUpdateResponse:myData url:urlStr];
            [self reloadTeamsDataLocally:urlStr];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [p hide:YES];
//            [[[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            [self reloadTeamsDataLocally:urlStr];
        }];
}
-(void)populateChildContactsAndTeams{
    teamsAPI=[[AppManager sharedManager] teamsAPIForChildTeam:_childTeam];
    contacts = [[AppManager sharedManager] ContactsForChildTeam:_childTeam];
}
-(void)reloadTeamsDataLocally:(NSString*)key{
    
    JSONResponse* jsonResp=[[AppManager sharedManager] jsonResponse:key];
    if (jsonResp==nil) {
        return;
    }
    NSArray* teamsArray = [NSKeyedUnarchiver unarchiveObjectWithData:jsonResp.value];
    if (teamsArray==nil) {
        return;
    }
    [[[AppManager sharedManager]teamsAPI] removeAllObjects];
    
    for (NSDictionary* opt in teamsArray) {
        Team* a=[[Team alloc] initWithDictionary:opt];
        [[[AppManager sharedManager]teamsAPI] addObject:a];
    }
    
    if (_isEditor) {
        teamsAPI=[[AppManager sharedManager] teamsAPI];
        contacts = [[AppManager sharedManager] contacts];
        _descriptionLbl.text = @"Tap on the plus sign to add a team member";
    }else{
        [self populateChildContactsAndTeams];
        _descriptionLbl.text = @"Tap on the minus sign to remove a team member";
    }
    
    [self updateCount];
//    _heightConst.constant=112+80*[self tableView:nil numberOfRowsInSection:0];
    [self resetExpansionFlags];
    [teamImagesDbHitIds removeAllObjects];
    [_tablwVu reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

-(void)updateCount{
    int count = 0;
    if (_isEditor) {
        count = (int)[[AppManager sharedManager] teamsAPIForChildTeam:_childTeam].count+(int)[[AppManager sharedManager] ContactsForChildTeam:_childTeam].count;
        self.teamMemberCount.text = [NSString stringWithFormat:@"%i",count];
        if (_isEditor==NO) {
            [_viewTeamBtn setTitle:@"ADD MORE TO THE TEAM" forState:UIControlStateNormal];
        }else {
            [_viewTeamBtn setTitle:@"VIEW MY TEAM" forState:UIControlStateNormal];
            if (count<1) {
                _viewTeamBtn.enabled=NO;
            }else{
                _viewTeamBtn.enabled=YES;
            }
        }
    }else{
        count = (int)teamsAPI.count+(int)contacts.count;
        self.teamMemberCount.text = [NSString stringWithFormat:@"%i",count];
        
        if (_isEditor==NO) {
            [_viewTeamBtn setTitle:@"ADD MORE TO THE TEAM" forState:UIControlStateNormal];
        }else{
            if (count<1) {
                _viewTeamBtn.enabled=NO;
            }else{
                _viewTeamBtn.enabled=YES;
            }
            [_viewTeamBtn setTitle:@"VIEW MY TEAM" forState:UIControlStateNormal];
        }
    }
    _viewTeamBtn.backgroundColor = _viewTeamBtn.enabled?[[AppManager sharedManager] appThemeColor]:[[AppManager sharedManager] appThemeDisabledColor];
}
-(void)resetExpansionFlags{
    [expansionFlags removeAllObjects];
    for (int i=0;i<(contacts.count+teamsAPI.count);i++) {
        [expansionFlags addObject:[NSNumber numberWithBool:NO]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (isSearch) {
        return [search count];
    }
    else
    {
        return [contacts count] + teamsAPI.count;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (isSearch) {
        if ([[search objectAtIndex:indexPath.row] isKindOfClass:[Contact class]]) {
            return;
        }
    }
    else
    {
        if(indexPath.row < [contacts count])
        {
            return;
        }
    }
    
    
    BOOL isExpanded = [[expansionFlags objectAtIndex:indexPath.row] boolValue];
    if (isExpanded==YES) {
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                            action:kActTeamContactExpanded
                                                                                             label:nil
                                                                                             value:nil] build]];
    }else{
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                            action:kActTeamContactCollapsed
                                                                                             label:nil
                                                                                             value:nil] build]];
    }
    [expansionFlags setObject:[NSNumber numberWithBool:!isExpanded] atIndexedSubscript:indexPath.row];
    [_tablwVu reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self filterTableView];
    [textField endEditing:YES];
    return YES;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL isExpanded = [[expansionFlags objectAtIndex:indexPath.row] boolValue];
    TeamMemberCell* t=nil;
    if (isSearch) {
        if ([[search objectAtIndex:indexPath.row] isKindOfClass:[Contact class]]) {
            t =  [self populateTeamCellFromContact:[search objectAtIndex:indexPath.row] isExpanded:isExpanded];
        }else{
            Team* team = [search objectAtIndex:indexPath.row];
            t =  [self populateTeamCellFromTeam:team isExpanded:isExpanded];
        }
    }
    else
    {
        if(indexPath.row < [contacts count])
        {
            t =  [self populateTeamCellFromContact:[contacts objectAtIndex:indexPath.row] isExpanded:isExpanded];
        }
        else
        {
            Team* team = [teamsAPI objectAtIndex:(indexPath.row - [contacts count])];
            t =  [self populateTeamCellFromTeam:team isExpanded:isExpanded];
        }
    }
    t.delegate=self;
    t.indexPath=indexPath;
    if (t.discipline.text.length>0) {
        t.discTop.constant=3;
    }else{
        t.discTop.constant=0;
    }
    if (t.agenciesCollVuHghtCost.constant>0) {
        t.agencyColVuTop.constant=8;
    }else{
      t.agencyColVuTop.constant=0;
    }
    
    return t;
}
-(TeamMemberCell*)populateTeamCellFromContact:(Contact*)contact isExpanded:(BOOL)isExpanded{
    TeamMemberCell* cell = nil;
    if (isExpanded) {
        cell = [_tablwVu dequeueReusableCellWithIdentifier:@"ExpandedMemberCell"];
    }else{
        cell = [_tablwVu dequeueReusableCellWithIdentifier:@"TeamMemberCell"];
    }
    [cell addGestureRecognizer:cell.lngPress];
    [AppManager layoutCellIfNeeded:cell tableView:_tablwVu];
    cell.teamAPI=nil;
    if ([AppManager team:_childTeam containsContact:contact]) {
        [cell.addRemoveBtn setImage:[UIImage imageNamed:@"my_team_icon_minus"] forState:UIControlStateNormal];
        cell.addRemoveBtn.tag=1;
    }else{
        [cell.addRemoveBtn setImage:[UIImage imageNamed:@"my_team_icon_plus"] forState:UIControlStateNormal];
        cell.addRemoveBtn.tag=0;
    }
    
    
    
    
    cell.locateWidth.constant=50;
    cell.thrdBtn.hidden=NO;
    cell.thrdBtn.tag=1;
    [cell.thrdBtn setImage:[UIImage imageNamed:@"edit_icon_red"] forState:UIControlStateNormal];
    
    
    cell.locateLeading.constant=contact.email.length<1?0:8;
    cell.callBtn.hidden = contact.phone.length<1;
    cell.emailWidth.constant = contact.email.length<1?0:50;
    cell.emailBtn.hidden = contact.email.length<1;
    
    cell.agenciesCollectionVu.hidden = YES;
    if (contact.agency.length>0) {
        cell.agencyLbl.hidden=NO;
        cell.agencyLbl.text = contact.agency;
        cell.agenciesCollVuHghtCost.constant=10;
    }else{
        cell.agencyLbl.hidden=YES;
        cell.agencyLbl.text=@"";
        cell.agenciesCollVuHghtCost.constant=0;
    }

    [cell.toggleExpansionBtn setHidden:YES];
    [cell.name setText:contact.name];
    [cell.discipline setText:contact.discipline];
    
//    if (contact.extension.length>0) {
//        [cell.contact setText:[NSString stringWithFormat:@"%@%@%@",contact.phone,@" Ext. ",contact.extension]];
//    }else{
//        [cell.contact setText:contact.phone];
//    }
    
//    if (contact.email.length>0) {
//        [cell.email setText:contact.email];
//        cell.emailIcon.hidden=NO;
//        cell.emailUnderLine.hidden=NO;
//    }else{
//        cell.emailIcon.hidden=YES;
//        [cell.email setText:contact.email];
//        cell.emailUnderLine.hidden=YES;
//    }
    
    
    UIImage *imge = [UIImage imageWithData:contact.image];
    if (imge == nil) {
            if ([contact.gender integerValue]==0) {
                [cell.img setImage:[UIImage imageNamed:@"my_team_user_boy_placeholder"]];
            }else{
                [cell.img setImage:[UIImage imageNamed:@"my_team_user_girl_placeholder"]];
            }
    }
    else{
        [cell.img setImage:imge];
    }
    return cell;
}
-(TeamMemberCell*)populateTeamCellFromTeam:(Team*)team isExpanded:(BOOL)isExpanded{
    TeamMemberCell* cell=nil;
    if (isExpanded) {
        ExpandedMemberCell* expCell =[_tablwVu dequeueReusableCellWithIdentifier:@"ExpandedMemberCell"];
        [AppManager layoutCellIfNeeded:cell tableView:_tablwVu];
        expCell.officesIcon.hidden = expCell.officesTable.hidden = team.offices.count == 0;
        expCell.phonesIcon.hidden = expCell.phonesTable.hidden = expCell.telUnderline.hidden = team.phones.count == 0;
        expCell.emailsIcon.hidden = expCell.emailsTable.hidden = expCell.emailUnderline.hidden = team.emails.count == 0;
        expCell.officeHeightConstraint.constant = team.offices.count*50;
        expCell.phonesTableVuHeight.constant = team.phones.count*15;
        expCell.emailsHeightConst.constant = team.emails.count*15;
        [expCell reloadData];
        
        cell = expCell;
        //populate other expanded data
    }else{
        cell =[_tablwVu dequeueReusableCellWithIdentifier:@"TeamMemberCell"];
        [AppManager layoutCellIfNeeded:cell tableView:_tablwVu];
    }
    [cell removeGestureRecognizer:cell.lngPress];
    cell.agenciesCollectionVu.hidden = team.agencyThumbnails.count==0;
    cell.agenciesCollVuHghtCost.constant = team.agencyThumbnails.count==0?0:40;
    cell.agenciesCollVuWdt.constant = MIN(team.agencyThumbnails.count*50,teamDisciplineLabelWidth);
    cell.teamAPI=team;
    if (team.names.count>0) {
        [cell.name setText:((TeamName*)[team.names objectAtIndex:0]).value];
    }else{
        [cell.name setText:@"Name N/A"];
    }
    if (team.disciplines.count>0) {
        [cell.discipline setText:((TeamDiscipline*)[team.disciplines objectAtIndex:0]).value];
    }else{
        [cell.discipline setText:@"Discipline N/A"];
    }
    cell.agencyLbl.hidden=YES;
    cell.thrdBtn.tag=0;
    [cell.thrdBtn setImage:[UIImage imageNamed:@"location_icon_red"] forState:UIControlStateNormal];
    cell.locateWidth.constant  = team.offices.count == 0?0:50;
    cell.thrdBtn.hidden = team.offices.count == 0;
    cell.locateLeading.constant  = team.emails.count == 0?0:8;
    cell.callBtn.hidden = team.phones.count == 0;
    cell.emailWidth.constant = team.emails.count == 0?0:50;
    cell.emailBtn.hidden = team.emails.count == 0;
    [cell layoutIfNeeded];
    TeamAPIImage* img;
    if ([teamImagesDbHitIds containsObject:team.nid]) {
        img = [self teamAPIImageForId:team.nid onlyCache:YES];
    }else{
        img = [self teamAPIImageForId:team.nid onlyCache:NO];
    }
    if (img!=nil) {
        cell.img.image=[UIImage imageWithData:img.data];
    }else if (team.headshots.count>0) {
        Headshot* h = [team.headshots objectAtIndex:0];
        [cell.img sd_setImageWithURL:[NSURL URLWithString:h.uri]
                    placeholderImage:[UIImage new]];
    }else{
        if (team.genders.count>0) {
            NSString* g = [team.genders objectAtIndex:0];
            if ([g isEqualToString:@"Male"]) {
                [cell.img setImage:[UIImage imageNamed:@"my_team_user_boy_placeholder"]];
            }else{
                [cell.img setImage:[UIImage imageNamed:@"my_team_user_girl_placeholder"]];
            }
        }
        
        
    }
    
    if ([AppManager team:_childTeam containsTeamAPI:team]) {
        [cell.addRemoveBtn setImage:[UIImage imageNamed:@"my_team_icon_minus"] forState:UIControlStateNormal];
        cell.addRemoveBtn.tag=1;
    }else{
        [cell.addRemoveBtn setImage:[UIImage imageNamed:@"my_team_icon_plus"] forState:UIControlStateNormal];
        cell.addRemoveBtn.tag=0;
    }
    if (team.offices.count+team.agencyThumbnails.count+team.phones.count>0) {
        cell.toggleExpansionBtn.hidden = NO;
    }
    else{
        cell.toggleExpansionBtn.hidden = YES;
    }
    

    [cell reloadData];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    id obj = nil;
    if (isSearch) {
        if ([[search objectAtIndex:indexPath.row] isKindOfClass:[Contact class]]) {
            obj = [search objectAtIndex:indexPath.row];
        }else{
            obj = [search objectAtIndex:indexPath.row];
        }
    }else{
        if (indexPath.row < [contacts count]) {
            obj = [contacts objectAtIndex:indexPath.row];
        }else{
            obj = [teamsAPI objectAtIndex:indexPath.row-contacts.count];
        }
    }

    NSString *nameStr = @"";
    NSString* discipStr = @"";
    
    if ([obj isKindOfClass:[Contact class]]) {
        Contact* c = (Contact*)obj;
        nameStr = c.name;
        discipStr = c.discipline;
    }else if([obj isKindOfClass:[Team class]]){
        Team* c = (Team*)obj;
        nameStr = c.title;
        TeamDiscipline* td = c.disciplines.firstObject;
        if (td!=nil) {
            discipStr = td.value;
        }
    }
    BOOL isExpanded = [[expansionFlags objectAtIndex:indexPath.row] boolValue];
    
    
    CGFloat nameHt = [AppManager frameForTextInConstraint:CGSizeMake(teamNameLabelWidth, CGFLOAT_MAX) string:nameStr fontName:@"Lato-Bold" fontSize:18.0].size.height;
    CGFloat disciHt = [AppManager frameForTextInConstraint:CGSizeMake(teamDisciplineLabelWidth, CGFLOAT_MAX) string:discipStr fontName:@"Lato-Regular" fontSize:12.0].size.height;
    
    if (discipStr>0) {
        disciHt+=3;
    }
    if ([obj isKindOfClass:[Contact class]]) {
        disciHt-=18;
    }else if([obj isKindOfClass:[Team class]]){
//        disciHt+=8;
    }
    
    if (isExpanded==NO) {
        return 154-22-15+nameHt+disciHt;
    }
    ExpandedMemberCell* expCell=(ExpandedMemberCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    return [expCell cellHeight]-22-15+nameHt+disciHt;
}
#pragma mark - search
- (IBAction)editingChanged:(UITextField*)textfield {
    [self filterTableView];
//    if (srchFld.text.length<1) {
//        isSearch = NO;
//        [self resetExpansionFlags];
//        [_tablwVu reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
//    }
//    [self adjustSearchBtnSize];
}
-(void)adjustSearchBtnSize{
    
    if (srchFld.text.length<1) {
        [self.view layoutIfNeeded];
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            _srchBtnHeightConstraint.constant=15;
            _clearBtnHeightConstraint.constant=_srchBtnHeightConstraint.constant;
            [self.view layoutIfNeeded];
        } completion:nil];
    }
    else{
        [self.view layoutIfNeeded];
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            _srchBtnHeightConstraint.constant=21;
            _clearBtnHeightConstraint.constant=_srchBtnHeightConstraint.constant;
            [self.view layoutIfNeeded];
        } completion:nil];
        
    }
}
- (IBAction)searchPressed:(id)sender {
    
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                        action:kActTeamSearch
                                                                                         label:nil
                                                                                         value:nil] build]];
    
    [self filterTableView];
    [_txtSearch endEditing:YES];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (locked) {
        return;
    }
    [lock lock];
    locked=YES;
    if (scrollView.contentOffset.y < yOffset) {
        yOffset = scrollView.contentOffset.y;
        if (self.headerHeightConstraint.constant>=170) {
            [lock unlock];
            locked=NO;
            return;
        }
        [UIView animateWithDuration:ANIMATION_DURATION_SHORT animations:^{
            self.headerHeightConstraint.constant = 170;
            self.headerBgImgHeight.constant = 170;
            if (_isEditor) {
                self.addContactBtn.hidden = NO;
                self.srchFldRightConstraint.constant=8;
            }
            [self.view layoutIfNeeded];
        }];
    }
    else 
    {
        yOffset = scrollView.contentOffset.y;
        int diff = (int)self.tablwVu.contentSize.height-self.tablwVu.frame.size.height;
        if (diff<180 || self.headerHeightConstraint.constant<=48) {
            [lock unlock];
            locked=NO;
            return;
        }
        

        [UIView animateWithDuration:ANIMATION_DURATION_SHORT animations:^{
            self.headerHeightConstraint.constant = 48;
            self.headerBgImgHeight.constant = 50;
            if (_isEditor) {
                self.addContactBtn.hidden = YES;
                self.srchFldRightConstraint.constant=_name.frame.size.width+_img.frame.size.width+25;
            }
            [self.view layoutIfNeeded];
        }];
    }
    [lock unlock];
    locked=NO;
}

-(void)filterTableView
{
    
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                        action:kActTeamSearch
                                                                                         label:nil
                                                                                         value:nil] build]];
    
    [self resetExpansionFlags];
    if (srchFld.text.length<1) {
        isSearch = NO;
        [_tablwVu reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        isSearch = YES;
        search = [NSMutableArray new];
        for (int i=0; i<[[[AppManager sharedManager] contacts] count]; i++) {
            if ([[[[[AppManager sharedManager] contacts] objectAtIndex:i] name] rangeOfString:srchFld.text options:NSCaseInsensitiveSearch].location!=NSNotFound) {
                [search addObject:[[[AppManager sharedManager] contacts] objectAtIndex:i]];
            }
        }
        for (int i=0; i<[[[AppManager sharedManager] teamsAPI] count]; i++) {
            if ([[[[[AppManager sharedManager] teamsAPI] objectAtIndex:i] title] rangeOfString:srchFld.text options:NSCaseInsensitiveSearch].location!=NSNotFound) {
                [search addObject:[[[AppManager sharedManager] teamsAPI] objectAtIndex:i]];
            }
        }
        [_tablwVu reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    [self adjustSearchBtnSize];
}
- (IBAction)addContactPressed:(UIButton *)sender {
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                        action:kActTeamAddContactPressed
                                                                                         label:nil
                                                                                         value:nil] build]];
    AddContactViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddContactViewController"];
    vc.childTeam=_childTeam;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - CellInteraction
-(void)cell:(TeamMemberCell *)cell addRemoveButtonPressedWithIndexPath:(NSIndexPath *)indexPath{
    [_tablwVu selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    if (locked==YES) {
        return;
    }
    if (cell.addRemoveBtn.tag==0) {
        [self localCell:cell addRemoveButtonPressedWithIndexPath:indexPath];
    }else{
//        UIAlertController * alert=   [UIAlertController
//                                      alertControllerWithTitle:@"Warning!"
//                                      message:[NSString stringWithFormat:@"Are you sure to remove?"]
//                                      preferredStyle:UIAlertControllerStyleActionSheet];
//        UIAlertAction* ok = [UIAlertAction
//                             actionWithTitle:@"OK"
//                             style:UIAlertActionStyleDestructive
//                             handler:^(UIAlertAction * action)
//                             {
//                                 [alert dismissViewControllerAnimated:YES completion:nil];
//                                 [self localCell:cell addRemoveButtonPressedWithIndexPath:indexPath];
//                             }];
//        UIAlertAction* cancel = [UIAlertAction
//                                 actionWithTitle:@"Cancel"
//                                 style:UIAlertActionStyleDefault
//                                 handler:^(UIAlertAction * action)
//                                 {
//                                     [_tablwVu deselectRowAtIndexPath:indexPath animated:NO];
//                                     [alert dismissViewControllerAnimated:YES completion:nil];
//                                     
//                                 }];
//        
//        [alert addAction:ok];
//        [alert addAction:cancel];
//        [self presentViewController:alert animated:YES completion:nil];
        deletingCell=cell;
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                             destructiveButtonTitle:@"Delete"
                                                  otherButtonTitles:nil];
        [sheet showInView:self.view];
        
    }
}


- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self localCell:deletingCell addRemoveButtonPressedWithIndexPath:deletingCell.indexPath];
    }else if(buttonIndex == actionSheet.cancelButtonIndex){
        [_tablwVu deselectRowAtIndexPath:deletingCell.indexPath animated:NO];
        deletingCell=nil;
    }
}

-(void)cell:(TeamMemberCell *)cell phonePressedWithIndexPath:(NSIndexPath *)indexPath
{
    NSString* phn = nil;
    NSString* ext = nil;
    if (isSearch) {
        if ([[search objectAtIndex:indexPath.row] isKindOfClass:[Contact class]]) {
            Contact* c = [search objectAtIndex:indexPath.row];
            phn=c.phone;
            ext=c.extension;
        }else{
            Team* t = [search objectAtIndex:indexPath.row];
            if (t.phones.count>0) {
                TeamPhone* tp = [t.phones objectAtIndex:0];
                phn = tp.number;
                ext = tp.ext;
            }
        }
    }else{
        if (indexPath.row < [contacts count]) {
            Contact* c = [contacts objectAtIndex:indexPath.row];
            phn=c.phone;
            ext = c.extension;
        }else{
            Team* t = [teamsAPI objectAtIndex:indexPath.row-contacts.count];
            if (t.phones.count>0) {
                TeamPhone* tp = [t.phones objectAtIndex:0];
                phn = tp.number;
                ext = tp.ext;
            }
        }
    }
    
    phn=[phn stringByReplacingOccurrencesOfString:@"-" withString:@""];
    if (phn!=nil &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@;%@",phn,ext]]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@;%@",phn,ext]]];
    }
}

-(void)cell:(TeamMemberCell *)cell emailPressedWithIndexPath:(NSIndexPath *)indexPath
{
    NSString* email = nil;
    if (isSearch) {
        if ([[search objectAtIndex:indexPath.row] isKindOfClass:[Contact class]]) {
            Contact* c = [search objectAtIndex:indexPath.row];
            email=c.email;
        }else{
            Team* t = [search objectAtIndex:indexPath.row];
            if (t.emails.count>0) {
                TeamEmail* te = [t.emails objectAtIndex:0];
                email = te.value;
            }
        }
    }else{
        if (indexPath.row < [contacts count]) {
            Contact* c = [contacts objectAtIndex:indexPath.row];
            email=c.email;
        }else{
            Team* t = [teamsAPI objectAtIndex:indexPath.row-contacts.count];
            if (t.emails.count>0) {
                TeamEmail* te = [t.emails objectAtIndex:0];
                email = te.value;
            }
        }
    }
    if (email!=nil &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto://%@",email]]]) {
            NSLog(@"%@",email);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto://%@",email]]];
    }
}

-(void)localCell:(TeamMemberCell *)cell addRemoveButtonPressedWithIndexPath:(NSIndexPath *)indexPath{
    BOOL isRemoved=NO;
    if (isSearch) {
        if ([[search objectAtIndex:indexPath.row] isKindOfClass:[Contact class]]) {
            Contact* c = [search objectAtIndex:indexPath.row];
            if ([AppManager team:_childTeam containsContact:c]) {
                _childTeam.contactsIdsCSV=[_childTeam.contactsIdsCSV stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@%@",c.objectID.URIRepresentation.absoluteString,SEPARATOR] withString:@""];
                isRemoved = YES;
            }else{
                _childTeam.contactsIdsCSV=[_childTeam.contactsIdsCSV stringByAppendingString:[NSString stringWithFormat:@"%@%@",c.objectID.URIRepresentation.absoluteString,SEPARATOR]];
                isRemoved = NO;
            }
        }else{
            Team* t = [search objectAtIndex:indexPath.row];
            if ([AppManager team:_childTeam containsTeamAPI:t]) {
                _childTeam.teamIdsCSV=[_childTeam.teamIdsCSV stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@%@",t.nid,SEPARATOR] withString:@""];
                isRemoved = YES;
            }else{
                _childTeam.teamIdsCSV=[_childTeam.teamIdsCSV stringByAppendingString:[NSString stringWithFormat:@"%@%@",t.nid,SEPARATOR]];
                isRemoved = NO;
            }
        }
    }else{
        if (indexPath.row < [contacts count]) {
            Contact* c = [contacts objectAtIndex:indexPath.row];
            if ([AppManager team:_childTeam containsContact:c]) {
                _childTeam.contactsIdsCSV=[_childTeam.contactsIdsCSV stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@%@",c.objectID.URIRepresentation.absoluteString,SEPARATOR] withString:@""];
                isRemoved = YES;
            }else{
                _childTeam.contactsIdsCSV=[_childTeam.contactsIdsCSV stringByAppendingString:[NSString stringWithFormat:@"%@%@",c.objectID.URIRepresentation.absoluteString,SEPARATOR]];
                isRemoved = NO;
            }
        }else{
            Team* t = [teamsAPI objectAtIndex:indexPath.row-contacts.count];
            if ([AppManager team:_childTeam containsTeamAPI:t]) {
                _childTeam.teamIdsCSV=[_childTeam.teamIdsCSV stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@%@",t.nid,SEPARATOR] withString:@""];
                isRemoved = YES;
            }else{
                _childTeam.teamIdsCSV=[_childTeam.teamIdsCSV stringByAppendingString:[NSString stringWithFormat:@"%@%@",t.nid,SEPARATOR]];
                isRemoved =  NO;
            }
        }
    }
    NSError *error;
    [appDelegate.managedObjectContext save:&error];
    if (error!=nil) {
        [JDStatusBarNotification showWithStatus:isRemoved?@"Removed with error":@"Added with error" dismissAfter:STATUSBAR_NOTIFICATION_DURATION];
        [_tablwVu deselectRowAtIndexPath:indexPath animated:NO];
        return;
    }else{
        locked=YES;
        TeamMemberCell* cel=[_tablwVu cellForRowAtIndexPath:indexPath];
        UIView* vu = cel.contVu;
        UIGraphicsBeginImageContext(vu.bounds.size);
        [vu.layer renderInContext:UIGraphicsGetCurrentContext()];
        CGRect rect = [vu convertRect:vu.frame toView:_viewTeamBtn.superview];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        UIImageView* imgVu = [[UIImageView alloc] initWithFrame:rect];
        imgVu.image = image;
        [_viewTeamBtn.superview addSubview:imgVu];
        
//        [JDStatusBarNotification showWithStatus:isRemoved?@"Removed successfully":@"Added successfully" dismissAfter:STATUSBAR_NOTIFICATION_DURATION];
        if (isRemoved==YES) {
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                                action:kActTeamContactRemoved
                                                                                                 label:nil
                                                                                                 value:nil] build]];
            
            if (!_isEditor) {
                [self animationCompletedWithView:imgVu indexPath:indexPath];
            }else{
                [imgVu genieOutTransitionWithDuration:ANIMATION_DURATION_LONG startRect:CGRectMake(_viewTeamBtn.frame.origin.x+_viewTeamBtn.frame.size.width/2.0, _viewTeamBtn.frame.origin.y-5, 5, 5) startEdge:BCRectEdgeTop completion:^{
                    [self animationCompletedWithView:imgVu indexPath:indexPath];
                }];
            }
            
        }else{
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                                action:kActTeamContactAdded
                                                                                                 label:nil
                                                                                                 value:nil] build]];
            [imgVu genieInTransitionWithDuration:ANIMATION_DURATION_LONG
                                 destinationRect:CGRectMake(_viewTeamBtn.frame.origin.x+_viewTeamBtn.frame.size.width/2.0, _viewTeamBtn.frame.origin.y-5, 5, 5)
                                 destinationEdge:BCRectEdgeTop
                                      completion:^{
                                          [self animationCompletedWithView:imgVu indexPath:indexPath];
                                      }
             ];
        }
    }
    
}
-(void)animationCompletedWithView:(UIView*)imgVu indexPath:(NSIndexPath*)indexPath{
    locked=NO;
    [self updateCount];
    [imgVu removeFromSuperview];
    [[AppManager sharedManager] populateTeamsData];
    [_tablwVu deselectRowAtIndexPath:indexPath animated:NO];
    if (!_isEditor) {
        [self populateChildContactsAndTeams];
        [self.tablwVu reloadData];
    }else{
        [_tablwVu reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
//    if (_isEditor==NO) {
        [self switchVCifNeeded];
//    }
    deletingCell = nil;
}
-(void)switchVCifNeeded{
    [self updateCount];
    int count = (int)[[AppManager sharedManager] teamsAPIForChildTeam:_childTeam].count+(int)[[AppManager sharedManager] ContactsForChildTeam:_childTeam].count;
    NSMutableArray* arr = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    BOOL twoTeamEditors=NO;
    if (arr.count>1) {
        twoTeamEditors=[[arr objectAtIndex:arr.count-2] isKindOfClass:[TeamEditorViewController class]];
    }
    if (count<1 && _isEditor==NO) {
        
        if (twoTeamEditors==YES) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            TeamEditorViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamEditorViewController"];
            vc.child = _child;
            vc.childTeam=[[AppManager sharedManager] teamForChild:_child];
            long n = [[AppManager sharedManager] ContactsForChildTeam:vc.childTeam].count;
            n += [[AppManager sharedManager] teamsAPIForChildTeam:vc.childTeam].count;
            vc.isEditor= n<1;
            
            [arr setObject:vc atIndexedSubscript:arr.count-1];
            [self.navigationController setViewControllers:arr animated:NO];
        }
    }else if(count<1 && _isEditor==YES && twoTeamEditors==YES){
        [arr removeLastObject];
        if (arr.count<2) {
            self.navigationItem.leftBarButtonItem=nil;
        }
        [arr replaceObjectAtIndex:arr.count-1 withObject:self];
        [self.navigationController setViewControllers:arr animated:NO];
    }
}
-(void)cell:(TeamMemberCell *)cell toggleExpansionButtonPressedWithIndexPath:(NSIndexPath *)indexPath{
    BOOL isExpanded = [[expansionFlags objectAtIndex:indexPath.row] boolValue];
    [expansionFlags setObject:[NSNumber numberWithBool:!isExpanded] atIndexedSubscript:indexPath.row];
    
    if ([[expansionFlags objectAtIndex:indexPath.row] boolValue]==YES) {
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                            action:kActTeamContactExpanded
                                                                                             label:nil
                                                                                             value:nil] build]];
    }else{
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                            action:kActTeamContactCollapsed
                                                                                             label:nil
                                                                                             value:nil] build]];
    }
    
    [_tablwVu reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void)cell:(TeamMemberCell *)cell editContactPressedWithIndexPath:(NSIndexPath *)indexPath{
    
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:kCatTeam
                                                                                        action:kActTeamContactEditPressed
                                                                                         label:nil
                                                                                         value:nil] build]];
    
    AddContactViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddContactViewController"];
    //    vc.childTeam=_childTeam;
    if (isSearch==YES) {
        vc.contact=search[indexPath.row];
    }else{
        vc.contact=contacts[indexPath.row];
    }
    [self.navigationController pushViewController:vc animated:YES];
//    UIAlertView* alertVu = [[UIAlertView alloc] initWithTitle:@"Do you want to edit the contact?" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Edit", nil];
//    alertVu.tag=indexPath.row;
//    [alertVu show];
}

-(void)cell:(TeamMemberCell *)cell imgUpload:(NSIndexPath *)indexPath
{
    pickingImageIndexPath = indexPath;
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    CGFloat size = 200.0;
    CGFloat scale = image.size.height>image.size.width?size/image.size.height:size/image.size.width;
    Contact* c=nil;
    Team* t=nil;
    if (isSearch) {
        id member = search[pickingImageIndexPath.row];
        if ([member isKindOfClass:[Contact class]]) {
            c = search[pickingImageIndexPath.row];
        }else{
            t = search[pickingImageIndexPath.row];
        }
    }
    else
    {
        if (pickingImageIndexPath.row<contacts.count) {
            c = [contacts objectAtIndex:pickingImageIndexPath.row];
        }else{
            t = [teamsAPI objectAtIndex:(pickingImageIndexPath.row - contacts.count)];
        }
    }
    if (c!=nil) {
        c.image = UIImageJPEGRepresentation ([image imageScaledToSize:CGSizeMake(image.size.width*scale, image.size.height*scale)],0.7);
    }else if (t!=nil){
        TeamAPIImage* teamAPIImage = [self teamAPIImageForId:t.nid onlyCache:NO];
        if (teamAPIImage==nil) {
            teamAPIImage=[NSEntityDescription insertNewObjectForEntityForName:@"TeamAPIImage" inManagedObjectContext:appDelegate.managedObjectContext];
            [cachedImages addObject:teamAPIImage];
        }
        teamAPIImage.data = UIImageJPEGRepresentation ([image imageScaledToSize:CGSizeMake(image.size.width*scale, image.size.height*scale)],0.7);
        teamAPIImage.teamAPIId = t.nid;
        [teamImagesDbHitIds removeObject:t.nid];
    }
    NSError *error;
    if (![appDelegate.managedObjectContext save:&error]) {
        [[[UIAlertView alloc]initWithTitle:@"" message:@"An error occured" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
    }else{
        [_tablwVu reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView.title isEqualToString:@"Do you want to edit the contact?"] && buttonIndex!=alertView.cancelButtonIndex) {
        AddContactViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddContactViewController"];
        //    vc.childTeam=_childTeam;
        if (isSearch==YES) {
            vc.contact=search[alertView.tag];
        }else{
            vc.contact=contacts[alertView.tag];
        }
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark - Actions
- (IBAction)clearBtnPressed:(id)sender {
    srchFld.text=@"";
    [self filterTableView];
}

- (IBAction)viewTeamAction:(UIButton *)sender {
    TeamEditorViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamEditorViewController"];
    vc.child = _child;
    vc.childTeam = _childTeam;
//    long n = [[AppManager sharedManager] ContactsForChildTeam:vc.childTeam].count;
//    n += [[AppManager sharedManager] teamsAPIForChildTeam:vc.childTeam].count;
    
    
    if ([[sender titleForState:UIControlStateNormal] isEqualToString:@"ADD MORE TO THE TEAM"]) {
        
        vc.isEditor=YES;
    }else{
        vc.isEditor=NO;
    }

    if ([[self.navigationController.viewControllers objectAtIndex:0] isKindOfClass:[self class]]) {
        if (self.navigationController.viewControllers.count>1) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else{
        if (self.navigationController.viewControllers.count>2) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
    
}
#pragma mark - DB
-(TeamAPIImage*)teamAPIImageForId:(NSString*)nid onlyCache:(BOOL)onlyCache{
    for (TeamAPIImage* t in cachedImages) {
        if ([t.teamAPIId isEqualToString:nid]) {
            return t;
        }
    }
    if (onlyCache) {
        return nil;
    }
    NSFetchRequest* req = [NSFetchRequest new];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"TeamAPIImage" inManagedObjectContext:appDelegate.managedObjectContext];
    NSPredicate* predi = [NSPredicate predicateWithFormat:@"teamAPIId=%@",nid];
    [req setEntity:entity];
    [req setPredicate:predi];
    NSArray* objs = [appDelegate.managedObjectContext executeFetchRequest:req error:NULL];
    [teamImagesDbHitIds addObject:nid];
    if (objs.count>0) {
        [cachedImages addObject:[objs objectAtIndex:0]];
        return [objs objectAtIndex:0];
    }
    return nil;
}
@end
