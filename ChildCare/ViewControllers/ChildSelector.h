//
//  SecondViewController.h
//  ChildCare
//
//  Created by Devclan on 14/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "IGLDropDownMenu.h"
#import "Agency.h"

@protocol ChildSelectionCompletionDelegate
@required
-(void)viewController:(UIViewController*)viewController completedSelectionWithChild:(Child*)child;
@end


@interface ChildSelector : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)id<ChildSelectionCompletionDelegate> completionDelegate;
@property (strong,nonatomic) Agency* agency;
@end

