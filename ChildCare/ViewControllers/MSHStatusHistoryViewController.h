//
//  MSHStatusHistoryViewController.h
//  ChildCare
//
//  Created by Devclan on 30/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Agency.h"
#import "Child.h"
#import "IGLDropDownMenu.h"
#import "WWAAMSHViewController.h"
@interface MSHStatusHistoryViewController : UIViewController
@property (nonatomic,strong)Agency* agency;
@property (nonatomic,strong)Child* activeChild;
@property (strong,nonatomic)WWAAMSHViewController* WWAAMSHVC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConst;
@property (strong, nonatomic) IGLDropDownMenu *dropdown;
@end
