//
//  ImageCache.h
//  ChildCare
//
//  Created by Devclan on 27/01/2016.
//  Copyright © 2016 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageCache : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "ImageCache+CoreDataProperties.h"
