//
//  UIImage.h
//  ChildCare
//
//  Created by Devclan on 22/12/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UIImage(Additions)
- (UIImage *)imageScaledToSize:(CGSize)newSize  ;
@end
