//
//  TeamPhone.h
//  ChildCare
//
//  Created by Devclan on 20/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamPhone : NSObject

@property (nonatomic,strong)NSString* number;
@property (nonatomic,strong)NSString* code;
@property (nonatomic,strong)NSString* ext;
- (instancetype)initWithDictionary:(NSDictionary*)dic;

@end
