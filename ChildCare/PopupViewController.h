//
//  PopupViewController.h
//  Meri Awaz
//
//  Created by Devclan on 13/10/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PopDelegate
@required
-(void)waitingButtonTapped;
-(void)accessingButtonTapped;
-(void)completedButtonTapped;
@end
@interface PopupViewController : UIViewController
@property id<PopDelegate> delegate;



@end
