//
//  Team.m
//  ChildCare
//
//  Created by Devclan on 20/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "Team.h"
#import "TeamAgency.h"
#import "TeamOffice.h"
#import "AppManager.h"

@implementation Team

-(instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [self init];
    if (self) {
        NSArray *agencies = [dic objectForKey:@"agency_thumbnail"];
        for (NSDictionary *d in agencies) {
            [_agencyThumbnails addObject:[[AgencyImage alloc] initWithDictionary:d]];
        }
        
        NSArray *genders = [dic objectForKey:@"gender"];
        for (NSDictionary *d in genders) {
            [_genders addObject:[d objectForKey:@"value"]];
        }
        
        _body = [[TeamBody alloc] initWithDictionary:[dic objectForKey:@"body"]];

        NSArray *disciplines = [dic objectForKey:@"discipline"];
        for (NSDictionary *d in disciplines) {
            [_disciplines addObject:[[TeamDiscipline alloc] initWithDictionary:d]];
        }
        
        NSArray *emails = [dic objectForKey:@"email"];
        for (NSDictionary *d in emails) {
            [_emails addObject:[[TeamEmail alloc] initWithDictionary:d]];
        }
        
        NSArray *offices = [dic objectForKey:@"offices"];
        for (NSDictionary *d in offices) {
            [_offices addObject:[[TeamOffice alloc] initWithDictionary:d]];
        }
        
        NSArray *headsots = [dic objectForKey:@"headshot"];
        for (NSDictionary *d in headsots) {
            [_headshots addObject:[[Headshot alloc] initWithDictionary:d]];
        }
        
        NSArray *names = [dic objectForKey:@"name"];
        for (NSDictionary *d in names) {
            [_names addObject:[[TeamName alloc] initWithDictionary:d]];
        }
        
        NSArray* phns = [dic objectForKey:@"team_phone"];
        for(NSDictionary *d in phns){
            [_phones addObject:[[TeamPhone alloc] initWithDictionary:d]];
        }
        _title = [AppManager getStringForKey:@"title" fromDic:dic];
        _nid = [AppManager getStringForKey:@"nid" fromDic:dic];
    }
    return self;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _agencyThumbnails = [NSMutableArray new];
        _body = [[TeamBody alloc]init];
        _disciplines = [NSMutableArray new];
        _emails = [NSMutableArray new];
        _genders = [NSMutableArray new];
        _offices = [NSMutableArray new];
        _headshots=[NSMutableArray new];
        _names = [NSMutableArray new];
        _phones = [[NSMutableArray alloc] init];
        _title = @"";
        _nid=@"";
    }
    return self;
}

@end
