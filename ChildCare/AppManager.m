//
//  AppManager.m
//  PostingApp
//
//  Created by Devclan on 03/08/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "AppManager.h"
#import "Contact.h"
#import "ChildTeam.h"
#import "Constants.h"
#import "AgencyContact.h"
#import "Agency.h"
#import "ToolsPopupViewController.h"

@implementation AppManager{
}
+ (id)sharedManager {
    static AppManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
-(void)populateDB{
    [self populateAPIsData];
    [self populateChildrenData];
    [self populateContactData];
    [self populateTeamsData];
}
- (id)init {
    if (self = [super init]) {
        
        _appThemeColor=[UIColor colorWithRed:228/255.0 green:27/255.0 blue:34/255.0 alpha:1];
        _appThemeDisabledColor=[UIColor colorWithRed:242/255.0 green:183/255.0 blue:189/255.0 alpha:1];
        
        _placeholderColor=[UIColor colorWithRed:125/255.0 green:126/255.0 blue:127/255.0 alpha:1];
        _textColor=[UIColor colorWithRed:125/255.0 green:126/255.0 blue:127/255.0 alpha:1];
        _yellowColor = [UIColor colorWithRed:254/255.0 green:159/255.0 blue:7/255.0 alpha:1];
        _greeColor = [UIColor colorWithRed:2/255.0 green:123/255.0 blue:42/255.0 alpha:1];
        
        _agencies=[NSMutableArray new];
        _homeTextAPI=[NSMutableArray new];
        _agenciesContacts=[NSMutableArray new];
        _teamsAPI = [NSMutableArray new];
        _teamsDB = [NSMutableArray new];
        _contacts = [NSMutableArray new];
        
        _jsonResponses=[NSMutableArray new];
        _appDelegate=[[UIApplication sharedApplication] delegate];
        _children=[NSMutableArray new];
        
        
        [self populateDB];
        if (_children.count>0 && [self activeChild]==nil) {
            Child* c = [_children objectAtIndex:0];
            c.active=@YES;
            [_appDelegate.managedObjectContext save:NULL];
        }
        
    }
    return self;
}
//-(int)getNextContactId{
//    int nxtId = -1;
//    
//    for (Contact *c in _contacts) {
//        if ([c.identity integerValue]>nxtId) {
//            nxtId = (int) [c.identity integerValue];
//        }
//    }
//    
//    return nxtId+1;
//}
-(BOOL)childExists:(NSString*)name{
    BOOL b=NO;
    for (Child* c in _children) {
        if ([c.name isEqualToString:name]) {
            b=YES;
            break;
        }
    }
    return b;
}
//-(NSArray*)contactsWithCSVIds:(NSString*)ids{
//    NSArray* idsArr = [ids componentsSeparatedByString:@","];
//    NSMutableArray* arr = [NSMutableArray new];
//    for (NSString* strId in idsArr) {
//        Contact* c = [self contactWithId:strId];
//        if (c!=nil) {
//            [arr addObject:c];
//        }
//    }
//    return arr;
//}
//-(Contact*)contactWithId:(NSString*)identity{
//    for (Contact* c in _contacts) {
//        if ([[c.identity stringValue] isEqualToString:identity]) {
//            return c;
//        }
//    }
//    return nil;
//}
+ (BOOL) team:(ChildTeam*)team containsContact:(Contact *)contact{
    NSArray* cntIds = [team.contactsIdsCSV componentsSeparatedByString:SEPARATOR];
    for (NSString* i in cntIds) {
        if ([i isEqualToString:contact.objectID.URIRepresentation.absoluteString]) {
            return YES;
        }
    }
    return NO;
}
+ (BOOL) team:(ChildTeam*)team containsTeamAPI:(Team *)teamAPI{
    NSArray* tmIds = [team.teamIdsCSV componentsSeparatedByString:SEPARATOR];
    for (NSString* i in tmIds) {
        if ([i isEqualToString:teamAPI.nid]) {
            return YES;
        }
    }
    return NO;
}

-(Contact*)contactWithId:(NSString*)identity{
    for (Contact* c in _contacts) {
        if ([c.objectID.URIRepresentation.absoluteString isEqualToString:identity]) {
            return c;
        }
    }
    return nil;
}

-(Team*)teamAPIWithId:(NSString*)identity{
    for (Team* c in _teamsAPI) {
        if ([c.nid isEqualToString:identity]) {
            return c;
        }
    }
    return nil;
}
-(Child*)activeChild{
    Child* ch=nil;
    for (Child* c in _children) {
        if ([c.active boolValue]==YES) {
            ch=c;
            break;
        }
    }
    return ch;
}
-(void)populateChildrenData{
    [_children removeAllObjects];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Child"
                                              inManagedObjectContext:_appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError* error;
    NSArray *fetchedObjects = [_appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0) {
        for (Child *c in fetchedObjects) {
            [_children addObject:c];
        }
    }
    
}

-(void)populateTeamsData{
    [_teamsDB removeAllObjects];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ChildTeam"
                                              inManagedObjectContext:_appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError* error;
    NSArray *fetchedObjects = [_appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0) {
        for (ChildTeam *t in fetchedObjects) {
            [_teamsDB addObject:t];
        }
    }
}
-(ChildTeam*)teamForChild:(Child*)child{
    for (ChildTeam* t in _teamsDB) {
        if ([t.childId isEqualToString:child.objectID.URIRepresentation.absoluteString]) {
            return t;
        }
    }

    ChildTeam *childTeam = [NSEntityDescription insertNewObjectForEntityForName:@"ChildTeam" inManagedObjectContext:_appDelegate.managedObjectContext];
    
    childTeam.childId = child.objectID.URIRepresentation.absoluteString;
    childTeam.teamIdsCSV=@"";
    childTeam.contactsIdsCSV=@"";
    NSError *error;
    
    if (![_appDelegate.managedObjectContext save:&error]) {
//        [[[UIAlertView alloc]initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        return nil;
    }
    else
    {
        [self populateTeamsData];
        return childTeam;
        
    }
}
-(void)populateContactData{
    [_contacts removeAllObjects];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Contact"
                                              inManagedObjectContext:_appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError* error;
    NSArray *fetchedObjects = [_appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0) {
        for (Contact *c in fetchedObjects) {
            [_contacts addObject:c];
        }
    }
    
    NSArray *sortedArray;
    sortedArray = [_contacts sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(Contact*)a name];
        NSString *second = [(Contact*)b name];
        return [first compare:second];
    }];
    
    
   
    _contacts = [[NSMutableArray alloc] initWithArray:sortedArray];
}


-(ImageCache*)imageFromDBWithURL:(NSString*)url{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ImageCache"
                                              inManagedObjectContext:_appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSPredicate* predi = [NSPredicate predicateWithFormat:@"url=%@",url];
    NSError* error;
    [fetchRequest setPredicate:predi];
    NSArray *fetchedObjects = [_appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0) {
        ImageCache* c = [fetchedObjects objectAtIndex:0];
        return c;
    }
    return nil;
}

-(void)imageFromURL:(NSString*)url completion:(void(^)(UIImage* image, NSError* error))completion{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   [self insertOrUpdateImage:data url:url];
                                   completion(image,nil);
                               } else{
                                   completion(nil,error);
                               }
                           }];
}


-(void)populateAPIsData{
    [_jsonResponses removeAllObjects];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"JSONResponse"
                                              inManagedObjectContext:_appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError* error;
    NSArray *fetchedObjects = [_appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0) {
        for (JSONResponse *c in fetchedObjects) {
            [_jsonResponses addObject:c];
        }
    }
}
-(void)synchronizeAgenicesAndAgenciesContacts{
    if (_agencies.count<1 || _agenciesContacts.count<1) {
        return;
    }
    for (AgencyContact* ac in _agenciesContacts) {
        Agency* a = [self agencyForId:ac.agencyId];
        if (a!=nil) {
            a.agencyContact=ac;
        }
    }
}
-(NSMutableArray*)teamsAPIForChildTeam:(ChildTeam*)team{
    NSMutableArray* arr = [NSMutableArray new];
    NSArray* idsArr = [team.teamIdsCSV componentsSeparatedByString:SEPARATOR];
    for (NSString* strId in idsArr) {
        Team* t = [self teamAPIWithId:strId];
        if (t!=nil) {
            [arr addObject:t];
        }
    }
    
    
    NSArray *sortedArray;
    sortedArray = [arr sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(Team*)a title];
        NSString *second = [(Team*)b title];
        return [first compare:second];
    }];
    
    
    
    arr = [[NSMutableArray alloc] initWithArray:sortedArray];
    
    
    return arr;
}
-(NSMutableArray*)ContactsForChildTeam:(ChildTeam*)team{
    NSMutableArray* arr = [NSMutableArray new];
    
    NSArray* idsArr = [team.contactsIdsCSV componentsSeparatedByString:SEPARATOR];
    for (NSString* strId in idsArr) {
        Contact* c = [self contactWithId:strId];
        if (c!=nil) {
            [arr addObject:c];
        }
    }
    
    NSArray *sortedArray;
    sortedArray = [arr sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(Contact*)a name];
        NSString *second = [(Contact*)b name];
        return [first compare:second];
    }];
    
    
    
    arr = [[NSMutableArray alloc] initWithArray:sortedArray];
    
    
    return arr;
}
-(Agency*)agencyForId:(NSString*)nid{
    for (Agency* a in _agencies) {
        if ([a.nid isEqualToString:nid]) {
            return a;
        }
    }
    return nil;
}
+ (UIColor *) colorFromHexString:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

-(JSONResponse*)jsonResponse:(NSString*)url{
    JSONResponse* json=nil;
    for (JSONResponse* r in _jsonResponses) {
        if ([r.url isEqualToString:url]) {
            json=r;
            break;
        }
    }
    return json;
}
+(CGRect)frameForTextInConstraint:(CGSize)labelContraints string:(NSString*)string fontName:(NSString*)fontName fontSize:(int)fontSize{
    NSStringDrawingContext *context     = [[NSStringDrawingContext alloc] init];
    return [string boundingRectWithSize:labelContraints
                                                   options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin
                                                attributes:@{ NSFontAttributeName : [UIFont fontWithName:fontName size:fontSize],
                                                              NSForegroundColorAttributeName : [UIColor redColor]}
                                                   context:context];
}
+(void)presentToolsPopupInViewController:(UIViewController*)viewController fromFrame:(CGRect)frame{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Calendar"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //Handel your yes please button action here
                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Notes"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [viewController presentViewController:alert animated:YES completion:nil];
    
    
}
-(int)indexForChild:(Child*)child{
    for (int j=0; j<_children.count; j++) {
        Child* c = _children[j];
        if ([c.objectID.URIRepresentation.absoluteString isEqualToString:child.objectID.URIRepresentation.absoluteString]) {
            return j;
        }
    }
    return -1;
}
+(BOOL)isNumeric:(NSString*)inputString{
    BOOL isValid = NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:inputString];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet];
    return isValid;
}
+(BOOL)isValueNULLForKey:(NSString*)key inDic:(NSDictionary*)dic{
    return [dic objectForKey:key]==[NSNull null];
}
-(BOOL)insertOrUpdateResponse:(NSData*)resp url:(NSString*)url{
    JSONResponse* jsonResp = nil;
    for (JSONResponse* r in _jsonResponses) {
        if ([r.url isEqualToString:url]) {
            jsonResp=r;
        }
    }
    if (jsonResp==nil) {
        jsonResp = [NSEntityDescription
                    insertNewObjectForEntityForName:@"JSONResponse"
                    inManagedObjectContext:_appDelegate.managedObjectContext];
    }
    jsonResp.url=url;
    jsonResp.value=resp;
    jsonResp.date=[NSDate date];
    NSError* error;
    [_appDelegate.managedObjectContext save:&error];
    if (error==nil) {
        [self populateAPIsData];
    }
    return error==nil;
}
-(BOOL)insertOrUpdateImage:(NSData*)data url:(NSString*)url{
    ImageCache* img = [self imageFromDBWithURL:url];
    if (img==nil) {
        img = [NSEntityDescription
                    insertNewObjectForEntityForName:@"ImageCache"
                    inManagedObjectContext:_appDelegate.managedObjectContext];
    }
    img.url=url;
    img.data=data;
    img.date=[NSDate date];
    NSError* error;
    [_appDelegate.managedObjectContext save:&error];
    return error==nil;
}


+(NSString*)getStringForKey:(NSString*)key fromDic:(NSDictionary*)dic{
    id s=[dic objectForKey:key];
    if (s==[NSNull null]) {
        return @"";
    }
    return s;
}
+(void)layoutCellIfNeeded:(UITableViewCell*)cell tableView:(UITableView*)tableView{
    if (cell.bounds.size.width!=tableView.bounds.size.width) {
        cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
        [cell setNeedsLayout];
        [cell layoutIfNeeded];
    }
}
+ (NSString *)urlencode:(NSString*)url{
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[url UTF8String];
    long sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

+(int)getIntForKey:(NSString*)key fromDic:(NSDictionary*)dic{
    id s=[dic objectForKey:key];
    if (s==[NSNull null]) {
        return 0;
    }
    return [s intValue];
}
+(double)getDoubleForKey:(NSString*)key fromDic:(NSDictionary*)dic{
    id s=[dic objectForKey:key];
    if (s==[NSNull null]) {
        return 0.0;
    }
    return [s doubleValue];
}
+(int)getBoolForKey:(NSString*)key fromDic:(NSDictionary*)dic{
    id s=[dic objectForKey:key];
    if (s==[NSNull null]) {
        return NO;
    }
    return [s boolValue];
}

+ (NSString*)loadImageWithName:(NSString*)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:name];
//    UIImage* image = [UIImage imageWithContentsOfFile:path];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        return path;
    } else{
        return @"";
    }
}

+ (void)saveImageLocal: (UIImage*)image imgName:(NSString*)name
{
    if (image != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:name];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (!fileExists) {
            NSData* data = UIImagePNGRepresentation(image);
            [data writeToFile:path atomically:YES];
        }
    }
}

@end
