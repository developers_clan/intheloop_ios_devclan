//
//  AppManager.h
//  PostingApp
//
//  Created by Devclan on 03/08/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//
#import "Child.h"
#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "JSONResponse.h"
#import "ChildTeam.h"
#import "Contact.h"
#import "Team.h"
#import "TabsController.h"
#import "ImageCache+CoreDataProperties.h"
@class Agency;

@interface AppManager : NSObject
+ (id)sharedManager;
+(NSString*)getStringForKey:(NSString*)key fromDic:(NSDictionary*)dic;
+(BOOL)isValueNULLForKey:(NSString*)key inDic:(NSDictionary*)dic;
+(int)getIntForKey:(NSString*)key fromDic:(NSDictionary*)dic;
+(int)getBoolForKey:(NSString*)key fromDic:(NSDictionary*)dic;
+(double)getDoubleForKey:(NSString*)key fromDic:(NSDictionary*)dic;
+ (UIColor *) colorFromHexString:(NSString *)hexString;
+ (BOOL) team:(ChildTeam*)team containsContact:(Contact *)contact;
+ (BOOL) team:(ChildTeam*)team containsTeamAPI:(Team *)teamAPI;
+(BOOL)isNumeric:(NSString*)inputString;
+ (NSString *)urlencode:(NSString*)url;
+(void)layoutCellIfNeeded:(UITableViewCell*)cell tableView:(UITableView*)tableView;
+(void)presentToolsPopupInViewController:(UIViewController*)viewController fromFrame:(CGRect)frame;
@property (strong,nonatomic) NSMutableArray* agencies;
@property (strong,nonatomic) NSMutableArray* agenciesContacts;
@property (strong,nonatomic) NSMutableArray* homeTextAPI;
@property (strong,nonatomic) NSMutableArray* teamsAPI;
@property (strong,nonatomic) NSMutableArray* teamsDB;
@property (strong,nonatomic) NSMutableArray* contacts;
@property (strong,nonatomic) NSMutableArray* jsonResponses;
@property (strong,nonatomic) AppDelegate* appDelegate;
-(JSONResponse*)jsonResponse:(NSString*)url;
-(BOOL)insertOrUpdateImage:(NSData*)data url:(NSString*)url;
-(ImageCache*)imageFromDBWithURL:(NSString*)url;
-(int)indexForChild:(Child*)child;
-(NSMutableArray*)teamsAPIForChildTeam:(ChildTeam*)team;
-(NSMutableArray*)ContactsForChildTeam:(ChildTeam*)team;
-(BOOL)insertOrUpdateResponse:(NSData*)resp url:(NSString*)url;
@property (strong,nonatomic) NSMutableArray* children;
-(void)populateChildrenData;
-(Agency*)agencyForId:(NSString*)nid;
-(ChildTeam*)teamForChild:(Child*)child;
-(BOOL)childExists:(NSString*)name;
-(Child*)activeChild;
-(void)populateDB;
-(void)synchronizeAgenicesAndAgenciesContacts;
-(void)populateTeamsData;
+(CGRect)frameForTextInConstraint:(CGSize)labelContraints string:(NSString*)string fontName:(NSString*)fontName fontSize:(int)fontSize;
+ (NSString*)loadImageWithName:(NSString*)name;
+ (void)saveImageLocal: (UIImage*)image imgName:(NSString*)name;
@property (strong,nonatomic) UIColor* appThemeColor;
@property (strong,nonatomic) TabsController* tabsController;
@property (strong,nonatomic) UIColor* appThemeDisabledColor;

@property (strong,nonatomic) UIColor* textColor;
@property (strong,nonatomic) UIColor* placeholderColor;

@property (strong,nonatomic) UIColor* greeColor;
@property (strong,nonatomic) UIColor* yellowColor;
@end
