//
//  HomeText.h
//  ChildCare
//
//  Created by developer clan on 11/04/2016.
//  Copyright © 2016 Devclan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppManager.h"

@interface HomeText : NSObject

@property (strong,nonatomic)NSString* homeText;

- (instancetype)initWithDictionary:(NSDictionary*)dic;

@end
