//
//  TeamAPIImage+CoreDataProperties.m
//  
//
//  Created by Devclan on 18/01/2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TeamAPIImage+CoreDataProperties.h"

@implementation TeamAPIImage (CoreDataProperties)

@dynamic teamAPIId;
@dynamic data;

@end
