//
//  TeamAPIImage+CoreDataProperties.h
//  
//
//  Created by Devclan on 18/01/2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TeamAPIImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface TeamAPIImage (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *teamAPIId;
@property (nullable, nonatomic, retain) NSData *data;

@end

NS_ASSUME_NONNULL_END
