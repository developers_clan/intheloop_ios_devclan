//
//  Contact.h
//  
//
//  Created by Devclan on 05/01/2016.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Contact : NSManagedObject

@property (nonatomic, retain) NSString * agency;
@property (nonatomic, retain) NSString * discipline;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * extension;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSNumber * gender;

@end
