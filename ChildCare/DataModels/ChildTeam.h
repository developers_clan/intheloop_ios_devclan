//
//  ChildTeam.h
//  
//
//  Created by Devclan on 05/01/2016.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ChildTeam : NSManagedObject

@property (nonatomic, retain) NSString * childId;
@property (nonatomic, retain) NSString * teamIdsCSV;
@property (nonatomic, retain) NSString * contactsIdsCSV;

@end
