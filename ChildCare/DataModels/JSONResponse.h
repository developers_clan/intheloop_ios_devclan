//
//  JSONResponse.h
//  
//
//  Created by Devclan on 05/01/2016.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface JSONResponse : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSData * value;

@end
