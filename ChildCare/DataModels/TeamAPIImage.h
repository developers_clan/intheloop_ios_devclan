//
//  TeamAPIImage.h
//  
//
//  Created by Devclan on 18/01/2016.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface TeamAPIImage : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "TeamAPIImage+CoreDataProperties.h"
