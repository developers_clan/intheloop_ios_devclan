//
//  Child.h
//  
//
//  Created by Devclan on 05/01/2016.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Child : NSManagedObject

@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) NSNumber * gender;
@property (nonatomic, retain) NSNumber * identity;
@property (nonatomic, retain) NSString * name;

@end
