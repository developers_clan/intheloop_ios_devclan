//
//  Contact.m
//  
//
//  Created by Devclan on 05/01/2016.
//
//

#import "Contact.h"


@implementation Contact

@dynamic agency;
@dynamic discipline;
@dynamic email;
@dynamic extension;
@dynamic image;
@dynamic name;
@dynamic phone;
@dynamic gender;

@end
