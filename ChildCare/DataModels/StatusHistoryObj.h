//
//  StatusHistoryObj.h
//  
//
//  Created by Devclan on 05/01/2016.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface StatusHistoryObj : NSManagedObject

@property (nonatomic, retain) NSString * agencyId;
@property (nonatomic, retain) NSString * childId;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * programInd;
@property (nonatomic, retain) NSNumber * stageInd;
@property (nonatomic, retain) NSNumber * statusInd;
@property (nonatomic, retain) NSString * title;

@end
