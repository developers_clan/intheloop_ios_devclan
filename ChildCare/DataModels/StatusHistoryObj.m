//
//  StatusHistoryObj.m
//  
//
//  Created by Devclan on 05/01/2016.
//
//

#import "StatusHistoryObj.h"


@implementation StatusHistoryObj

@dynamic agencyId;
@dynamic childId;
@dynamic date;
@dynamic programInd;
@dynamic stageInd;
@dynamic statusInd;
@dynamic title;

@end
