//
//  TeamOffice.m
//  ChildCare
//
//  Created by Devclan on 20/11/2015.
//  Copyright (c) 2015 Devclan. All rights reserved.
//

#import "TeamOffice.h"
#import "AppManager.h"

@implementation TeamOffice

-(instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [self init];
    if (self) {
        _office = [AppManager getStringForKey:@"office" fromDic:dic];
    }
    return self;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _office = @"N/A";
    }
    return self;
}

@end
